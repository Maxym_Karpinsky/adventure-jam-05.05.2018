﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    public class ComboTracker : MonoBehaviour
    {
		[SerializeField] private float _trackingCooldownTime = 6f;
		private Cooldown _trackingCooldown;

		private Tracker<int> _tracker;
		public int _ComboValue { get { return this._tracker.Value; } }

		private Coroutine _comboTrackingProcess;

		private IEnumerator ComboTrackingProcess()
		{
			// Turn on the UI.
			this._comboDisplayController?.Open();

			while (!this._trackingCooldown.IsFinished())
			{
				// Update the UI slider.
				yield return null;
			}

			this._tracker.Reset();

			// Turn off the UI.
			this._comboDisplayController?.Close();

			this._comboTrackingProcess = null;
		}

		public void Register(int quantity)
		{
			this._trackingCooldown.Reset();

			this._tracker.Value += quantity;
			
			if (this._comboTrackingProcess == null)
				this._comboTrackingProcess = this.StartCoroutine(this.ComboTrackingProcess());
			
			// Update the UI score.
			this._comboDisplayController?.UpdateLayout();
		}

		// TEST
		[SerializeField] private ComboDisplayController _comboDisplayController;

		public void Register()
		{
			this.Register(1);
		}

		protected void Awake()
		{
			this._trackingCooldown = new Cooldown(this._trackingCooldownTime, 0);
			this._tracker = new Tracker<int>(0);

			this._comboTrackingProcess = null;
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{

		//}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(ComboTracker))]
    [CanEditMultipleObjects]
    public class ComboTrackerEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            ComboTracker sComboTracker = target as ComboTracker;
#pragma warning restore 0219
        }
    }
#endif
}