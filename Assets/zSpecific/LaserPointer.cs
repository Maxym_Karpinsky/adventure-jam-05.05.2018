﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
	[RequireComponent(typeof(LineRenderer))]
    public class LaserPointer : MonoBehaviour 
    {
		[SerializeField] private LayerMask _hitLayerMask;
		[SerializeField] private float _maxDistance = 20f;

		private LineRenderer _lineRenderer;

		private void FixedUpdate()
		{
			RaycastHit raycastHit;
			if (Physics.Raycast(this.transform.position, this.transform.forward, out raycastHit, this._maxDistance, this._hitLayerMask, QueryTriggerInteraction.Ignore))
			{
				this._lineRenderer.SetPosition(1, new Vector3(0f, 0f, (raycastHit.point - this.transform.position).magnitude));
			}
			else
			{
				this._lineRenderer.SetPosition(1, new Vector3(0f, 0f, this._maxDistance));
			}
		}

		private void Awake()
		{
			this._lineRenderer = this.GetComponent<LineRenderer>();
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{

		//}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(LaserPointer))]
    [CanEditMultipleObjects]
    public class LaserPointerEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            LaserPointer sLaserPointer = target as LaserPointer;
#pragma warning restore 0219
        }
    }
#endif
}