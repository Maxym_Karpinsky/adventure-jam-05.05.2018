﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    public class Character : LivingEntity
    {
		[SerializeField] private Transform _weaponHolder;

		[SerializeField] private Weapon _weapon;
		public Weapon _Weapon { get { return this._weapon; } }

		public void Equip(Weapon weapon)
		{
			if (this._weapon != null)
				Destroy(this._weapon.gameObject);

			this._weapon = weapon;
			this._weapon.transform.SetParent(this._weaponHolder, false);
		}

		public void Attack()
		{
			Vector3 direction = Input.mousePosition - FollowPositionCameraController.Instance_._ControlledCamera.WorldToScreenPoint(this.transform.position);
			direction.z = direction.y;
			direction = FollowPositionCameraController.Instance_._ControlledCameraEntity.transform.rotation * direction;
			direction.y = 0;

			this.transform.rotation = Quaternion.LookRotation(direction);
			this._weapon.Attack();
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{

		//}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(Character))]
    [CanEditMultipleObjects]
    public class CharacterEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            Character sCharacter = target as Character;
#pragma warning restore 0219
        }
    }
#endif
}