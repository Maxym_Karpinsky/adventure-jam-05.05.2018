﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
	[CreateAssetMenu(fileName = "s Weapon Properties", menuName = "Fighting System/Weapon Properties", order = 1)]
	public class WeaponProperties : ScriptableObject
	{
		[SerializeField] private float _damagePercentageMultiplier = 1f;
		public float _DamagePercentageMultiplier { get { return this._damagePercentageMultiplier; } }

		[SerializeField] private float _movementSpeedPercentageMultiplier = 1f;
		public float _MovementSpeedPercentageMultiplier { get { return this._movementSpeedPercentageMultiplier; } }

		[SerializeField] private MinMaxFloat _attackIntervalTime = new MinMaxFloat(0f, 100f);
		public MinMaxFloat _AttackIntervalTime { get { return this._attackIntervalTime; } }

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{

		//}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
	[CustomEditor(typeof(WeaponProperties))]
	[CanEditMultipleObjects]
	public class WeaponPropertiesEditor : Editor
	{
		private void OnEnable()
		{

		}

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();

#pragma warning disable 0219
			WeaponProperties sWeaponProperties = target as WeaponProperties;
#pragma warning restore 0219
		}
	}
#endif
}