﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

/// <summary>
/// IDEA !!!!!!!!!!!!! MAKE SUPER COLLISION PROPERTIES AND VARIOUS TYPES OF COLLISION CHECKS USING THOSE VALUES BIATCH
/// </summary>

namespace Kimo.Core
{
	[CreateAssetMenu(fileName = "s Bullet Properties", menuName = "Fighting System/Bullet Properties", order = 1)]
	public class BulletProperties : ScriptableObject
	{
		[SerializeField] private WeaponProperties _weaponProperties;

		[SerializeField] private float _damage = 30f;
		public int _Damage { get { return (int)(this._damage * this._weaponProperties._DamagePercentageMultiplier); } }

		[Range(0f, 10f)]
		[SerializeField] private float _collisionCheckBias = 0.15f;
		public float _CollisionCheckBias { get { return this._collisionCheckBias; } }

		[Range(0f, 10f)]
		[SerializeField] private float _initialCollisionCheckRadius = 0.5f;
		public float _InitialCollisionCheckRadius { get { return this._initialCollisionCheckRadius; } }

		[Range(0f, 10f)]
		[SerializeField] private float _collisionCheckRadius = 0.1f;
		public float _CollisionCheckRadius { get { return this._collisionCheckRadius; } }

		[Range(0f, 100f)]
		[SerializeField] private float _movementSpeed = 25f;
		public float _MovementSpeed { get { return this._movementSpeed * this._weaponProperties._MovementSpeedPercentageMultiplier; } }

		[SerializeField] private LayerMask _collisionLayerMask;
		public LayerMask _CollisionLayerMask { get { return this._collisionLayerMask; } }

		[SerializeField] private QueryTriggerInteraction _queryTriggerInteraction = QueryTriggerInteraction.Ignore;
		public QueryTriggerInteraction _QueryTriggerInteraction { get { return this._queryTriggerInteraction; } }

#if UNITY_EDITOR
		protected virtual void OnDrawGizmos()
		{

		}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(BulletProperties))]
    [CanEditMultipleObjects]
    public class BulletPropertiesEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            BulletProperties sBulletProperties = target as BulletProperties;
#pragma warning restore 0219
        }
    }
#endif
}