﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    public class InputController : MonoBehaviourSingleton<InputController>
    {
		[SerializeField] private Character _controlledCharacter;

		private void Update()
		{
			if (InputUtility.GetMouseButtonDown(0))
			{
				this._controlledCharacter.Attack();
			}
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{

		//}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(InputController))]
    [CanEditMultipleObjects]
    public class InputControllerEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            InputController sInputController = target as InputController;
#pragma warning restore 0219
        }
    }
#endif
}