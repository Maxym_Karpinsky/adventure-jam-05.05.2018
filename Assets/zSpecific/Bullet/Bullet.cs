﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;
using Kimo.Assistance.UTILITY;

namespace Kimo.Core
{
    public class Bullet : MonoBehaviour 
    {
		private const float BULLET_LIFE_TIME = 3f;

		[SerializeField] private BulletProperties _bulletProperties;

		private ProgressiveCooldownExecutor _poolCooldownExecutor;

		public void InitialCollisionCheck()
		{
			if (Physics.CheckSphere(this.transform.position, this._bulletProperties._InitialCollisionCheckRadius, this._bulletProperties._CollisionLayerMask, this._bulletProperties._QueryTriggerInteraction))
			{
				this._poolCooldownExecutor.Finish();
				this._poolCooldownExecutor.Execute();
			}
		}

		private void FixedUpdate()
		{
			this.transform.position += this.transform.forward * this._bulletProperties._MovementSpeed * Time.fixedDeltaTime;

			RaycastHit raycastHit;
			if (Physics.Raycast(this.transform.position, this.transform.forward, out raycastHit, this._bulletProperties._CollisionCheckBias + this._bulletProperties._MovementSpeed * Time.fixedDeltaTime, this._bulletProperties._CollisionLayerMask, this._bulletProperties._QueryTriggerInteraction) ||
				Physics.SphereCast(this.transform.position, this._bulletProperties._CollisionCheckRadius, this.transform.forward, out raycastHit, this._bulletProperties._CollisionCheckBias, this._bulletProperties._CollisionLayerMask, this._bulletProperties._QueryTriggerInteraction))
			{
				Health health;
				if (raycastHit.collider.GetComponent(out health))
				{
					health.Reduce(this._bulletProperties._Damage);

					// UserInterfaceSreenSpaceTextPopUpSpawner.Instance_.Spawn(this.transform.position, this._bulletProperties._Damage.ToCurrencyString());
					UserInterfaceWorldSpaceTextPopUpSpawner.Instance_.Spawn(this.transform.position, this._bulletProperties._Damage.ToCurrencyString());

					// ComboTracker.Instance_.Register();
				}

				this._poolCooldownExecutor.Finish();

				return;
			}

			this._poolCooldownExecutor.Execute();
		}

		private void Awake()
		{
			this._poolCooldownExecutor = new ProgressiveCooldownExecutor(new ProgressiveCooldown(BULLET_LIFE_TIME), () => ObjectPool.Instance_.PoolObject(this.gameObject));
		}

#if UNITY_EDITOR
		protected virtual void OnDrawGizmos()
		{
			GizmosUtility.DrawLine(this.transform.position, this.transform.position + this.transform.forward * (this._bulletProperties._CollisionCheckBias + this._bulletProperties._MovementSpeed * Time.fixedDeltaTime), Color.green, 0.5f);
			GizmosUtility.DrawCombinedSphere(this.transform.position, this._bulletProperties._InitialCollisionCheckRadius, Color.yellow, 0.2f);
			GizmosUtility.DrawCombinedSphere(this.transform.position, this._bulletProperties._CollisionCheckRadius, Color.red, 0.2f);
		}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(Bullet))]
    [CanEditMultipleObjects]
    public class BulletEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            Bullet sBullet = target as Bullet;
#pragma warning restore 0219
        }
    }
#endif
}