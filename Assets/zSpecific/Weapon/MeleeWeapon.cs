﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    public abstract class MeleeWeapon : Weapon 
    {
		[SerializeField] protected float damage = 50f;

#if UNITY_EDITOR
        //protected override void OnDrawGizmos()
        //{
            
        //}
#endif
    }
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(MeleeWeapon))]
    [CanEditMultipleObjects]
    public class MeleeWeaponEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            MeleeWeapon sMeleeWeapon = target as MeleeWeapon;
#pragma warning restore 0219
        }
    }
#endif
}