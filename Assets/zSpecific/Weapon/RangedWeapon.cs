﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using Kimo.Audio;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    public class RangedWeapon : Weapon 
    {
		[SerializeField] private AudioArchive _shotsArchive;
		[SerializeField] protected PoolableObject projectilePrefab;

		protected override void AttackProcess()
		{
			// TimeController.Instance_.FreezeTime(0f, 0.05f);

			// CameraController.Instance_._ShakeEffectController.Shake();
			AudioPlayer.PlayClipAtWorldPoint(this._shotsArchive.GetRandomAudioClip(), this.transform.position, AudioPlayer.AudioPlayerType.Specialty);

			ObjectPool.Instance_.DepoolObject<Bullet>(this.projectilePrefab, this.attackPoint.position, this.transform.rotation, false).InitialCollisionCheck();
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{

		//}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(RangedWeapon))]
    [CanEditMultipleObjects]
    public class RangedWeaponEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            RangedWeapon sRangedWeapon = target as RangedWeapon;
#pragma warning restore 0219
        }
    }
#endif
}