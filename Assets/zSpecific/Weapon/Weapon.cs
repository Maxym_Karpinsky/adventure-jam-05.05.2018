﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using Kimo.Audio;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
	public abstract class Weapon : MonoBehaviour
	{
		[SerializeField] protected WeaponProperties properties;

		[SerializeField] protected Transform attackPoint;
		public Transform _AttackPoint { get { return this.attackPoint; } }

		protected Cooldown attackCooldown;

		protected abstract void AttackProcess();

		public virtual void Attack()
		{
			if (this.attackCooldown.IsFinished())
			{
				this.AttackProcess();

				this.attackCooldown.Reset(this.properties._AttackIntervalTime.GetRandomValue());
			}
		}

		protected virtual void Awake()
		{
			this.attackCooldown = new Cooldown(this.properties._AttackIntervalTime.GetRandomValue(), this.properties._AttackIntervalTime.Max);
		}

#if UNITY_EDITOR
	//protected override void OnDrawGizmos()
	//{

	//}
#endif
}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(Weapon))]
    [CanEditMultipleObjects]
    public class WeaponEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            Weapon sWeapon = target as Weapon;
#pragma warning restore 0219
        }
    }
#endif
}