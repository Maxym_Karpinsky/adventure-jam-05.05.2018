﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;
using Kimo.Assistance.UTILITY;

namespace Kimo.Core
{
	public class Sword : MeleeWeapon
	{
		[SerializeField] private Transform _controllingEntity;

		[Range(0.1f, 30f)]
		[SerializeField] private float _attackRange = 2f;

		[Range(10f, 180f)]
		[SerializeField] private float _attackAngle = 45f;
		private float _projectionAngle;

		[SerializeField] private LayerMask _hitLayerMask;

		[Header("VFX")]
		[SerializeField] private PoolableObject _hitVFX;
		private float _hitVFXDuration;

		[SerializeField] private AnimationTrigger _animationTrigger;
		private ComboTracker _attackComboTracker;

		protected override void AttackProcess()
		{
			this._animationTrigger.TriggerAnimation(this._attackComboTracker._ComboValue % this._animationTrigger._AnimationsQuantity);
			this._attackComboTracker.Register();

			Collider[] colliders = Physics.OverlapSphere(this._controllingEntity.position, this._attackRange, this._hitLayerMask, QueryTriggerInteraction.Ignore);

			for (int i = 0; i < colliders.Length; i++)
			{
				Enemy enemy;
				if (colliders[i].GetComponent(out enemy))
				{
					Vector3 distanceVectorToCollider = enemy.transform.position - this._controllingEntity.position;
					distanceVectorToCollider.y = 0;

					Vector3 forwardAlignedWithColliderPoint = this._controllingEntity.forward * distanceVectorToCollider.z;
					Vector3 distanceVectorToAlignedPoint = forwardAlignedWithColliderPoint - distanceVectorToCollider;

					Vector3 alignedDistanceVectorToCollider = distanceVectorToCollider + distanceVectorToAlignedPoint.normalized * Mathf.Clamp(distanceVectorToAlignedPoint.magnitude, 0f, enemy._HitboxRadius);

					if (MathUtility.GetProjectionValue(this._controllingEntity.forward, alignedDistanceVectorToCollider) >= this._projectionAngle)
					{
						// Main
						enemy.Health_.Reduce(this.damage);

						// VFX
						ObjectPool.Instance_.DepoolObject<ParticleSystem>(this._hitVFX, enemy.transform.position, false, true, this._hitVFXDuration);
						// ObjectPool.Instance_.DepoolObject<ParticleSystem>(this._hitVFX, this._controllingEntity.position + distanceVectorToCollider - distanceVectorToCollider * enemy._HitboxRadius, false, true, this._hitVFXDuration);

						// TimeController.Instance_.FreezeTime(0f, 0.02f); // - Usually use it on super attacks.
						UserInterfaceWorldSpaceTextPopUpSpawner.Instance_.Spawn(enemy.transform.position, this.damage.ToCurrencyString());
						CameraController.Instance_._ShakeEffectController.Shake();

						// SFX

						// Misc
						GameController.Instance_._ComboTracker.Register();
					}
				}
			}
		}

		protected override void Awake()
		{
			base.Awake();

			this._projectionAngle = MathUtility.GetProjectionValue(Vector3.forward, MathUtility.GetDirectionFromAngle(this._attackAngle));

			this._hitVFXDuration = this._hitVFX.GetComponent<ParticleSystem>().main.duration;
			this._attackComboTracker = this.GetComponent<ComboTracker>();
		}

#if UNITY_EDITOR
		protected virtual void OnDrawGizmos()
		{
			GizmosUtility.DrawLine(this._controllingEntity.position, this._controllingEntity.position + MathUtility.GetDirectionFromAngle(this._attackAngle, this._controllingEntity.eulerAngles.y) * this._attackRange, Color.white);
			GizmosUtility.DrawLine(this._controllingEntity.position, this._controllingEntity.position + MathUtility.GetDirectionFromAngle(-this._attackAngle, this._controllingEntity.eulerAngles.y) * this._attackRange, Color.white);
			GizmosUtility.DrawCombinedSphere(this._controllingEntity.position, this._attackRange, Color.red, 0f);
		}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
	[CustomEditor(typeof(Sword))]
	[CanEditMultipleObjects]
	public class SwordEditor : Editor
	{
		private void OnEnable()
		{

		}

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();

#pragma warning disable 0219
			Sword sSword = target as Sword;
#pragma warning restore 0219
		}
	}
#endif
}