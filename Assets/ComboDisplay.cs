﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;
using Kimo.Assistance.UTILITY;

using TMPro;

namespace Kimo.Core
{
    public class ComboDisplay : UserInterfaceDisplay
    {
		[SerializeField] private TextMeshProUGUI _comboTextField;
		public TextMeshProUGUI _ComboTextField { get { return this._comboTextField; } }

		private void Awake()
		{
			ComboDisplayController.Instance_.Initialize(this);
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{
		//}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(ComboDisplay))]
    [CanEditMultipleObjects]
    public class ComboDisplayEditor : Editor
    {
#pragma warning disable 0219
        private ComboDisplay _sComboDisplay;
#pragma warning restore 0219

        private void OnEnable()
        {
            this._sComboDisplay = target as ComboDisplay;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
        }
    }
#endif
}