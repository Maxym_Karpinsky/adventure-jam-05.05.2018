﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    public class HackingEntity : MonoBehaviour 
    {
		[SerializeField] private UnityEvent _onHasBeenHacked;
		public UnityEvent _OnHasBeenHacked { get { return this._onHasBeenHacked; } }

		[SerializeField] private HackingData _hackingData;

		public bool HasBeenHacked { get; set; }

		public void Hack()
		{
			if (this.HasBeenHacked)
			{
				this._onHasBeenHacked.Invoke();
				return;
			}

			HackingController.Instance_.StartHacking(this._hackingData, this);
		}

		[SerializeField] private UnityEvent _onSuccess;
		public UnityEvent _OnSuccess { get { return this._onSuccess; } }

		[SerializeField] private UnityEvent _onFailure;
		public UnityEvent _OnFailure { get { return this._onFailure; } }


#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{

		//}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(HackingEntity))]
    [CanEditMultipleObjects]
    public class HackingEntityEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            HackingEntity sHackingEntity = target as HackingEntity;
#pragma warning restore 0219
        }
    }
#endif
}