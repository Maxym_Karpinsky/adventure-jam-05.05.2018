﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
	[CreateAssetMenu(fileName = "s Hacking Data", menuName = "Specific/Hacking/Hacking Data")]
    public class HackingData : ScriptableObject 
    {
		[SerializeField] private int _requiredSkillLevel;
		public int _RequiredSkillLevel { get { return this._requiredSkillLevel; } }

		[SerializeField] private float _hackingComplexity;
		public float _HackingComplexity { get { return this._hackingComplexity; } }

#if UNITY_EDITOR
        //protected override void OnDrawGizmos()
        //{
            
        //}
#endif
    }
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(HackingData))]
    [CanEditMultipleObjects]
    public class HackingDataEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            HackingData sHackingData = target as HackingData;
#pragma warning restore 0219
        }
    }
#endif
}