﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;
using Kimo.Assistance.UTILITY;

namespace Kimo.Core
{
	[RequireComponent(typeof(NavMeshAgentController), typeof(SphereCollider))]
	public class Enemy : LivingEntity
	{
		public Health Health_ { get; private set; }

		[Range(0.1f, 20f)]
		[SerializeField] private float _hitboxRadius = 1f;
		public float _HitboxRadius { get { return this._hitboxRadius; } }

		[SerializeField] private LayerMask _characterLayerMask;
		private NavMeshAgentController _navMeshAgentController;

		[SerializeField] private LayerMask _visibilityLayerMask;

		[SerializeField] private RangedWeapon _rangedWeapon;

		[Kimo.Assistance.HideInInspector]
		[SerializeField] private SphereCollider _targetTrigger;
		private Character _target;
		private WaitForSeconds _searchTargetDelay = new WaitForSeconds(0.25f);
		private Vector3 _lastVisiblePosition;
		
		private bool IsVisible(Vector3 position)
		{
			RaycastHit raycastHit;
			if (Physics.Raycast(this.transform.position, this._target.transform.position - this.transform.position, out raycastHit, this._targetTrigger.radius, this._visibilityLayerMask, QueryTriggerInteraction.Ignore))
			{
				if (this._characterLayerMask.Contains(raycastHit.collider))
				{
					this._lastVisiblePosition = position;

					return true;
				}
			}

			return false;
		}

		private Coroutine _lookForTargetProcess;

		private IEnumerator LookForTargetProcess()
		{
			while (this._target != null)
			{
				if (this.IsVisible(this._target.transform.position))
				{
					this.Attack();
				}

				yield return this._searchTargetDelay;
			}
		}

		private void LookForTarget()
		{
			if (this._lookForTargetProcess != null)
				this.StopCoroutine(this._lookForTargetProcess);

			this._lookForTargetProcess = this.StartCoroutine(this.LookForTargetProcess());
		}

		private IEnumerator AttackProcess()
		{
			while (this._target != null && this.IsVisible(this._target.transform.position))
			{
				this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(this._target.transform.position - this.transform.position), 3.5f * Time.deltaTime);

				this._rangedWeapon.Attack();

				yield return null;
			}

			this.SearchTarget();
		}

		private void Attack()
		{
			this.StopAllCoroutines();
			this.StartCoroutine(this.AttackProcess());
		}

		private IEnumerator SearchTargetProcess()
		{
			this.LookForTarget();

			while ((this.transform.position - this._lastVisiblePosition).sqrMagnitude > 10f)
			{
				this._navMeshAgentController.SetDestination(this._lastVisiblePosition);

				yield return this._searchTargetDelay;
			}
		}

		private void SearchTarget()
		{
			this.StopAllCoroutines();
			this.StartCoroutine(this.SearchTargetProcess());
		}

		protected virtual void Awake()
		{
			this.Health_ = this.GetComponent<Health>();
			this._navMeshAgentController = this.GetComponent<NavMeshAgentController>();
			this._targetTrigger = this.GetComponent<SphereCollider>();
		}

		private void OnTriggerEnter(Collider other)
		{
			if (this._characterLayerMask.Contains(other))
			{
				this._target = other.GetComponent<Character>();

				this.Attack();
			}
		}

		private void OnTriggerExit(Collider other)
		{
			if (this._characterLayerMask.Contains(other))
			{
				this._target = null;
				this._navMeshAgentController.SetDestination(this.transform.position);

				this.SearchTarget();
			}
		}

#if UNITY_EDITOR
		protected virtual void OnDrawGizmos()
		{
			
		}

		protected virtual void OnDrawGizmosSelected()
		{
			if (this._targetTrigger == null)
			{
				this._targetTrigger = this.GetComponent<SphereCollider>();
			}

			GizmosUtility.DrawCombinedSphere(this.transform.position, this._hitboxRadius, Color.yellow * 0.5f, 0f);
			GizmosUtility.DrawCombinedSphere(this.transform.position, this._targetTrigger.radius - 0.1f, Color.red * 0.5f, 0f);
		}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
	[CustomEditor(typeof(Enemy))]
	[CanEditMultipleObjects]
	public class EnemyEditor : Editor
	{
		private void OnEnable()
		{

		}

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();

#pragma warning disable 0219
			Enemy sEnemy = target as Enemy;
#pragma warning restore 0219
		}
	}
#endif
}