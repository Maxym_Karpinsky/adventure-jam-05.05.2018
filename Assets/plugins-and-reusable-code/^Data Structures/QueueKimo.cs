﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Collections.Generic
{
    public class QueueKimo<T> 
    {
        private LinkedList<T> _linkedList;

        public int Count { get { return this._linkedList.Count; } }

        public void Enqueue(T item)
        {
            this._linkedList.AddLast(item);
        }

        public T Dequeue()
        {
            T firstValue = this._linkedList.First.Value;

            this._linkedList.RemoveFirst();

            return firstValue;
        }

        public void Remove(T item)
        {
            this._linkedList.Remove(item);
        }

        public QueueKimo()
        {
            this._linkedList = new LinkedList<T>();
        }

        public T Peek()
        {
            return this._linkedList.First.Value;
        }
    }
}