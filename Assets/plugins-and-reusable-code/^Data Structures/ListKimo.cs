﻿/* Created by Max.K.Kimo */

using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Collections.Generic
{
	/// <summary>
	/// Unnecessary to implement it yet
	/// </summary>
	/// <typeparam name="T"></typeparam>
	[Serializable]
    public class ListKimo<T> : IList<T>, IEnumerable<T>
    {
		private const int DEFAULT_ARRAY_CAPACITY = 4;

		[SerializeField] private T[] _items;

		public ListKimo(int capacity = 0)
		{
			throw new System.NotImplementedException();
			this._items = new T[capacity];
		}

		public ListKimo(IEnumerable<T> collection)
		{
			throw new System.NotImplementedException();
		}

		public T this[int index] { get { return this._items[index]; } set { this._items[index] = value; } }

		public int Count { get { throw new System.NotImplementedException(); } }

		public bool IsReadOnly { get { return false; } }

		public void Add(T item)
		{
			throw new System.NotImplementedException();
		}

		public void Clear()
		{
			throw new System.NotImplementedException();
		}

		public bool Contains(T item)
		{
			throw new System.NotImplementedException();
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			throw new System.NotImplementedException();
		}

		public IEnumerator<T> GetEnumerator()
		{
			throw new System.NotImplementedException();
		}

		public int IndexOf(T item)
		{
			throw new System.NotImplementedException();
		}

		public void Insert(int index, T item)
		{
			throw new System.NotImplementedException();
		}

		public bool Remove(T item)
		{
			throw new System.NotImplementedException();
		}

		public void RemoveAt(int index)
		{
			throw new System.NotImplementedException();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			throw new System.NotImplementedException();
		}
	}
}