﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core.PCG
{
    public class MapGenerator : MonoBehaviour 
    {
		[SerializeField] private int _seed = 0;

		[Range(1, 10)]
		[SerializeField] private int _octaves = 1;

		[SerializeField] private float _scale = 23f;

		[Range(0f, 1f)]
		[SerializeField] private float _persistance = 0.5f;
		[SerializeField] private float _lacunarity = 1.1f;

		[SerializeField] private Vector2 _offset;

		[SerializeField] private AnimationCurve _meshHeightCurve;
		[SerializeField] private float _meshHeightMapMultiplier = 1f;

		[SerializeField] private MapDisplay MapDisplau;

		[SerializeField] private bool _autoUpdate = true;

		[Range(0, 6)]
		[SerializeField] private int _levelOfDetail = 0; 

		[SerializeField] private Terrain[] _regions;
		public Terrain[] _Regions { get { return this._regions; } }

		public void GenerateMap()
		{
			float[,] heightMap = Noise.GenerateNoiseGrid2D(MeshGenerator.TERRAIN_MESH_CHUNK_SIZE, MeshGenerator.TERRAIN_MESH_CHUNK_SIZE, this._seed, this._octaves, this._scale, this._persistance, this._lacunarity, this._offset);

			switch (this.MapDisplau._DisplayMode)
			{
				case MapDisplay.DisplayMode.HeightMap:

					this.MapDisplau.DrawHeightMap(heightMap);

					break;
				case MapDisplay.DisplayMode.ColorMap:

					this.MapDisplau.DrawColorMap(heightMap);

					break;
				case MapDisplay.DisplayMode.Mesh:

					this.MapDisplau.DrawMesh(heightMap, MeshGenerator.GenerateTerrainMesh(heightMap, this._meshHeightCurve, this._levelOfDetail, this._meshHeightMapMultiplier));

					break;
			}
		}

		private void Start()
		{
			this.GenerateMap();
		}

		private void OnValidate()
		{
			if (this._autoUpdate)
			{
				this.GenerateMap();
			}
		}

#if UNITY_EDITOR


		//protected override void OnDrawGizmos()
		//{

		//}
#endif
	}

	[System.Serializable]
	public struct Terrain
	{
		public enum TerrainType { Water, Ground, Mountain, SnowMountain }

		[SerializeField] private TerrainType _terrainType;
		public TerrainType _TerrainType { get { return this._terrainType; } }

		[SerializeField] private float _pickPercentage;
		public float _PickPercentage { get { return this._pickPercentage; } }

		[SerializeField] private Color _color;
		public Color _Color { get { return this._color; } }
	}
}

namespace Kimo.Core.PCG.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(MapGenerator))]
    [CanEditMultipleObjects]
    public class MapGeneratorEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            MapGenerator sMapGenerator = target as MapGenerator;
#pragma warning restore 0219
        }
    }
#endif
}