﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Core.Pathfinding;

namespace Kimo.Core.PCG
{
    public abstract class Field : MonoBehaviour 
    {
        /// <summary>
        /// Virtual position of the field in world space | NOTE: property doesn't necessary represent actual position of the gameObject
        /// </summary>
        public Vector3 WorldPosition { get; set; }

        [SerializeField] protected float generationRadius = 1f;
        /// <summary>
        /// The radius of a single node or the field
        /// </summary>
        public float _GenerationRadius { get { return this.generationRadius; } }

        protected float _generationDiameter;
        /// <summary>
        /// The diameter of a single node or the field
        /// </summary>
        public float _GenerationDiameter { get { return this._generationDiameter; } }

        /// <summary>
        /// Initializes field with new properties settings
        /// </summary>
        public abstract void Initialize();

        protected virtual void Awake()
        {
            // Initially calculate diameter of a single node
            this._generationDiameter = this.generationRadius * 2;

            this.Initialize();
        }

#if UNITY_EDITOR
        protected virtual void OnDrawGizmos()
        {
            
        }
#endif
    }
}

namespace Kimo.Core.PCG.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(Field))]
    [CanEditMultipleObjects]
    public class FieldEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            Field sField = target as Field;
#pragma warning restore 0219
        }
    }
#endif
}