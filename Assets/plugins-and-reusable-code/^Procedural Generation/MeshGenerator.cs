﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core.PCG
{
    public static class MeshGenerator
    {
		public const int VERTICES_QUANTITY_PER_TRIANGLE = 3;
		public const int VERTICES_QUANTITY_PER_SQUARE = VERTICES_QUANTITY_PER_TRIANGLE * 2;

		public const int TERRAIN_MESH_CHUNK_SIZE = 97;

		public static TerrainMeshData GenerateTerrainMesh(float[,] heightMap, AnimationCurve heightCurve, int levelOfDetail, float heightMultiplier, float distanceBetweenVertices = 1f)
		{
			int width = heightMap.GetLength(0);
			int height = heightMap.GetLength(1);

			float halfWidth = (width - 1) / 2f;
			float halfHeight = (height - 1) / 2f;

			int levelOfDetailIncrement = levelOfDetail == 0 ? 1 : levelOfDetail * 2;
			int verticesPerLine = (width - 1) / levelOfDetailIncrement + 1;

			TerrainMeshData terrainMeshData = new TerrainMeshData(verticesPerLine, verticesPerLine);

			int vertexIndex = 0;
			for (int y = 0; y < height; y += levelOfDetailIncrement)
			{
				for (int x = 0; x < width; x += levelOfDetailIncrement)
				{
					terrainMeshData._Vertices[vertexIndex] = new Vector3((x - halfWidth) * distanceBetweenVertices, heightCurve.Evaluate(heightMap[x, y]) * heightMultiplier, (y - halfHeight) * distanceBetweenVertices);

					if (x < width - 1 && y < height - 1)
					{
						terrainMeshData.AddTriangle(vertexIndex, vertexIndex + terrainMeshData._MeshVertexWidthLineLength, vertexIndex + terrainMeshData._MeshVertexWidthLineLength + 1);
						terrainMeshData.AddTriangle(vertexIndex, vertexIndex + terrainMeshData._MeshVertexWidthLineLength + 1, vertexIndex + 1);
					}

					terrainMeshData._UV[vertexIndex] = new Vector2(x / (float)width, y / (float)height);

					++vertexIndex;
				}
			}

			return terrainMeshData;
		}
	}

	[System.Serializable]
	public class TerrainMeshData
	{
		[SerializeField] private int _meshVertexWidthLineLength;
		public int _MeshVertexWidthLineLength { get { return this._meshVertexWidthLineLength; } }

		[SerializeField] private int _meshVertexHeightLineLength;
		public int _MeshVertexHeightLineLength { get { return this._meshVertexHeightLineLength; } }

		[SerializeField] private Vector3[] _vertices;
		public Vector3[] _Vertices { get { return this._vertices; } }

		[SerializeField] private int[] _triangles;
		public int[] _Triangles { get { return this._triangles; } }

		[SerializeField] private Vector2[] _uv;
		public Vector2[] _UV { get { return this._uv; } }

		private int _trianglesVerticesCount = 0;

		public void AddTriangle(int vertex1, int vertex2, int vertex3)
		{
			this._triangles[this._trianglesVerticesCount] = vertex1;
			this._triangles[this._trianglesVerticesCount + 1] = vertex2;
			this._triangles[this._trianglesVerticesCount + 2] = vertex3;

			this._trianglesVerticesCount += 3;
		}

		public Mesh CreateMesh()
		{
			Mesh mesh = new Mesh();

			mesh.name = "terrain_mesh";
			mesh.vertices = this._vertices;
			mesh.triangles = this._triangles;
			mesh.uv = this._uv;

			mesh.RecalculateNormals();

			return mesh;
		}

		public TerrainMeshData(int meshVertexWidthLineLength, int meshVertexHeightLineLength)
		{
			this._meshVertexWidthLineLength = meshVertexWidthLineLength;
			this._meshVertexHeightLineLength = meshVertexHeightLineLength;

			this._vertices = new Vector3[this._meshVertexWidthLineLength * this._meshVertexHeightLineLength];
			this._triangles = new int[(this._meshVertexWidthLineLength - 1) * (this._meshVertexHeightLineLength - 1) * MeshGenerator.VERTICES_QUANTITY_PER_SQUARE];
			this._uv = new Vector2[this._meshVertexWidthLineLength * this._meshVertexHeightLineLength];
		}
	}
}