﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;
using Kimo.Assistance.UTILITY;

namespace Kimo.Core.PCG
{
	public class MapDisplay : MonoBehaviourSingleton<MapDisplay>
	{
		public enum DisplayMode { HeightMap, ColorMap, Mesh }

		[SerializeField] private DisplayMode _displayMode;
		public DisplayMode _DisplayMode { get { return this._displayMode; } }

		[SerializeField] private Renderer _textureRenderer;
		[SerializeField] private MeshFilter _meshFilter;

		[SerializeField] private MapGenerator _mapGenerator;

		public void DrawHeightMap(float[,] heightMap)
		{
			int width = heightMap.GetLength(0);
			int height = heightMap.GetLength(1);

			Color[] pixels = new Color[width * height];

			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					pixels[y * width + x] = Color.Lerp(Color.black, Color.white, heightMap[x, y]);
				}
			}

			this._textureRenderer.sharedMaterial.mainTexture = Texture2DUtility.CreateTexture(width, height, pixels, FilterMode.Point, TextureWrapMode.Clamp);
			// this._textureRenderer.transform.localScale = new Vector3(width, 1, height);
		}

		public void DrawColorMap(float[,] heightMap)
		{
			int width = heightMap.GetLength(0);
			int height = heightMap.GetLength(1);

			Color[] pixels = new Color[width * height];

			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					for (int i = 0; i < this._mapGenerator._Regions.Length; i++)
					{
						if (heightMap[x, y] <= this._mapGenerator._Regions[i]._PickPercentage)
						{
							pixels[y * width + x] = this._mapGenerator._Regions[i]._Color;
							break;
						}
					}
				}
			}

			this._textureRenderer.sharedMaterial.mainTexture = Texture2DUtility.CreateTexture(width, height, pixels, FilterMode.Point, TextureWrapMode.Clamp);
		}

		[SerializeField] private AreaField _areaFieldPrefab;
		[SerializeField] private AreaGameAssetArchive _areaGameAssetArchive;
		[SerializeField] private AreaGameAssetArchive _darkAreaGameAssetArchive;
		[SerializeField] private int _generationStepsJump = 30;

		[SerializeField] private GameAssetArchive _frogs;

		public void DrawMesh(float[,] heightMap, TerrainMeshData terrainMeshData, int textureResolutionMultiplier = 1)
		{
			int lastGrenerationStepsJump = this._generationStepsJump;
			int currentGenerationStepsJump = lastGrenerationStepsJump;
			int numberOfConsecuentBlablaDeleteme = 0;

			int width = heightMap.GetLength(0) * textureResolutionMultiplier;
			int height = heightMap.GetLength(1) * textureResolutionMultiplier;

			Color[] pixels = new Color[width * height];

			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					for (int i = 0; i < this._mapGenerator._Regions.Length; i++)
					{
						if (heightMap[x / textureResolutionMultiplier, y / textureResolutionMultiplier] <= this._mapGenerator._Regions[i]._PickPercentage)
						{
							pixels[y * width + x] = this._mapGenerator._Regions[i]._Color;

							if (currentGenerationStepsJump > lastGrenerationStepsJump + this._generationStepsJump)
							{
								if (this._mapGenerator._Regions[i]._TerrainType == Terrain.TerrainType.Ground)
								{
									if (Random.value < 0.5f)
									{
										Instantiate(this._frogs.GetRandomAsset(), terrainMeshData._Vertices[y / textureResolutionMultiplier * width + x / textureResolutionMultiplier], Quaternion.identity);
									}

									if (numberOfConsecuentBlablaDeleteme > 4)
									{
										AreaField areaField = Instantiate(
											this._areaFieldPrefab,
											terrainMeshData._Vertices[y / textureResolutionMultiplier * width + x / textureResolutionMultiplier] + new Vector3(0f, 3.5f, 0f),
											Quaternion.identity
										);

										FieldGenerator.GenerateField(areaField, Random.value > 0.5f ? this._areaGameAssetArchive : this._darkAreaGameAssetArchive);

										lastGrenerationStepsJump = currentGenerationStepsJump + Random.Range(-this._generationStepsJump / 2, this._generationStepsJump / 2);
									}

									++numberOfConsecuentBlablaDeleteme;
								}
								else
								{
									numberOfConsecuentBlablaDeleteme = 0;
								}
							}
							break;
						}
					}

					currentGenerationStepsJump++;
				}
			}

			this._meshFilter.sharedMesh = terrainMeshData.CreateMesh();
			this._meshFilter.GetComponent<MeshRenderer>().sharedMaterial.mainTexture = Texture2DUtility.CreateTexture(width, height, pixels, FilterMode.Point, TextureWrapMode.Clamp);
			this._meshFilter.GetComponent<MeshCollider>().sharedMesh = this._meshFilter.sharedMesh;
			// this._meshRenderer.sharedMaterial.mainTexture = this.Blur(Texture2DUtility.CreateTexture(width, height, pixels, FilterMode.Bilinear, TextureWrapMode.Clamp), 2);
		}

		private Texture2D Blur(Texture2D image, int blurSize)
		{
			Texture2D blurred = new Texture2D(image.width, image.height);

			// look at every pixel in the blur rectangle
			for (int xx = 0; xx < image.width; xx++)
			{
				for (int yy = 0; yy < image.height; yy++)
				{
					float avgR = 0, avgG = 0, avgB = 0, avgA = 0;
					int blurPixelCount = 0;

					// average the color of the red, green and blue for each pixel in the
					// blur size while making sure you don't go outside the image bounds
					for (int x = xx; (x < xx + blurSize &&  x < image.width); x++)
					{
						for (int y = yy; (y < yy + blurSize &&  y < image.height); y++)
						{
							Color pixel = image.GetPixel(x, y);

							avgR += pixel.r;
							avgG += pixel.g;
							avgB += pixel.b;
							avgA += pixel.a;

							blurPixelCount++;
						}
					}

					avgR = avgR / blurPixelCount;
					avgG = avgG / blurPixelCount;
					avgB = avgB / blurPixelCount;
					avgA = avgA / blurPixelCount;

					// now that we know the average for the blur size, set each pixel to that color
					for (int x = xx; x < xx + blurSize &&  x < image.width; x++)
							for (int y = yy; y < yy + blurSize &&  y < image.height; y++)
								blurred.SetPixel(x, y, new Color(avgR, avgG, avgB, avgA));
				}
			}

			blurred.Apply();
			return blurred;
		}
 

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{

		//}
#endif
	}
}

namespace Kimo.Core.PCG.UTILITY
{
#if UNITY_EDITOR
	[CustomEditor(typeof(MapDisplay))]
	[CanEditMultipleObjects]
	public class MapDisplauEditor : Editor
	{
		private void OnEnable()
		{

		}

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();

#pragma warning disable 0219
			MapDisplay sMapDisplau = target as MapDisplay;
#pragma warning restore 0219
		}
	}
#endif
}