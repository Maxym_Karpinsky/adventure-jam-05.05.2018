﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core.PCG
{
    public class FieldGenerator : MonoBehaviour 
    {
		[SerializeField] private Field3D _testField;
		[SerializeField] private GameAssetArchive _testGameAssetArchive;

        /// <summary>
        /// Generates nodes field using set properties in field object
        /// To set/change generation properties use `SetFieldGenerationProperties()` on field object
        /// </summary>
        /// <param name="field">Field2D type object</param>
        public void GenerateField(Field2D field, GameAssetArchive gameAssetArchive)
        {
            if (field.Grid_ == null)
                field.Initialize();

			gameAssetArchive.Initialize();
		}
        
        public void GenerateField(Field3D field, GameAssetArchive gameAssetArchive)
        {
            if (field.Grid_ == null)
                field.Initialize();

			gameAssetArchive.Initialize();

			for (int y = 0; y < field.YUnitQuantity; y++)
			{
				for (int x = 0; x < field.XUnitQuantity; x++)
				{
					for (int z = 0; z < field.ZUnitQuantity; z++)
					{
						GameAsset gameAsset = gameAssetArchive.GetRandomAsset();

						if (gameAsset != null)
						{
							switch (gameAsset._State)
							{
								case GameAsset.AvailabilityState.Free:
									break;
								case GameAsset.AvailabilityState.Neutral:
									break;
								case GameAsset.AvailabilityState.Obstacle:

									Instantiate(gameAsset.gameObject, field.Grid_[x, y, z].WorldPosition, Quaternion.identity, field.transform);

									break;
							}
						}
					}
				}
			}
        }

		[SerializeField] private AreaField _testAreaField;
		[SerializeField] private AreaGameAssetArchive _testAreaGameAssetArchive;

		public static void GenerateField(AreaField field, AreaGameAssetArchive areaGameAssetArchive)
		{
			areaGameAssetArchive.Initialize();

			for (int i = 0; i < field._GenerationQuantity; i++)
			{
				AreaGameAsset areaGameAsset = areaGameAssetArchive.GetRandomAsset();

				if (areaGameAsset != null)
				{
					Vector2 randomUnitCirclePosition = Random.insideUnitCircle * field._GenerationRadius;
					Vector3 randomSpawnPosition = new Vector3(field.transform.position.x + randomUnitCirclePosition.x, field.transform.position.y, field.transform.position.z + randomUnitCirclePosition.y);

					RaycastHit groundRaycastHit;
					if (!Physics.CheckBox(randomSpawnPosition, areaGameAsset._RelativeBoundsSize, Quaternion.identity, areaGameAsset._GenerationRestrictedLayerMask, QueryTriggerInteraction.Ignore) && 
						Physics.Raycast(
							randomSpawnPosition, 
							Vector3.down, 
							out groundRaycastHit, 
							float.MaxValue, 
							field._GenerationLayerMask, 
							QueryTriggerInteraction.Ignore
						))
					{
						GameObject spawnedGameObject = Instantiate(areaGameAsset.gameObject, groundRaycastHit.point, Quaternion.Euler(0f, 360f * Random.value, 0f), field.transform);

						float rnv = Random.value;
						spawnedGameObject.transform.localScale = rnv > 0.7f ? Vector3.one * rnv : Vector3.one * 0.7f;
					}
				}
			}
		}

		private void Start()
		{
			this.GenerateField(this._testField, this._testGameAssetArchive);
			FieldGenerator.GenerateField(this._testAreaField, this._testAreaGameAssetArchive);
		}

#if UNITY_EDITOR
		private void OnDrawGizmos()
        {
            
        }
#endif
    }
}

namespace Kimo.Core.PCG.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(FieldGenerator))]
    [CanEditMultipleObjects]
    public class FieldGeneratorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            FieldGenerator sField = target as FieldGenerator;
#pragma warning restore 0219
        }
    }
#endif
}