﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Core.Pathfinding;

namespace Kimo.Core.PCG
{
    public class Field3D : Field
    {
        [SerializeField] protected int xUnitQuantity = 10, zUnitQuantity = 10, yUnitQuantity = 1;
        /// <summary>
        /// Quantity of nodes per X coordinate unit
        /// </summary>
        public int XUnitQuantity { get { return this.xUnitQuantity; } }
        /// <summary>
        /// Quantity of nodes per Z coordinate unit
        /// </summary>
        public int ZUnitQuantity { get { return this.zUnitQuantity; } }
        /// <summary>
        /// Quantity of nodes per Y coordinate unit
        /// </summary>
        public int YUnitQuantity { get { return this.yUnitQuantity; } }

        [SerializeField] protected float nodeHeight = 10f;
        /// <summary>
        /// Height of a single node of the field
        /// </summary>
        public float NodeHeight { get { return this.nodeHeight; } }

        /// <summary>
        /// Sets field initialization/generation properties
        /// Call `Initialize()` to apply changes to `Grid`
        /// </summary>
        /// <param name="nodeRadius">Radius of a single node</param>
        public void SetFieldGenerationProperties(float nodeRadius) { this.generationRadius = nodeRadius; this._generationDiameter = this.generationRadius * 2; }

        /// <summary>
        /// Sets field initialization/generation properties
        /// Call `Initialize()` to apply changes to `Grid`
        /// </summary>
        /// <param name="xUnitQuantity">Quantity of nodes on X coordinate</param>
        /// <param name="zUnitQuantity">Quantity of nodes on Z coordinate</param>
        public void SetFieldGenerationProperties(int xUnitQuantity, int zUnitQuantity) { this.xUnitQuantity = xUnitQuantity; this.zUnitQuantity = zUnitQuantity; }

        /// <summary>
        /// Sets field initialization/generation properties
        /// Call `Initialize()` to apply changes to `Grid`
        /// </summary>
        /// <param name="xUnitQuantity">Quantity of nodes on X coordinate</param>
        /// <param name="zUnitQuantity">Quantity of nodes on Z coordinate</param>
        /// <param name="nodeRadius">Radius of a single node</param>
        public void SetFieldGenerationProperties(int xUnitQuantity, int zUnitQuantity, float nodeRadius) { this.SetFieldGenerationProperties(xUnitQuantity, zUnitQuantity); this.generationRadius = nodeRadius; this._generationDiameter = this.generationRadius * 2; }

        /// <summary>
        /// Sets field initialization/generation properties
        /// Call `Initialize()` to apply changes to `Grid`
        /// </summary>
        /// <param name="xUnitQuantity">Quantity of nodes on X coordinate</param>
        /// <param name="zUnitQuantity">Quantity of nodes on Z coordinate</param>
        /// <param name="nodeRadius">Radius of a single node</param>
        /// <param name="nodeHeight">Height of a single node | Height of Y level</param>
        public void SetFieldGenerationProperties(int xUnitQuantity, int zUnitQuantity, float nodeRadius, float nodeHeight) { this.SetFieldGenerationProperties(xUnitQuantity, zUnitQuantity, nodeRadius); this.nodeHeight = nodeHeight; }

        /// <summary>
        /// Sets field initialization/generation properties
        /// Call `Initialize()` to apply changes to `Grid`
        /// </summary>
        /// <param name="xUnitQuantity">Quantity of nodes on X coordinate</param>
        /// <param name="zUnitQuantity">Quantity of nodes on Z coordinate</param>
        /// <param name="zUnitQuantity">Quantity of nodes on Y coordinate</param>
        public void SetFieldGenerationProperties(int xUnitQuantity, int zUnitQuantity, int yUnitQuantity) { this.xUnitQuantity = xUnitQuantity; this.zUnitQuantity = zUnitQuantity; this.yUnitQuantity = yUnitQuantity; }

        /// <summary>
        /// Sets field initialization/generation properties
        /// Call `Initialize()` to apply changes to `Grid`
        /// </summary>
        /// <param name="xUnitQuantity">Quantity of nodes on X coordinate</param>
        /// <param name="zUnitQuantity">Quantity of nodes on Z coordinate</param>
        /// <param name="zUnitQuantity">Quantity of nodes on Y coordinate</param>
        /// <param name="nodeRadius">Radius of a single node</param>
        public void SetFieldGenerationProperties(int xUnitQuantity, int zUnitQuantity, int yUnitQuantity, float nodeRadius) { this.SetFieldGenerationProperties(xUnitQuantity, zUnitQuantity, yUnitQuantity); this.generationRadius = nodeRadius; this._generationDiameter = this.generationRadius * 2; }

        /// <summary>
        /// Sets field initialization/generation properties
        /// Call `Initialize()` to apply changes to `Grid`
        /// </summary>
        /// <param name="xUnitQuantity">Quantity of nodes on X coordinate</param>
        /// <param name="zUnitQuantity">Quantity of nodes on Z coordinate</param>
        /// <param name="yUnitQuantity">Quantity of nodes on Y coordinate</param>
        /// <param name="nodeRadius">Radius of a single node</param>
        /// <param name="nodeHeight">Height of a single node | Height of Y level</param>
        public void SetFieldGenerationProperties(int xUnitQuantity, int zUnitQuantity, int yUnitQuantity, float nodeRadius, float nodeHeight) { this.SetFieldGenerationProperties(xUnitQuantity, zUnitQuantity, yUnitQuantity, nodeRadius); this.nodeHeight = nodeHeight; }

        /// <summary>
        /// 3D grid of nodes that represents field
        /// </summary>
        public Node[,,] Grid_ { get; private set; } 

        /// <summary>
        /// Initializes field with new properties settings
        /// </summary>
        public override void Initialize()
        {
            this.Grid_ = new Node[this.xUnitQuantity, this.yUnitQuantity, this.zUnitQuantity];

			for (int x = 0; x < this.xUnitQuantity; x++)
			{
				for (int z = 0; z < this.zUnitQuantity; z++)
				{
					for (int y = 0; y < this.yUnitQuantity; y++)
					{
						this.Grid_[x, y, z] = new Node(new Vector3(x * this._generationDiameter + this.generationRadius, y * this.nodeHeight, z * this._generationDiameter + this.generationRadius));
					}
				}
			}
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            
        }
#endif
    }
}

namespace Kimo.Core.PCG.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(Field3D))]
    [CanEditMultipleObjects]
    public class Field3DEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            Field3D sField = target as Field3D;
#pragma warning restore 0219
        }
    }
#endif
}