﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core.PCG
{
	public static class Noise
	{
		public static float[,] GenerateNoiseGrid2D(int width, int height, int seed, int octaves, float scale, float persistance, float lacunarity, Vector2 offset)
		{
			float[,] noiseGrid = new float[width, height];

			System.Random prng = new System.Random(seed);

			Vector2[] octaveOffsets = new Vector2[octaves];
			for (int i = 0; i < octaveOffsets.Length; i++)
				octaveOffsets[i] = new Vector2(prng.Next(-100000, 100000) + offset.x, prng.Next(-100000, 100000) + offset.y);

			float maxNoiseHeight = float.MinValue;
			float minNoiseHeight = float.MaxValue;

			float halfWidth = width / 2f;
			float halfHeight = height / 2f;

			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					float amplitude = 1f;
					float frequency = 1f;

					float noiseHeight = 0f;

					for (int i = 0; i < octaves; i++)
					{
						// * 2f - 1f >>> Mathf.PerlinNoise() gives value between 0 and 1 - that was used to transform value to -1 and 1 system --- changed to use extension
						float perlinValue = Mathf.PerlinNoise(
							((x - halfWidth) / scale + octaveOffsets[i].x) * frequency, 
							((y - halfHeight) / scale + octaveOffsets[i].y) * frequency
						).ToNegativePositiveInterval(1f); // Useless step as of now

						noiseHeight += perlinValue * amplitude;

						amplitude *= persistance;
						frequency *= lacunarity;
					}

					if (noiseHeight > maxNoiseHeight)
						maxNoiseHeight = noiseHeight;
					else if (noiseHeight < minNoiseHeight)
						minNoiseHeight = noiseHeight;

					noiseGrid[x, y] = noiseHeight;
				}
			}

			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					noiseGrid[x, y] = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, noiseGrid[x, y]);
				}
			}

			return noiseGrid;
		}
	}
}