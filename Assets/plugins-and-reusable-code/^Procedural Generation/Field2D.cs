﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Core.Pathfinding;
using Kimo.Assistance;

namespace Kimo.Core.PCG
{
    [System.Serializable]
    public class Field2D : Field 
    {
        [SerializeField] protected int xUnitQuantity = 10, yUnitQuantity = 10;
        /// <summary>
        /// Quantity of nodes per X coordinate unit
        /// </summary>
        public int XUnitQuantity { get { return this.xUnitQuantity; } }
        /// <summary>
        /// Quantity of nodes per Y coordinate unit
        /// </summary>
        public int YUnitQuantity { get { return this.yUnitQuantity; } }

        /// <summary>
        /// 2D grid of nodes that represents field
        /// </summary>
        public Node[,] Grid_ { get; private set; }

        /// <summary>
        /// Initializes field with new properties settings
        /// </summary>
        public override void Initialize()
        {
            this.Grid_ = new Node[this.xUnitQuantity, this.yUnitQuantity];
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            
        }
#endif
    }
}

namespace Kimo.Core.PCG.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(Field2D))]
    [CanEditMultipleObjects]
    public class Field2DEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            Field2D sField2D = target as Field2D;
#pragma warning restore 0219
        }
    }
#endif
}