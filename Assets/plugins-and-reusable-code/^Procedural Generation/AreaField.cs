﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;
using Kimo.Assistance.UTILITY;

namespace Kimo.Core.PCG
{
	public class AreaField : Field
	{
		[SerializeField] private int _generationQuantity = 20;
		public int _GenerationQuantity { get { return this._generationQuantity; } }

		[SerializeField] private LayerMask _generatiorLayerMask;
		public LayerMask _GenerationLayerMask { get { return this._generatiorLayerMask; } }

		public override void Initialize()
		{

		}


#if UNITY_EDITOR

		protected override void OnDrawGizmos()
		{
			if (Selection.activeTransform == this.transform)
			{
				GizmosUtility.DrawCombinedSphere(this.transform.position, this.generationRadius, Color.cyan);
			}
		}
#endif
	}
}

namespace Kimo.Core.PCG.UTILITY
{
#if UNITY_EDITOR
	[CustomEditor(typeof(AreaField))]
	[CanEditMultipleObjects]
	public class AreaFieldEditor : Editor
	{
		private void OnEnable()
		{

		}

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();

#pragma warning disable 0219
			AreaField sAreaField = target as AreaField;
#pragma warning restore 0219
		}
	}
#endif
}