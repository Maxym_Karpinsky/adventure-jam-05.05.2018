﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core
{
    public class LookAtCameraController : CameraController
    {
		private void LateUpdate()
		{
			this.controlledCamera.transform.LookAt(this.target);
		}

#if UNITY_EDITOR
		protected virtual void OnDrawGizmos()
        {
            
        }
#endif
    }
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(LookAtCameraController))]
    [CanEditMultipleObjects]
    public class LookAtCameraControllerEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            LookAtCameraController sLookAtCameraController = target as LookAtCameraController;
#pragma warning restore 0219
        }
    }
#endif
}