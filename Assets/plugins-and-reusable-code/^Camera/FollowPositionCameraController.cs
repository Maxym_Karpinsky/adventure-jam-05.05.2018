﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    public class FollowPositionCameraController : CameraController 
    {
		[Header("Specific")]
		[SerializeField] private float _movementSmoothingFactor = 2.5f;

		private void LateUpdate()
		{
			this.controlledCameraEntity.transform.position = Vector3.Lerp(this.controlledCameraEntity.transform.position, this.target.transform.position - this.initialOffsetToTarget, Time.deltaTime * this._movementSmoothingFactor);
		}

#if UNITY_EDITOR
		protected virtual void OnDrawGizmos()
        {
            
        }
#endif
    }
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(FollowPositionCameraController))]
    [CanEditMultipleObjects]
    public class FollowPositionCameraControllerEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            FollowPositionCameraController sFollowPositionCameraController = target as FollowPositionCameraController;
#pragma warning restore 0219
        }
    }
#endif
}