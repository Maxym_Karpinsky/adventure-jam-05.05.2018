﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    [System.Serializable]
    public abstract class CameraController : MonoBehaviourSingleton<CameraController>
	{
		[Header("Control")]
		[SerializeField] protected Transform controlledCameraEntity;
		public Transform _ControlledCameraEntity { get { return this.controlledCameraEntity; } }

		[SerializeField] protected Camera controlledCamera;
		public Camera _ControlledCamera { get { return this.controlledCamera; } }

		[SerializeField] protected Transform target;
		public Transform _Target { get { return this.target; } }

		protected Vector3 initialOffsetToTarget;
		public Vector3 _InitialOffsetToTarget { get { return this.initialOffsetToTarget; } }

		[Header("Effects")]
		[SerializeField] private ShakeEffectController _shakeEffectController;
		public ShakeEffectController _ShakeEffectController { get { return this._shakeEffectController; } }

		protected override void Awake()
		{
			base.Awake();

			this.initialOffsetToTarget = this.target.transform.position - this.controlledCamera.transform.position;
		}
	}
}