﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
	[System.Serializable]
	[CreateAssetMenu(fileName = "s Shake Effect Properties", menuName = "Effects Properties/Shake Effect Properties")]
	public class ShakeEffectProperties : ScriptableObject
	{
		[Header("Behavioural Settings")]
		[Tooltip("Range from which randomly will be taken value that will allow to get the direction of first positional movement on shake.")]
		[SerializeField]
		[MinMaxFloatRange(0f, 360f)]
		private MinMaxFloat _startAngle = new MinMaxFloat(0f, 360f);
		public MinMaxFloat _StartAngle { get { return this._startAngle; } }

		[Tooltip("Max distance of shake. Strength of shake.")]
		[SerializeField] private float _amplitude = 1f;
		public float _Amplitude { get { return this._amplitude; } }

		[Tooltip("Speed of moving from current to next point during shake")]
		[SerializeField] private float _speed = 5f;
		public float _Speed { get { return this._speed; } }

		[Tooltip("The duration of shake. Doesn't represent seconds.")]
		[SerializeField] private float _duration = 0.3f;
		public float _Duration { get { return this._duration; } }

		[Header("Noise | Position Range Randomness")]
		[Range(0f, 1f), Tooltip("Value that represents - how much randomization should be applied to choosing the next point while shaking.")]
		[SerializeField] private float _noisePercentage = 0.3f;
		public float _NoisePercentage { get { return this._noisePercentage; } }

		[Tooltip("Wether to choose use Mathf.Perlin or Random.value to get random value between 0 and 1.")]
		[SerializeField] private bool _usePerlinNoise;
		public bool _UsePerlinNoise { get { return this._usePerlinNoise; } }

		[Header("Position")]
		[Tooltip("Pattern that shake follows during moving from one point to another. 0 - initial point. 1 - target point.")]
		[SerializeField] private AnimationCurve _positionCurve;
		public AnimationCurve _PositionCurve { get { return this._positionCurve; } }

		[Header("Rotation")]
		[Range(0f, 90f), Tooltip("Max rotation of shake. Strength of shake.")]
		[SerializeField] private float _rotationAmplitude = 10f;
		public float _RotationAmplitude { get { return this._rotationAmplitude; } }

		[Range(0f, 1f), Tooltip("Value represents - how much of the choosen/generated rotation should be applied to shake.")]
		[SerializeField] private float _rotationPercentage;
		public float _RotationPercentage { get { return this._rotationPercentage; } }

		[Tooltip("Pattern that shake follows during moving from one rotation to another. 0 - initial rotation. 1 - target rotation.")]
		[SerializeField] private AnimationCurve _rotationCurve;
		public AnimationCurve _RotationCurve { get { return this._rotationCurve; } }

		[Header("Damping | Smoothing | Ease")]
		[Range(0f, 1f), Tooltip("Value represents - how much of the damping, slowing, ease should be evaluated and apllied to amplitude.")]
		[SerializeField] private float _amplitudeDampingPercentage = 1f;
		public float _AmplitudeDampingPercentage { get { return this._amplitudeDampingPercentage; } }

		[Tooltip("Pattern that defines how much of the damping is applied at the moment to amplitude. 1 - no damping is applied. 0 - full damping is applied.")]
		[SerializeField] private AnimationCurve _amplitudeDampingCurve;
		public AnimationCurve _AmplitudeDampingCurve { get { return this._amplitudeDampingCurve; } }

		[Tooltip("Should speed be affected by damping as well? Damping is apllied to amplitude by default.")]
		[SerializeField] private bool _affectSpeedByDamping;
		public bool _AffectSpeedByDamping { get { return this._affectSpeedByDamping; } }

		public ShakeEffectProperties(
			MinMaxFloat startAngle,
			float amplitude,
			float speed,
			float duration,
			float noisePercentage,
			bool usePerlinNoise,
			AnimationCurve positionCurve,
			float rotationAmplitude,
			float rotationPercentage,
			AnimationCurve rotationCurve,
			float amplitudeDampingPercentage,
			AnimationCurve amplitudeDampingCurve,
			bool affectSpeedByDamping
		)
		{
			this._startAngle = startAngle;
			this._amplitude = amplitude;
			this._speed = speed;
			this._duration = duration;
			this._noisePercentage = noisePercentage;
			this._usePerlinNoise = usePerlinNoise;
			this._positionCurve = positionCurve;
			this._rotationAmplitude = rotationAmplitude;
			this._rotationPercentage = rotationPercentage;
			this._rotationCurve = rotationCurve;
			this._amplitudeDampingPercentage = amplitudeDampingPercentage;
			this._amplitudeDampingCurve = amplitudeDampingCurve;
			this._affectSpeedByDamping = affectSpeedByDamping;
		}
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(ShakeEffectProperties))]
    [CanEditMultipleObjects]
    public class ShakeEffectPropertiesEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            ShakeEffectProperties sShakeEffectProperties = target as ShakeEffectProperties;
#pragma warning restore 0219
        }
    }
#endif
}