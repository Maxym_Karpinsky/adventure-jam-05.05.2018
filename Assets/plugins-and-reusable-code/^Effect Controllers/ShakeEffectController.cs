﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
	[System.Serializable]
    public class ShakeEffectController
    {
		private Coroutine _shakeProcessCoroutine;

		[SerializeField] private MonoBehaviour _invoker;
		public MonoBehaviour Invoker
		{
			get { return this._invoker; }
			set
			{
				this.Stop();
				this._invoker = value;
			}
		}

		public Transform AffectedEntity;

		[SerializeField] private ShakeEffectProperties _defaultShakeEffectProperties;

		public void Shake()
		{
			this.Shake(this._defaultShakeEffectProperties);
		}

		public void Shake(ShakeEffectProperties shakeEffectProperties)
		{
			this.Stop();

			this._shakeProcessCoroutine = this._invoker.StartCoroutine(this.ShakeProcess(shakeEffectProperties));
		}

		private Vector3 _initialPosition;
		private Quaternion _initialRotation;

		public void Stop()
		{
			if (this._shakeProcessCoroutine != null)
				this._invoker.StopCoroutine(this._shakeProcessCoroutine);

			this.AffectedEntity.transform.localPosition = this._initialPosition;
			this.AffectedEntity.transform.localRotation = this._initialRotation;

			this._shakeProcessCoroutine = null;
		}

		public IEnumerator ShakeProcess(ShakeEffectProperties shakeEffectProperties)
		{
			float progressPercentage = 0f;
			float movementPercentage = 0f;

			float angleRad = shakeEffectProperties._StartAngle.GetRandomValue() * Mathf.Deg2Rad - Mathf.PI;

			// Position
			this._initialPosition = this.AffectedEntity.transform.localPosition;

			Vector3 previousPosition = this._initialPosition;
			Vector3 shakePositionRange = new Vector3(Mathf.Cos(angleRad), Mathf.Sin(angleRad)) * shakeEffectProperties._Amplitude;
			Vector3 targetPosition = this._initialPosition + shakePositionRange;

			// Rotation
			this._initialRotation = this.AffectedEntity.transform.localRotation;

			Quaternion previousRotation = this._initialRotation;
			Quaternion targetRotation = this._initialRotation * Quaternion.Euler(new Vector3(shakePositionRange.y, shakePositionRange.x).normalized * (shakeEffectProperties._RotationAmplitude * shakeEffectProperties._RotationPercentage));

			float movementDistance = Vector3.Distance(targetPosition, previousPosition);

			while (progressPercentage <= 1f)
			{
				float amplitudeDampingFactor = shakeEffectProperties._AmplitudeDampingCurve.Evaluate(progressPercentage * shakeEffectProperties._AmplitudeDampingPercentage);

				progressPercentage += Time.deltaTime / shakeEffectProperties._Duration;

				float movementPercentageIncrease = Time.deltaTime * shakeEffectProperties._Speed / movementDistance;
				if (shakeEffectProperties._AffectSpeedByDamping)
					movementPercentageIncrease *= amplitudeDampingFactor;

				movementPercentage += movementPercentageIncrease;

				this.AffectedEntity.transform.localPosition = Vector3.Lerp(previousPosition, targetPosition, shakeEffectProperties._PositionCurve.Evaluate(Mathf.Clamp01(movementPercentage)));
				this.AffectedEntity.transform.localRotation = Quaternion.Lerp(previousRotation, targetRotation, shakeEffectProperties._RotationCurve.Evaluate(Mathf.Clamp01(movementPercentage)));

				if (movementPercentage >= 1f)
				{
					float noiseAngle;
					if (shakeEffectProperties._UsePerlinNoise)
						noiseAngle = (Mathf.PerlinNoise(Time.time, Time.time) - 0.5f) * Mathf.PI;
					else
						noiseAngle = (Random.value - 0.5f) * Mathf.PI;

					angleRad += Mathf.PI + noiseAngle * shakeEffectProperties._NoisePercentage;

					previousPosition = this.AffectedEntity.transform.localPosition;
					shakePositionRange = new Vector3(Mathf.Cos(angleRad), Mathf.Sin(angleRad)) * (shakeEffectProperties._Amplitude * amplitudeDampingFactor);
					targetPosition = this._initialPosition + shakePositionRange;

					previousRotation = this.AffectedEntity.transform.localRotation;
					targetRotation = this._initialRotation * Quaternion.Euler(new Vector3(shakePositionRange.y, shakePositionRange.x).normalized * (shakeEffectProperties._RotationAmplitude * amplitudeDampingFactor * shakeEffectProperties._RotationPercentage));

					movementDistance = Vector3.Distance(targetPosition, previousPosition);
					movementPercentage = 0f;
				}

				yield return null;
			}

			this.AffectedEntity.transform.localPosition = this._initialPosition;
			this.AffectedEntity.transform.localRotation = this._initialRotation;
		}

		public ShakeEffectController(MonoBehaviour invoker, Transform affectedEntity, ShakeEffectProperties defaultShakeEffectProperties)
		{
			this._invoker = invoker;
			this.AffectedEntity = affectedEntity;
			this._defaultShakeEffectProperties = defaultShakeEffectProperties;
		}
    }
}