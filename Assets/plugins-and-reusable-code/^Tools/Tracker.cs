﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;
using Kimo.Assistance.UTILITY;
using System;

namespace Kimo.Core
{
	[System.Serializable]
    public class Tracker<TValue>
    {
		private TValue _defaultValue;
		public TValue Value { get; set; }

		public void Reset()
		{
			this.Value = this._defaultValue;
		}

		public void Reset(MonoBehaviour invoker, float delayTime)
		{
			invoker.StartCoroutine(CoroutineUtility.DelayedActionProcess(new WaitForSeconds(delayTime), this.Reset));
		}

		public void Reset(MonoBehaviour invoker, float delayTime, Action action)
		{
			action += this.Reset;
			invoker.StartCoroutine(CoroutineUtility.DelayedActionProcess(new WaitForSeconds(delayTime), action));
		}

		public void Reset(MonoBehaviour invoker, YieldInstruction yieldInstruction)
		{
			invoker.StartCoroutine(CoroutineUtility.DelayedActionProcess(yieldInstruction, this.Reset));
		}

		public void Reset(MonoBehaviour invoker, YieldInstruction yieldInstruction, Action action)
		{
			action += this.Reset;
			invoker.StartCoroutine(CoroutineUtility.DelayedActionProcess(yieldInstruction, action));
		}

		public void Reset(MonoBehaviour invoker, Cooldown cooldown, bool resetCooldown = false)
		{
			invoker.StartCoroutine(CoroutineUtility.DelayedActionProcess(cooldown, this.Reset, resetCooldown));
		}

		public void Reset(MonoBehaviour invoker, Cooldown cooldown, Action action, bool resetCooldown = false)
		{
			action += this.Reset;
			invoker.StartCoroutine(CoroutineUtility.DelayedActionProcess(cooldown, action, resetCooldown));
		}

		public Tracker(TValue defaultValue)
		{
			this._defaultValue = defaultValue;
			this.Reset();
		}

		public Tracker(TValue defaultValue, TValue value_)
		{
			this._defaultValue = defaultValue;
			this.Value = value_;
		}
	}
}