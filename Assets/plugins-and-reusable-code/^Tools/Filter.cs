﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
	public class Filter<TEqualityElement>
	{
		public TEqualityElement DefaultEqualityElement_ { get; private set; }
		public TEqualityElement EqualityElement { get; set; }

		public Filter(TEqualityElement defaultEqualityElement)
		{
			this.DefaultEqualityElement_ = defaultEqualityElement;
			this.EqualityElement = defaultEqualityElement;
		}

		public Filter(TEqualityElement defaultEqualityElement, TEqualityElement equalityElement)
		{
			this.DefaultEqualityElement_ = defaultEqualityElement;
			this.EqualityElement = equalityElement;
		}

		public virtual bool IsEligible(TEqualityElement other)
		{
			return this.EqualityElement.Equals(this.DefaultEqualityElement_) || this.EqualityElement.Equals(other);
		}
	}
}