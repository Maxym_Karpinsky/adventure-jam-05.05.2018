﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;
using Kimo.Assistance.UTILITY;

namespace Kimo.Core
{
    public class TimeController : MonoBehaviourSingleton<TimeController>
    {
		private bool _timeIsFrozen;

		public void FreezeTime(float timeScale, float time)
		{
			if (!this._timeIsFrozen)
			{
				this._timeIsFrozen = true;

				float previousTimeScale = Time.timeScale;
				this.StartCoroutine(CoroutineUtility.DelayedActionProcess(new WaitForSecondsRealtime(time), () => { Time.timeScale = previousTimeScale; this._timeIsFrozen = false; }));

				Time.timeScale = timeScale;
			}
		}

		private bool _timePauseRegistered;
		private float _prePauseTimeScale;

		public void PauseTime()
		{
			this._timePauseRegistered = true;

			this._prePauseTimeScale = Time.timeScale;
			Time.timeScale = 0;
		}

		public void ResumeTime()
		{
			if (this._timePauseRegistered)
			{
				Time.timeScale = this._prePauseTimeScale;

				this._timePauseRegistered = false;
			}
		}

#if UNITY_EDITOR
        //protected override void OnDrawGizmos()
        //{
            
        //}
#endif
    }
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(TimeController))]
    [CanEditMultipleObjects]
    public class TimeControllerEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            TimeController sTimeController = target as TimeController;
#pragma warning restore 0219
        }
    }
#endif
}