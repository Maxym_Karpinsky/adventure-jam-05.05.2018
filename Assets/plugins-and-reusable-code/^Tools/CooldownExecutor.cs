﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
	public abstract class CooldownExecutor<T> //TODO: split into multiple files
		where T : ICooldown
	{
		protected T _cooldown;
		public T _Cooldown { get { return this._cooldown; } }

		protected Action _action;

		public abstract bool Execute();
		public abstract void Finish();

		public CooldownExecutor(T cooldown, Action action)
		{ 
			this._cooldown = cooldown;
			this._action = action;
		}
	}

    public sealed class CooldownExecutor : CooldownExecutor<ICooldown>
    {
		public CooldownExecutor(ICooldown cooldown, Action action) : base(cooldown, action)
		{
		}

		public override bool Execute()
		{
			if (this._cooldown.IsFinished())
			{
				this.Finish();

				return true;
			}

			return false;
		}

		public override void Finish()
		{
			this._cooldown.Reset();
			this._action.Invoke();
		}
	}

	public sealed class ProgressiveCooldownExecutor : CooldownExecutor<IProgressiveCooldown>
	{
		public ProgressiveCooldownExecutor(IProgressiveCooldown cooldown, Action action) : base(cooldown, action)
		{
		}

		public override bool Execute()
		{
			this._cooldown.Progress();

			if (this._cooldown.IsFinished())
			{
				this.Finish();

				return true;
			}

			return false;
		}

		public override void Finish()
		{
			this._cooldown.Reset();
			this._action.Invoke();
		}
	}

	public interface ICooldown
	{
		bool IsFinished();
		void Reset();
	}

	public interface IProgressiveCooldown : ICooldown
	{
		void Progress();
	}

	public class Cooldown : ICooldown
	{
		public float TagetElapsedTime_ { get; private set; }
		public float PreviousTime_ { get; private set; }

		public virtual bool IsFinished()
		{
			return Time.time - this.PreviousTime_ >= this.TagetElapsedTime_;
		}

		public virtual void Reset()
		{
			this.PreviousTime_ = Time.time;
		}

		public virtual void Reset(float newTargetElapsedTime)
		{
			this.TagetElapsedTime_ = newTargetElapsedTime;
			this.Reset();
		}

		public virtual void Finish()
		{
			this.PreviousTime_ = Time.time - this.TagetElapsedTime_;
		}

		public virtual float GetElapsedTime()
		{
			return Time.time - this.PreviousTime_;
		}
		
		public virtual float GetTimeLeft()
		{
			return this.TagetElapsedTime_ - this.GetElapsedTime();
		}

		public Cooldown(float targetElapsedTime, float initialTime = 0)
		{
			this.TagetElapsedTime_ = targetElapsedTime;
			this.PreviousTime_ = Time.time - initialTime;
		}
	}

	public class ProgressiveCooldown : IProgressiveCooldown
	{
		public float TagetElapsedTime_ { get; private set; }
		public float ElapsedTime_ { get; private set; }

		public virtual void Progress()
		{
			this.ElapsedTime_ += Time.deltaTime;
		}

		public virtual bool IsFinished()
		{
			return this.ElapsedTime_ >= this.TagetElapsedTime_;
		}

		public virtual void Reset()
		{
			this.ElapsedTime_ -= this.TagetElapsedTime_;
		}

		public virtual void Finish()
		{
			this.ElapsedTime_ = this.TagetElapsedTime_;
		}

		public ProgressiveCooldown(float targetElapsedTime, float initialTime = 0)
		{
			this.TagetElapsedTime_ = targetElapsedTime;
			this.ElapsedTime_ = initialTime;
		}
	}
}