﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
	public class LazyField<TField>
	{
		private Func<TField> _valueInitializationFunction;

		private TField _value;
		public TField _Value
		{
			get
			{
				if (this._value == null)
					this._value = this._valueInitializationFunction.Invoke();

				return this._value;
			}
		}

		public LazyField(Func<TField> initializeValueFunction)
		{
			this._valueInitializationFunction = initializeValueFunction;
		}
	}
}