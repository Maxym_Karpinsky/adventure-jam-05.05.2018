﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.UserInterface
{
	[AddComponentMenu("Layout/Radial Layout Group")]
	public class RadialLayoutGroup : LayoutGroup
	{
		[Range(0f, 360f)] public float ArcMin;
		[Range(0f, 360f)] public float ArcMax;
		[Range(0f, 360f)] public float ArcOffset;
		public float Radius = 5f;

		public override void CalculateLayoutInputVertical()
		{
			this.UpdateLayout();
		}

		public override void CalculateLayoutInputHorizontal()
		{
			this.UpdateLayout();
		}

		public override void SetLayoutHorizontal()
		{

		}

		public override void SetLayoutVertical()
		{

		}

		protected override void OnEnable()
		{
			base.OnEnable();

			this.UpdateLayout();
		}

		private void UpdateLayout()
		{
			m_Tracker.Clear();

			if (this.transform.childCount == 0) return;

			float fillAngle = (this.ArcMax - this.ArcMin) / this.transform.childCount;

			float progressAngle = this.ArcOffset;
			for (int i = 0; i < this.transform.childCount; i++)
			{
				RectTransform child = this.transform.GetChild(i) as RectTransform;
				if (child != null)
				{
					m_Tracker.Add(
						this,
						child,
						DrivenTransformProperties.Anchors |
						DrivenTransformProperties.AnchoredPosition |
						DrivenTransformProperties.Pivot
					);

					Vector3 targetPosition = new Vector3(Mathf.Cos(progressAngle * Mathf.Deg2Rad), Mathf.Sin(progressAngle * Mathf.Deg2Rad), 0);

					child.localPosition = targetPosition * this.Radius;

					child.anchorMin = child.anchorMax = child.pivot = new Vector2(0.5f, 0.5f);

					progressAngle += fillAngle;
				}
			}
		}

#if UNITY_EDITOR
		protected virtual void OnDrawGizmos()
		{
			
		}
#endif
	}
}

namespace Kimo.UserInterface.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(RadialLayoutGroup))]
    [CanEditMultipleObjects]
    public class RadialLayoutGroupEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            RadialLayoutGroup sRadialLayoutGroup = target as RadialLayoutGroup;
#pragma warning restore 0219
        }
    }
#endif
}