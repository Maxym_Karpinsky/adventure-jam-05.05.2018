﻿/* Created by Max.K.Kimo */

#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using TMPro;

using UnityEditor;
using UnityEditorInternal;

namespace Kimo.EditorExtension.Assistance
{
	[ExecuteInEditMode]
	public class UserInterfaceEditorController : MonoBehaviour
	{
		[Header("Font")]
		[SerializeField] private Font _globalFont;

		[Header("TMPro Font Asset")]
		[SerializeField] private TMP_FontAsset _TMP_FontAsset;

#if UNITY_EDITOR
		protected virtual void OnDrawGizmos()
		{

		}
#endif
	}
}

namespace Kimo.EditorExtension.Assistance.UTILITY
{
	[CustomEditor(typeof(UserInterfaceEditorController))]
	[CanEditMultipleObjects]
	public class UserInterfaceEditorControllerEditor : Editor
	{
		private SerializedProperty _globalFontProperty;
		private SerializedProperty _TMP_FontAssetProperty;

		private void OnEnable()
		{
			this._globalFontProperty = serializedObject.FindProperty("_globalFont");
			this._TMP_FontAssetProperty = serializedObject.FindProperty("_TMP_FontAsset");
		}

		private void ApplyFontGlobally(Font font)
		{
			Text[] textFields = FindObjectsOfType<Text>();

			for (int i = 0; i < textFields.Length; i++)
			{
				textFields[i].font = font;
			}
		}

		private void ApplyFontGlobally(TMP_FontAsset font)
		{
			TextMeshProUGUI[] textMeshProUGUIFields = FindObjectsOfType<TextMeshProUGUI>();

			for (int i = 0; i < textMeshProUGUIFields.Length; i++)
			{
				textMeshProUGUIFields[i].font = font;
			}
		}

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();

			if (GUILayout.Button("Apply Font Globally"))
			{
				this.ApplyFontGlobally(this._globalFontProperty.objectReferenceValue as Font);

				InternalEditorUtility.RepaintAllViews();
			}

			if (GUILayout.Button("Apply TMPro Font Asset Globally"))
			{
				this.ApplyFontGlobally(this._TMP_FontAssetProperty.objectReferenceValue as TMP_FontAsset);

				InternalEditorUtility.RepaintAllViews();
			}

#pragma warning disable 0219
			UserInterfaceEditorController sUserInterfaceEditorController = target as UserInterfaceEditorController;
#pragma warning restore 0219
		}
	}
}

#endif