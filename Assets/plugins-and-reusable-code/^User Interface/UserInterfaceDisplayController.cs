﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
	public abstract class UserInterfaceDisplayController<TDisplayController, TDisplay> : MonoBehaviourSingleton<TDisplayController>
		where TDisplayController : UserInterfaceDisplayController<TDisplayController, TDisplay>
		where TDisplay : UserInterfaceDisplay
	{
		protected TDisplay controlledUserInterfaceDisplay;

		private bool _isActive;
		public bool _IsActive { get { return this._isActive; } }

		[SerializeField] private UnityEvent _onInitializeStart;
		public UnityEvent _OnInitializeStart { get { return this._onInitializeStart; } }

		[SerializeField] private UnityEvent _onInitializeEnd;
		public UnityEvent _OnInitializeEnd { get { return this._onInitializeEnd; } }

		public void Initialize(TDisplay userInterfaceDisplay)
		{
			this._onInitializeStart.Invoke();
			userInterfaceDisplay._OnInitializeStart.Invoke();

			this.InitializeProcess(userInterfaceDisplay);

			userInterfaceDisplay._OnInitializeEnd.Invoke();
			this._onInitializeEnd.Invoke();
		}

		protected virtual void InitializeProcess(TDisplay userInterfaceDisplay)
		{
			this.controlledUserInterfaceDisplay = userInterfaceDisplay;
		}

		[SerializeField] private UnityEvent _onOpenStart;
		public UnityEvent _OnOpenStart { get { return this._onOpenStart; } }

		[SerializeField] private UnityEvent _onOpenEnd;
		public UnityEvent _OnOpenEnd { get { return this._onOpenEnd; } }

		public void Open()
		{
			this._isActive = true;

			this._onOpenStart.Invoke();
			this.controlledUserInterfaceDisplay._OnOpenStart.Invoke();

			this.OpenProcess();

			this.controlledUserInterfaceDisplay._OnOpenEnd.Invoke();
			this._onOpenEnd.Invoke();
		}

		protected virtual void OpenProcess()
		{
			this.UpdateLayout();
			this.controlledUserInterfaceDisplay.ActivateGameObject();
		}

		[SerializeField] private UnityEvent _onCloseStart;
		public UnityEvent _OnCloseStart { get { return this._onCloseStart; } }

		[SerializeField] private UnityEvent _onCloseEnd;
		public UnityEvent _OnCloseEnd { get { return this._onCloseEnd; } }

		public void Close()
		{
			this._isActive = false;

			this._onCloseStart.Invoke();
			this.controlledUserInterfaceDisplay._OnCloseStart.Invoke();

			this.CloseProcess();

			this.controlledUserInterfaceDisplay._OnCloseEnd.Invoke();
			this._onCloseEnd.Invoke();
		}

		protected virtual void CloseProcess()
		{
			this.controlledUserInterfaceDisplay.DeactivateGameObject();
		}

		public void Toggle()
		{
			if (this._isActive)
				this.Close();
			else
				this.Open();
		}

		public void ToggleGameObjectActivityStatus()
		{
			this.controlledUserInterfaceDisplay.gameObject.SetActive(!this.controlledUserInterfaceDisplay.gameObject.activeSelf);
		}

		public abstract void UpdateLayout();

#if UNITY_EDITOR
	protected virtual void OnDrawGizmos()
        {
            
        }
#endif
    }
}