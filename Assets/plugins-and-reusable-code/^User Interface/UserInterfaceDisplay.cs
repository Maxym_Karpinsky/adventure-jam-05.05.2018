﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
	public abstract class UserInterfaceDisplay : MonoBehaviour
	{
		[SerializeField] private UnityEvent _onInitializeStart;
		public UnityEvent _OnInitializeStart { get { return this._onInitializeStart; } }

		[SerializeField] private UnityEvent _onInitializeEnd;
		public UnityEvent _OnInitializeEnd { get { return this._onInitializeEnd; } }

		[SerializeField] private UnityEvent _onOpenStart;
		public UnityEvent _OnOpenStart { get { return this._onOpenStart; } }

		[SerializeField] private UnityEvent _onOpenEnd;
		public UnityEvent _OnOpenEnd { get { return this._onOpenEnd; } }

		[SerializeField] private UnityEvent _onCloseStart;
		public UnityEvent _OnCloseStart { get { return this._onCloseStart; } }

		[SerializeField] private UnityEvent _onCloseEnd;
		public UnityEvent _OnCloseEnd { get { return this._onCloseEnd; } }

#if UNITY_EDITOR
		protected virtual void OnDrawGizmos()
        {
            
        }
#endif
    }
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(UserInterfaceDisplay))]
    [CanEditMultipleObjects]
    public class UserInterfaceDisplayEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            UserInterfaceDisplay sUserInterfaceDisplay = target as UserInterfaceDisplay;
#pragma warning restore 0219
        }
    }
#endif
}