﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Events;
#endif

namespace Kimo.Core.UserInterface
{
	[AddComponentMenu("UI/Button Toggle", 30)]
	[RequireComponent(typeof(Button))]
	public class ButtonToggle : MonoBehaviour
	{
		private bool _isActive;
		public bool _IsActive { get { return this._isActive; } }

		private ButtonToggleGroup _buttonToggleGroup;
		public ButtonToggleGroup ButtonToggleGroup
		{
			get { return this._buttonToggleGroup; }
			set
			{
				if (this._buttonToggleGroup == null)
					this._buttonToggleGroup = value;
#if UNITY_EDITOR
				else
					Debug.LogWarning("You are trying to assign another ButtonToggleGroup to ButtonToggle");
#endif
			}
		}

		[SerializeField] private Image _targetGraphicsImageField;
		public Image _TargetGraphicsImageField { get { return this._targetGraphicsImageField; } }


		public Sprite ActivatedStateGraphics;

		[SerializeField] private UnityEvent _onActivate = new UnityEvent();
		public UnityEvent _OnActivate { get { return this._onActivate; } }

		public void Activate()
		{
			this._isActive = true;
			this._targetGraphicsImageField.sprite = this.ActivatedStateGraphics;

			this._OnActivate.Invoke();
		}

		internal void ForceActivate()
		{
			this._isActive = true;
			this._targetGraphicsImageField.sprite = this.ActivatedStateGraphics;

			this._OnActivate.Invoke();
		}


		public Sprite DeactivatedStateGraphics;

		[SerializeField] private UnityEvent _onDeactivate = new UnityEvent();
		public UnityEvent _OnDeactivate { get { return this._onDeactivate; } }

		public void Deactivate()
		{
			if (this.ButtonToggleGroup != null && !this.ButtonToggleGroup.ValidateDeativation(this))
				return;

			this._isActive = false;
			this._targetGraphicsImageField.sprite = this.DeactivatedStateGraphics;

			this._OnDeactivate.Invoke();
		}

		internal void ForceDeactivate()
		{
			this._isActive = false;
			this._targetGraphicsImageField.sprite = this.DeactivatedStateGraphics;

			this._OnDeactivate.Invoke();
		}

		public void Toggle()
		{
			if (this._isActive)
				this.Deactivate();
			else
				this.Activate();
		}

		private Button _targetButton;

		[SerializeField] private bool _isInitializedOnAwake;
		public bool _IsInitializedOnAwake { get { return this._isInitializedOnAwake; } }

		[SerializeField] private bool _isInitiallyActive;
		public bool _IsInitiallyActive { get { return this._isInitiallyActive; } }

		private void Awake()
		{
			this._targetButton = this.GetComponent<Button>();

			if (this._isInitializedOnAwake)
			{
				if (this._isInitiallyActive)
					this.Activate();
				else
					this.Deactivate();
			}
		}

#if UNITY_EDITOR
		protected virtual void Reset()
		{
			this._targetButton = this.GetComponent<Button>();

			UnityEventTools.RemovePersistentListener(this._targetButton.onClick, this.Toggle);
			UnityEventTools.AddPersistentListener(this._targetButton.onClick, this.Toggle);

			if (this._targetButton.image != null)
				this._targetGraphicsImageField = this._targetButton.image;
			else
				this._targetGraphicsImageField = this.GetComponent<Image>();
		}
#endif
	}
}

namespace Kimo.Core.UserInterface.UTILITY
{
#if UNITY_EDITOR
	[CustomEditor(typeof(ButtonToggle))]
	[CanEditMultipleObjects]
	public class ButtonProEditor : Editor
	{
		private SerializedProperty _targetGraphicsImageFieldProp;

		private SerializedProperty _onActivateProp;
		private SerializedProperty _onDeactivateProp;

		private SerializedProperty _isInitializedOnAwakeProp;
		private SerializedProperty _isInitiallyActiveProp;

		private void OnEnable()
		{
			this._targetGraphicsImageFieldProp = serializedObject.FindProperty("_targetGraphicsImageField");

			this._onActivateProp = serializedObject.FindProperty("_onActivate");
			this._onDeactivateProp = serializedObject.FindProperty("_onDeactivate");

			this._isInitializedOnAwakeProp = serializedObject.FindProperty("_isInitializedOnAwake");
			this._isInitiallyActiveProp = serializedObject.FindProperty("_isInitiallyActive");
		}

		public override void OnInspectorGUI()
		{
#pragma warning disable 0219
			ButtonToggle sButtonPro = target as ButtonToggle;
#pragma warning restore 0219

			EditorGUILayout.PropertyField(this._targetGraphicsImageFieldProp);

			sButtonPro.ActivatedStateGraphics =
				EditorGUILayout.ObjectField(
					new GUIContent("Activated State Source Image", "Graphics to change to on Activate"),
					sButtonPro.ActivatedStateGraphics,
					typeof(Sprite),
					true
				) as Sprite;

			EditorGUILayout.PropertyField(this._onActivateProp);

			EditorGUILayout.Space();

			sButtonPro.DeactivatedStateGraphics =
				EditorGUILayout.ObjectField(
					new GUIContent("Deactivated State Source Image", "Graphics to change to on Deactivate"),
					sButtonPro.DeactivatedStateGraphics,
					typeof(Sprite),
					true
				) as Sprite;

			EditorGUILayout.PropertyField(this._onDeactivateProp);

			this._isInitializedOnAwakeProp.boolValue =
				EditorGUILayout.Toggle(
					"Initialize On Awake",
					this._isInitializedOnAwakeProp.boolValue
					);

			if (this._isInitializedOnAwakeProp.boolValue)
			{
				EditorGUI.indentLevel++;

				this._isInitiallyActiveProp.boolValue =
					EditorGUILayout.Toggle(
						"Initially Active",
						this._isInitiallyActiveProp.boolValue
						);

				EditorGUI.indentLevel--;
			}
			else
				this._isInitiallyActiveProp.boolValue = false;

			serializedObject.ApplyModifiedProperties();
		}

		[MenuItem("GameObject/UI/Button Toggle &F1", false)]
		public static void CreateButtonProGameObject()
		{
			GameObject newGameObject = new GameObject("Button Toggle", typeof(Image), typeof(Button), typeof(ButtonToggle));

			newGameObject.transform.SetParent(Selection.activeTransform, false);
			newGameObject.transform.localPosition = Vector3.zero;

			Selection.activeTransform = newGameObject.transform;

			Undo.RegisterCreatedObjectUndo(newGameObject, "Undo Button Toggle");
		}
	}
#endif
}