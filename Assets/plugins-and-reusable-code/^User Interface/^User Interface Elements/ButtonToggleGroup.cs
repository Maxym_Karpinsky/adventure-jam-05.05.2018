﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core.UserInterface
{
	public class ButtonToggleGroup : MonoBehaviour
	{
		[SerializeField] private ButtonToggle _activeButtonToggle;
		public ButtonToggle _ActiveButtonToggle { get { return this._activeButtonToggle; } }

		[SerializeField] private bool _requireOneActiveToggle;
		public bool _RequireOneActiveToggle { get { return this._requireOneActiveToggle; } }

		private void OnActivationDetected(ButtonToggle sourceButtonToggle)
		{
			if (this._activeButtonToggle != sourceButtonToggle)
			{
				this._activeButtonToggle.ForceDeactivate();
				this._activeButtonToggle = sourceButtonToggle;
			}
		}

		private void OnDeactivationDetected(ButtonToggle sourceButtonToggle)
		{

		}

		public bool ValidateDeativation(ButtonToggle buttonToggle)
		{
			return !this._requireOneActiveToggle || this._activeButtonToggle != buttonToggle;
		}

		private List<ButtonToggle> _buttonToggles;
		public List<ButtonToggle> _ButtonToggles { get { return this._buttonToggles; } }

		public void Initialize()
		{
			this._buttonToggles = new List<ButtonToggle>(this.GetComponentsInChildren<ButtonToggle>());

			if (this._activeButtonToggle != null)
				this._activeButtonToggle.Activate();

			for (int i = 0; i < this._buttonToggles.Count; i++)
			{
				ButtonToggle buttonToggle = this._buttonToggles[i];
				buttonToggle.ButtonToggleGroup = this;

				if (buttonToggle != this._activeButtonToggle && !buttonToggle._IsInitiallyActive)
					buttonToggle.Deactivate();

				buttonToggle._OnActivate.AddListener(delegate { this.OnActivationDetected(buttonToggle); });
				buttonToggle._OnDeactivate.AddListener(delegate { this.OnDeactivationDetected(buttonToggle); });
			}
		}

		private void Awake()
		{
			this.Initialize();
		}

#if UNITY_EDITOR
		protected virtual void OnDrawGizmos()
		{

		}
#endif
	}
}

namespace Kimo.Core.UserInterface.UTILITY
{
#if UNITY_EDITOR
	[CustomEditor(typeof(ButtonToggleGroup))]
	[CanEditMultipleObjects]
	public class ButtonToggleGroupEditor : Editor
	{
		private void OnEnable()
		{

		}

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();

#pragma warning disable 0219
			ButtonToggleGroup sButtonToggleGroup = target as ButtonToggleGroup;
#pragma warning restore 0219
		}
	}
#endif
}