﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;
using TMPro;

namespace Kimo.Core
{
    public sealed class UserInterfaceSreenSpaceTextPopUpSpawner : UserInterfaceTextPopUpSpawner<UserInterfaceSreenSpaceTextPopUpSpawner>
    {
		protected override Vector3 GetSpawnPosition(Vector3 worldPosition)
		{
			return CameraController.Instance_._ControlledCamera.WorldToScreenPoint(worldPosition);
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{

		//}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(UserInterfaceSreenSpaceTextPopUpSpawner))]
    [CanEditMultipleObjects]
    public class UserInterfaceSreenSpaceTextPopUpSpawnerEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            UserInterfaceSreenSpaceTextPopUpSpawner sUserInterfaceSreenSpaceTextPopUpSpawner = target as UserInterfaceSreenSpaceTextPopUpSpawner;
#pragma warning restore 0219
        }
    }
#endif
}