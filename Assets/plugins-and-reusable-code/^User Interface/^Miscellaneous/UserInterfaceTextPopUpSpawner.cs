﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    public abstract class UserInterfaceTextPopUpSpawner<T> : MonoBehaviourSingleton<T>
		where T : UserInterfaceTextPopUpSpawner<T>
    {
		[SerializeField] private Transform _textPopUpContainer;
		[SerializeField] private TextPopUp _textPopUpPrefab;

		[Range(0.1f, 10f)]
		[SerializeField]
		private float _borrowTime = 0.5f;

		protected abstract Vector3 GetSpawnPosition(Vector3 worldPosition);

		public virtual void Spawn(Vector3 worldPosition, string message)
		{
			TextPopUp textPopUp = ObjectPool.Instance_.DepoolObject<TextPopUp>(
				this._textPopUpPrefab,
				this._textPopUpContainer,
				this.GetSpawnPosition(worldPosition),
				false,
				true,
				this._borrowTime
			);

			textPopUp.TextField_.text = message;
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{

		//}
#endif
	}
}