﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    public sealed class UserInterfaceWorldSpaceTextPopUpSpawner : UserInterfaceTextPopUpSpawner<UserInterfaceWorldSpaceTextPopUpSpawner>
    {
		[SerializeField] private Vector3 _offset = new Vector3(0f, 1.5f, 0f);

		protected override Vector3 GetSpawnPosition(Vector3 worldPosition)
		{
			return worldPosition + this.transform.rotation * this._offset;
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{

		//}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(UserInterfaceWorldSpaceTextPopUpSpawner))]
    [CanEditMultipleObjects]
    public class UserInterfaceWorldSpaceTextPopUpSpawnerEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            UserInterfaceWorldSpaceTextPopUpSpawner sUserInterfaceWorldSpaceTextPopUpSpawner = target as UserInterfaceWorldSpaceTextPopUpSpawner;
#pragma warning restore 0219
        }
    }
#endif
}