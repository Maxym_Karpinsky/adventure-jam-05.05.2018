﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;
using TMPro;

namespace Kimo.Core
{
	[RequireComponent(typeof(TextMeshProUGUI))]
    public class TextPopUp : PoolableObject
    {
		public TextMeshProUGUI TextField_ { get; private set; }

		[SerializeField] private float _upMovementSpeed = 100f;

		private void Update()
		{
			this.transform.localPosition += Vector3.up * Time.deltaTime * this._upMovementSpeed;
		}

		private void Awake()
		{
			this.TextField_ = this.GetComponent<TextMeshProUGUI>();
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{

		//}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(TextPopUp))]
    [CanEditMultipleObjects]
    public class TextPopUpEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            TextPopUp sTextPopUp = target as TextPopUp;
#pragma warning restore 0219
        }
    }
#endif
}