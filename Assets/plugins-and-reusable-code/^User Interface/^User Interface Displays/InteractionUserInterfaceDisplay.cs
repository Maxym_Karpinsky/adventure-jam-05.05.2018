﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using TMPro;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
	public class InteractionUserInterfaceDisplay : UserInterfaceDisplay
	{
		[SerializeField] private TextMeshProUGUI _dialogueTextField;
		public TextMeshProUGUI _DialogueTextField { get { return this._dialogueTextField; } }

		[SerializeField] private Image _collocutorIconImageField;
		public Image _CollocutorIconImageField { get { return this._collocutorIconImageField; } }

		[SerializeField] private TextMeshProUGUI _collocutorNameTextField;
		public TextMeshProUGUI _CollocutorNameTextField { get { return this._collocutorNameTextField; } }

		[SerializeField] private Button _backButtonField;
		public Button _BackButtonField { get { return this._backButtonField; } }

		[SerializeField] private Button _forwardButtonField;
		public Button _ForwardButtonField { get { return this._forwardButtonField; } }

		private void Start()
		{
			InteractionUserInterfaceDisplayController.Instance_.Initialize(this);
		}

#if UNITY_EDITOR
		protected override void OnDrawGizmos()
        {
			base.OnDrawGizmos();
        }
#endif
    }
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(InteractionUserInterfaceDisplay))]
    [CanEditMultipleObjects]
    public class InteractionUserInterfaceDisplayEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            InteractionUserInterfaceDisplay sInteractionUserInterfaceDisplay = target as InteractionUserInterfaceDisplay;
#pragma warning restore 0219
        }
    }
#endif
}