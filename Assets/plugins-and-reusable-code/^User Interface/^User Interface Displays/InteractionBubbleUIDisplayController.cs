﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;
using TMPro;
using System;

namespace Kimo.Core
{
    public class InteractionBubbleUIDisplayController : UserInterfaceDisplayController<InteractionBubbleUIDisplayController, InteractionBubbleUIDisplay>
    {
		[SerializeField] private KeyCode _reactionKeyCode = KeyCode.E;
		public KeyCode _ReactionKeyCode { get { return this._reactionKeyCode; } }

		[SerializeField] private Vector2 _bubbleDisplayOffset;
		public Vector2 _BubbleDisplayOffset { get { return this._bubbleDisplayOffset; } }

		[SerializeField] private Canvas _worldSpaceCanvas;

		private UnityEvent _reactionEvent;
		// public UnityEvent _ReactionEvent { get { return this._reactionEvent; } }

		public void DisplayBubble(BubbleInteractable bubbleInteractable)
		{
			this.controlledUserInterfaceDisplay.transform.position = bubbleInteractable.transform.position + this.controlledUserInterfaceDisplay.transform.up * this._bubbleDisplayOffset.y + this.controlledUserInterfaceDisplay.transform.right * this._bubbleDisplayOffset.x;

			this.controlledUserInterfaceDisplay._MessageTextField.text = bubbleInteractable._BubbleDisplayData._Message;

			this._reactionEvent = bubbleInteractable._BubbleDisplayData._ReactionEvent;

			this.Open();
		}

		public void OnStay()
		{
			if (Input.GetKeyDown(this._reactionKeyCode))
			{
				if (this._reactionEvent != null)
					this._reactionEvent.Invoke();
			}
		}

		public override void UpdateLayout()
		{
			
		}

#if UNITY_EDITOR
		protected override void OnDrawGizmos()
		{
			base.OnDrawGizmos();
		}
#endif

		[System.Serializable]
		public struct BubbleDisplayData
		{
			[SerializeField] private string _message;
			public string _Message { get { return this._message; } }

			[SerializeField] private UnityEvent _reactionEvent;
			public UnityEvent _ReactionEvent { get { return this._reactionEvent; } }
		}
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(InteractionBubbleUIDisplayController))]
    [CanEditMultipleObjects]
    public class InteractionBubbleUIDisplayControllerEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            InteractionBubbleUIDisplayController sInteractionBubbleUIDisplayController = target as InteractionBubbleUIDisplayController;
#pragma warning restore 0219
        }
    }
#endif
}