﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

using TMPro;

namespace Kimo.Core
{
    public class ProgressBarDisplay : UserInterfaceDisplay 
    {
		[SerializeField] private Image _progressFillImageField;
		public Image _ProgressFillImageField { get { return this._progressFillImageField; } }

		[SerializeField] private TextMeshProUGUI _progressTextField;
		public TextMeshProUGUI _ProgressTextField { get { return this._progressTextField; } }

		private void Start()
		{
			ProgressBarDisplayController.Instance_.Initialize(this);
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{

		//}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(ProgressBarDisplay))]
    [CanEditMultipleObjects]
    public class ProgressBarDisplayEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            ProgressBarDisplay sProgressBarDisplay = target as ProgressBarDisplay;
#pragma warning restore 0219
        }
    }
#endif
}