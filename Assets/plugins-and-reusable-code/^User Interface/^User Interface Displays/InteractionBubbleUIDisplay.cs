﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using TMPro;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    public class InteractionBubbleUIDisplay : UserInterfaceDisplay 
    {
		[SerializeField] private TextMeshProUGUI _messageTextField;
		public TextMeshProUGUI _MessageTextField { get { return this._messageTextField; } }

		private void Start()
		{
			InteractionBubbleUIDisplayController.Instance_.Initialize(this);
		}

#if UNITY_EDITOR
		protected override void OnDrawGizmos()
        {
			base.OnDrawGizmos();
        }
#endif
    }
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(InteractionBubbleUIDisplay))]
    [CanEditMultipleObjects]
    public class InteractionBubbleUIDisplayEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            InteractionBubbleUIDisplay sInteractionBubbleUIDisplay = target as InteractionBubbleUIDisplay;
#pragma warning restore 0219
        }
    }
#endif
}