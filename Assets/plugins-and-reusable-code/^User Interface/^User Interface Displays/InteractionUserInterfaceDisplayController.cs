﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using Kimo.Audio;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
	public class InteractionUserInterfaceDisplayController : UserInterfaceDisplayController<InteractionUserInterfaceDisplayController, InteractionUserInterfaceDisplay>
	{
		private Dialogue _displayedDialogue;
		public Dialogue _DisplayedDialogue { get { return this._displayedDialogue; } }

		private int _dialogueDataIndex;

		public void DisplayDialogue(Dialogue dialogue)
		{
			/*
			if (this._IsActive && this._displayedDialogue == dialogue) //! I think it's unnecessary to make this because this should just display things and if this conditions weren't met by game design then it should behave this way and show the dialogue again
				return;
			*/

			this._displayedDialogue = dialogue;
			this._dialogueDataIndex = -1;

			this.DisplayNextDialogueData();

			this.Open();
		}

		public void DisplayNextDialogueData()
		{
			this._dialogueDataIndex = (this._dialogueDataIndex + 1) % this._displayedDialogue._ConversationData.Count;

			this.UpdateLayout();
		}

		public void DisplayPreviousDialogueData()
		{
			this._dialogueDataIndex = (this._dialogueDataIndex + this._displayedDialogue._ConversationData.Count - 1) % this._displayedDialogue._ConversationData.Count;

			this.UpdateLayout();
		}

		public override void UpdateLayout()
		{
			DialogueData dialogueData = this._displayedDialogue._ConversationData[this._dialogueDataIndex];

			this.controlledUserInterfaceDisplay._DialogueTextField.text = dialogueData._SentenceText;
			//AudioPlayer.PlayClip(AudioPlayer.Instance_.DefaultVoiceOverSource, dialogueData._AudioCover); TODO: uncomment

			this.controlledUserInterfaceDisplay._CollocutorIconImageField.sprite = dialogueData._Collocutor._ProfileIcon;
			this.controlledUserInterfaceDisplay._CollocutorNameTextField.text = dialogueData._Collocutor._ProfileName;

			this.controlledUserInterfaceDisplay._BackButtonField.interactable = true;
			this.controlledUserInterfaceDisplay._ForwardButtonField.interactable = true;
			if (this._dialogueDataIndex == 0)
			{
				this.controlledUserInterfaceDisplay._BackButtonField.interactable = false;
			}
			else if (this._dialogueDataIndex == this._displayedDialogue._ConversationData.Count - 1)
			{
				this.controlledUserInterfaceDisplay._ForwardButtonField.interactable = false;
			}
		}

#if UNITY_EDITOR
		protected override void OnDrawGizmos()
        {
			base.OnDrawGizmos();
        }
#endif
    }
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(InteractionUserInterfaceDisplayController))]
    [CanEditMultipleObjects]
    public class InteractionUserInterfaceDisplayControllerEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            InteractionUserInterfaceDisplayController sInteractionUserInterfaceDisplayController = target as InteractionUserInterfaceDisplayController;
#pragma warning restore 0219
        }
    }
#endif
}