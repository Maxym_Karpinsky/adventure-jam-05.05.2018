﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    public class ProgressBarDisplayController : UserInterfaceDisplayController<ProgressBarDisplayController, ProgressBarDisplay> 
    {
		[SerializeField] private Vector3 _offset = new Vector3(0f, 2f, 0f);

		public void SetPosition(Transform transform)
		{
			this.transform.position = transform.position + Vector3.Scale(this.transform.up, this._offset);
		}

		public void Display(float value, float maxValue)
		{
			this.controlledUserInterfaceDisplay._ProgressFillImageField.fillAmount = value / maxValue;
			this.controlledUserInterfaceDisplay._ProgressTextField.text = string.Format("{0} / {1}", value, maxValue);
		}

		public override void UpdateLayout()
		{
			
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{

		//}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(ProgressBarDisplayController))]
    [CanEditMultipleObjects]
    public class ProgressBarDisplayControllerEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            ProgressBarDisplayController sProgressBarDisplayController = target as ProgressBarDisplayController;
#pragma warning restore 0219
        }
    }
#endif
}