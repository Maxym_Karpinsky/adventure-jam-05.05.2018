﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core.Pathfinding
{
    public class Pathfinder : MonoBehaviourSingleton<Pathfinder>
    {
        [SerializeField] private LayerMask _freeLayer, _neutralLayer, _obstacleLayer;
        public LayerMask FreeLayer { get { return this._freeLayer; } }
        public LayerMask NeutralLayer { get { return this._neutralLayer; } }
        public LayerMask ObstacleLayer { get { return this._obstacleLayer; } }


#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            
        }
#endif
    }
}

namespace Kimo.Core.Pathfinding.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(Pathfinder))]
    [CanEditMultipleObjects]
    public class PathfinderEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            Pathfinder sPathfinder = target as Pathfinder;
#pragma warning restore 0219
        }
    }
#endif
}