﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core.Pathfinding
{
    public class Node
    {
        /// <summary>
        /// If state is equal to GameAsset.AvailabilityState.Free |
        /// Used in pathfinding to reduce the cost of checks
        /// </summary>
        public bool Free_ { get; private set; }

        /// <summary>
        /// AvailabilityState of the node
        /// </summary>
        public GameAsset.AvailabilityState State_ { get; private set; }

        /// <summary>
        /// Changes state of the node |
        /// Determines if the node is Free_
        /// </summary>
        /// <param name="state"></param>
        public void ChangeState(GameAsset.AvailabilityState state)
        {
            this.State_ = state;
            this.Free_ = this.State_ == GameAsset.AvailabilityState.Free;
        }

        /// <summary>
        /// Game assets that fall within range of the node |
        /// This field isn't initialized by default to save memory |
        /// Be sure it's not null before using it
        /// </summary>
        public List<GameAsset> AttachedGameAssets_ { get; private set; }

        /// <summary>
        /// Determines AvailabilityState of the node
        /// </summary>
        public void DetermineState(bool initializeAttachedGameAssets = true)
        {
            if (initializeAttachedGameAssets)
            {
                
            }
        }

        /// <summary>
        /// Position of the node in the world space
        /// </summary>
        public Vector3 WorldPosition { get; set; }

#region Constructors

        /// <summary>
        /// Creates crystal clean instance
        /// </summary>
        public Node() { }

        /// <summary>
        /// Creates instance with world position
        /// </summary>
        /// <param name="worldPosition">Position of the node in the world space</param>
        public Node(Vector3 worldPosition)
        {
            this.WorldPosition = worldPosition;

			this.DetermineState(false);
		}

        /// <summary>
        /// Creates instance with world position | Determies AvailabilityState of the node
        /// </summary>
        /// <param name="worldPosition">Position of the node in the world space</param>
        /// <param name="initializeAttachedGameAssets">Request to initialize game assets attached to the node</param>
        public Node(Vector3 worldPosition, bool initializeAttachedGameAssets)
        {
            this.WorldPosition = worldPosition;

            this.DetermineState(initializeAttachedGameAssets);
        }

        /// <summary>
        /// Creates instance with world position and attached game assets |
        /// Determines state on request (state determination won't override game assets you are trying to set)
        /// </summary>
        /// <param name="worldPosition">Position of the node in the world space</param>
        /// <param name="attachedGameAssets">Game assets attached to the node</param>
        public Node(Vector3 worldPosition, List<GameAsset> attachedGameAssets, bool determineState = true)
        {
            this.WorldPosition = worldPosition;
            this.AttachedGameAssets_ = attachedGameAssets;

            this.DetermineState(false);
        }
#endregion
    }
}