﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance.UTILITY
{
    public static class MathUtility
    {
		public const float HALF_VALUE_PERCENT = 0.5f;
		public const float PI_HALF = 1.57079632f;

		public static float GetProjectionValue(Vector2 lhs, Vector2 rhs)
        {
            return Vector2.Dot(lhs.normalized, rhs.normalized);
        }

        public static float GetProjectionValue(Vector3 lhs, Vector3 rhs)
        {
            return Vector3.Dot(lhs.normalized, rhs.normalized);
        }

        public static Vector3 GetDirectionFromAngle(float angleInDegrees, float localRotationAngle = 0)
        {
            angleInDegrees += localRotationAngle;
            return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		/// <param name="power"></param>
		/// <returns>­(Σ value^n) where `n`is limited by `power`</returns>
		public static float SigmaOfValueToPow(float value, int power)
		{
			float result = 1f;
			float poweredValue = value;

			for (int i = 0; i < power; i++)
			{
				result += poweredValue;

				poweredValue *= value;
			}

			return result;
		}
    }
}