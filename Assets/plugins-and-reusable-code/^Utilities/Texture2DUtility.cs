﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance.UTILITY
{
    public static class Texture2DUtility
    {
		public static Texture2D CreateTexture(int width, int height, Color[] pixels)
		{
			Texture2D texture = new Texture2D(width, height);
			texture.SetPixels(pixels);
			texture.Apply();

			return texture;
		}

		public static Texture2D CreateTexture(int width, int height, Color[] pixels, FilterMode filterMode, TextureWrapMode textureWrapMode)
		{
			Texture2D texture = CreateTexture(width, height, pixels);
			texture.filterMode = filterMode;
			texture.wrapMode = textureWrapMode;

			return texture;
		}

		public static Texture2D CreateTexture(int width, int height, Color color)
		{
			Color[] pixels = new Color[width * height];

			for (int i = 0; i < pixels.Length; ++i)
				pixels[i] = color;

			return CreateTexture(width, height, pixels);
		}

		public static Texture2D CreateTexture(int width, int height, Color color, Color borderColor, Texture2DBorder texture2DBorder, float stylizationFactor = 1f)
		{
			stylizationFactor = stylizationFactor.Clamp01();

			Color[] pixels = new Color[width * height];

			// Full
			for (int i = 0; i < pixels.Length; ++i)
				pixels[i] = color;

			// Left
			for (int y = 0; y < (int)(width * height * (stylizationFactor + 0.2f).Clamp01()); y += width)
				for (int x = 0; x < texture2DBorder.LeftWidth; x++)
					pixels[y + x] = borderColor;

			// Right
			for (int y = width - 1; y < width * height; y += width)
				for (int x = 0; x < texture2DBorder.RightWidth; x++)
					pixels[y - x] = borderColor;

			// Top
			for (int x = width * height - 1; x > width * height - 1 - (int)(width * stylizationFactor); x--)
				for (int y = 0; y < texture2DBorder.TopWidth; y++)
					pixels[x - y * width] = borderColor;

			// Bottom
			for (int x = 0; x < width; x++)
				for (int y = 0; y < texture2DBorder.BottomWidth; y++)
					pixels[x + y * width] = borderColor;

			return CreateTexture(width, height, pixels);
		}

		public struct Texture2DBorder
		{
			public int LeftWidth;
			public int RightWidth;
			public int TopWidth;
			public int BottomWidth;

			public Texture2DBorder(int leftWidth = 1, int rightWidth = 1, int topWidth = 1, int bottomWidth = 1)
			{
				this.LeftWidth = leftWidth;
				this.RightWidth = rightWidth;
				this.TopWidth = topWidth;
				this.BottomWidth = bottomWidth;
			}
		}
	}
}