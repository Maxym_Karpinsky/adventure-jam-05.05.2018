﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Assistance.UTILITY
{
    public static class PathUtility 
    {
		public const string ASSETS = "Assets";
		public const string PROJECT_SETTINGS = "ProjectSettings";

#if UNITY_EDITOR
		public static string GetScriptFilePath(MonoBehaviour monoBehaviour)
		{
			MonoScript ms = MonoScript.FromMonoBehaviour(monoBehaviour);

			string scriptFilePath = AssetDatabase.GetAssetPath(ms);
			return scriptFilePath.Replace('\\', '/');
		}

		public static string GetScriptFilePath(ScriptableObject scriptableObject)
		{
			MonoScript ms = MonoScript.FromScriptableObject(scriptableObject);

			string scriptFilePath = AssetDatabase.GetAssetPath(ms);
			return scriptFilePath.Replace('\\', '/');
		}

		public static string GetScriptFileDirectoryPath(MonoBehaviour monoBehaviour)
		{
			string scriptFilePath = GetScriptFilePath(monoBehaviour);

			return scriptFilePath.Substring(0, scriptFilePath.LastIndexOf("/"));
		}

		public static string GetScriptFileDirectoryPath(ScriptableObject scriptableObject)
		{
			string scriptFilePath = GetScriptFilePath(scriptableObject);

			return scriptFilePath.Substring(0, scriptFilePath.LastIndexOf("/"));
		}
#endif
	}
}