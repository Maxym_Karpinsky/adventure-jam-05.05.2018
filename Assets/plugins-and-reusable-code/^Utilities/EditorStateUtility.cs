﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Core;
using Kimo.Assistance;

namespace Kimo.Assistance.UTILITY
{
#if UNITY_EDITOR
	[InitializeOnLoad]
	public static class EditorStateUtility
	{
		public static event Action OnEnteredEditMode;
		public static event Action OnExitingEditMode;
		public static event Action OnEnteredPlayMode;
		public static event Action OnExitingPlayMode;

		private static void OnPlaymodeStateChanged(PlayModeStateChange playModeStateChange)
		{
			switch (playModeStateChange)
			{
				case PlayModeStateChange.EnteredEditMode:

					if (EditorStateUtility.OnEnteredEditMode != null)
						EditorStateUtility.OnEnteredEditMode.Invoke();

					//foreach (InteractionTrigger interactionTrigger in GameObject.FindObjectsOfType<InteractionTrigger>())
					//{
					//	interactionTrigger.CheckForTriggerColliders();
					//}

					break;
				case PlayModeStateChange.ExitingEditMode:

					if (EditorStateUtility.OnExitingEditMode != null)
						EditorStateUtility.OnExitingEditMode.Invoke();

					break;
				case PlayModeStateChange.EnteredPlayMode:

					if (EditorStateUtility.OnEnteredPlayMode != null)
						EditorStateUtility.OnEnteredPlayMode.Invoke();

					//TODO: This is total shit, but I have no choise. The script reinitializes. If not using [InitializeOnLoad] it actually takes the references set and tries to call them. But by that time they are all null, thus it gives an error. Only solution would be to do the required things in Awake or Start and subscribe to Exiting to do what I was hoping to do. But it still doesn't work because I lose the reference to call on exiting play mode. So I will just do that in OnDrawGizmos.
					//foreach (InteractionTrigger interactionTrigger in GameObject.FindObjectsOfType<InteractionTrigger>())
					//{
					//	interactionTrigger.CheckForTriggerColliders();
					//}

					break;
				case PlayModeStateChange.ExitingPlayMode:

					if (EditorStateUtility.OnExitingPlayMode != null)
						EditorStateUtility.OnExitingPlayMode.Invoke();

					break;
			}
		}

		static EditorStateUtility()
		{
			EditorApplication.playModeStateChanged -= EditorStateUtility.OnPlaymodeStateChanged;
			EditorApplication.playModeStateChanged += EditorStateUtility.OnPlaymodeStateChanged;
		}
	}
#endif
}