﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance.UTILITY
{
#if UNITY_EDITOR
	public static class TagUtility
	{
		public static void AddTag(string tag)
		{
			Object[] asset = AssetDatabase.LoadAllAssetsAtPath(PathUtility.PROJECT_SETTINGS + "/TagManager.asset");

			if ((asset != null) && (asset.Length > 0))
			{
				SerializedObject serializedObject = new SerializedObject(asset[0]);
				SerializedProperty tags = serializedObject.FindProperty("tags");

				for (int i = 0; i < tags.arraySize; ++i)
					if (tags.GetArrayElementAtIndex(i).stringValue == tag) // Is tag already in there?
						return;

				tags.InsertArrayElementAtIndex(tags.arraySize);
				tags.GetArrayElementAtIndex(tags.arraySize - 1).stringValue = tag;
				serializedObject.ApplyModifiedProperties();
				serializedObject.Update();
			}
		}
	}
#endif
}