﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance.UTILITY
{
    public static class CoroutineUtility
    {
		public static IEnumerator DelayedActionProcess(YieldInstruction yieldInstruction, Action action)
		{
			yield return yieldInstruction;
			
			action.Invoke();
		}

		public static IEnumerator DelayedActionProcess(CustomYieldInstruction customYieldInstruction, Action action)
		{
			yield return customYieldInstruction;

			action.Invoke();
		}

		public static IEnumerator DelayedActionProcess(Cooldown cooldown, Action action, bool resetCooldown = false)
		{
			while (!cooldown.IsFinished())
				yield return null;

			if (resetCooldown)
				cooldown.Reset();

			action.Invoke();
		}

		// Just use action += .
		//public static IEnumerator DelayedActionProcess(YieldInstruction yieldInstruction, params Action[] actions)
		//{
		//	yield return yieldInstruction;

		//	for (int i = 0; i < actions.Length; i++)
		//	{
		//		actions[i].Invoke();
		//	}
		//}
	}
}