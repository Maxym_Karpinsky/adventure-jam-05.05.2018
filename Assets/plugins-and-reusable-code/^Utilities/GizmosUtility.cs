﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance.UTILITY
{
    public static class GizmosUtility
    {
		private const float DEFAULT_TRANSPARENCY_ALPHA = 0.5f;

		public static void DrawLine(Vector3 from, Vector3 to, Color color)
		{
			Gizmos.color = color;
			Gizmos.DrawLine(from, to);
		}

		public static void DrawLine(Vector3 from, Vector3 to, Color color, float alpha)
		{
			color.a = alpha;

			Gizmos.color = color;
			Gizmos.DrawLine(from, to);
		}
		
		public static void DrawCombinedCube(Vector3 center, Vector3 size, Color color, float alpha)
		{
			Gizmos.color = color;
			Gizmos.DrawWireCube(center, size);

			color.a = alpha;

			Gizmos.color = color;
			Gizmos.DrawCube(center, size);
		}

		public static void DrawCombinedCube(Vector3 center, Vector3 size, Color color)
		{
			GizmosUtility.DrawCombinedCube(center, size, color, DEFAULT_TRANSPARENCY_ALPHA);
		}

		public static void DrawCombinedCube(Vector3 center, Vector3 size, Color color, float alpha, Transform transform)
		{
			GizmosUtility.DrawCombinedCube(center, Vector3.Scale(size, transform.localScale), color, alpha);
		}

		public static void DrawCombinedCube(Vector3 center, Vector3 size, Color color, Transform transform)
		{
			GizmosUtility.DrawCombinedCube(center, Vector3.Scale(size, transform.localScale), color, DEFAULT_TRANSPARENCY_ALPHA);
		}

		public static void DrawCombinedMesh(Mesh mesh, Vector3 position, Quaternion rotation, Vector3 scale, Color color, float alpha)
		{
			Gizmos.color = color;
			Gizmos.DrawWireMesh(mesh, position, rotation, scale);

			color.a = alpha;

			Gizmos.color = color;
			Gizmos.DrawMesh(mesh, position, rotation, scale);
		}

		public static void DrawCombinedMesh(Mesh mesh, Vector3 position, Quaternion rotation, Vector3 scale, Color color)
		{
			GizmosUtility.DrawCombinedMesh(mesh, position, rotation, scale, color, DEFAULT_TRANSPARENCY_ALPHA);
		}

		public static void DrawCombinedSphere(Vector3 center, float radius, Color color, float alpha)
		{
			Gizmos.color = color;
			Gizmos.DrawWireSphere(center, radius);

			color.a = alpha;

			Gizmos.color = color;
			Gizmos.DrawSphere(center, radius);
		}

		public static void DrawCombinedSphere(Vector3 center, float radius, Color color)
		{
			GizmosUtility.DrawCombinedSphere(center, radius, color, DEFAULT_TRANSPARENCY_ALPHA);
		}
	}
}