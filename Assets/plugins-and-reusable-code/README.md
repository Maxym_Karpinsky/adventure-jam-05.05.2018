What is this Repository:

This is reusable code that can be used in Unity to decrease the time of development of simple/complicated reusable things.
If you want, you can contribute by pulling request.
If the code you contributed works I may directly add it to repository or change the syntax and correct some of the code to my liking.
If you consider contributing - try to use the naming style that you see in the current code. It may look weird, but there is a sheet on pdf file which explains the naming convention, just go to - .

License:

![CC-BY SA](~Utility_Icons/~Repository/CC-BY_SA.png)

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
