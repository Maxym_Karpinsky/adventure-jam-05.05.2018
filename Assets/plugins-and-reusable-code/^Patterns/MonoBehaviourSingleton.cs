﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
    public abstract class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviourSingleton<T>
    {
        public static T Instance_ { get; private set; }

        protected virtual void Awake()
        {
#if UNITY_EDITOR
            // This is just a hint
            // If the types are different it will throw an error anyway in future steps
            if (this.GetType() != typeof(T))
                Debug.LogWarning("Instance type: " + this.GetType().Name + " and encapsulation type: " + typeof(T).Name + " are different. This may lead to errors or unexpected behavior.");
#endif

            if (Instance_ == null)
            {
                Instance_ = this as T;
                if (Instance_.transform.parent == null)
                {
                    DontDestroyOnLoad(Instance_.gameObject);
                }
            }
            else if (Instance_ != this)
                Destroy(this.gameObject);
        }
    }
}