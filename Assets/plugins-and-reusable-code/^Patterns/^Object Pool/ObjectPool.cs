﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance.UTILITY;
using Kimo.Collections.Generic;

namespace Kimo.Assistance
{
    public sealed class ObjectPool : MonoBehaviourSingleton<ObjectPool>
    {
        public static long PoolObjectFreeInstanceID { get; private set; }

        public long GetNewPoolObjectFreeInstanceID()
        {
            PoolObjectFreeInstanceID++;
            return PoolObjectFreeInstanceID;
        }

        private Dictionary<long, QueueKimo<GameObject>> _objectPool;
        // [SerializeField] private int _defaultLocalPoolSize = 5;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="poolObject"></param>
        /// <param name="localObjectPool"></param>
        private void PutInsidePool(GameObject poolObject, QueueKimo<GameObject> localObjectPool)
        {
            poolObject.SetActive(false);
            poolObject.transform.SetParent(this.transform); // If you change scenes and pooled objects aren't inside object pool they will become null in queues

            localObjectPool.Enqueue(poolObject);
        }

        /// <summary>
        /// 
        /// Usually used on prefabs to pool created object
        /// </summary>
        /// <param name="clonningGameObject"></param>
        public void PoolObjectClone(GameObject clonningGameObject)
        {
            QueueKimo<GameObject> localObjectPool;

            // If this object isn't poolable object then make him
            PoolableObject clonningPoolObject = clonningGameObject.GetComponent<PoolableObject>();
            if (clonningPoolObject == null)
                clonningPoolObject = clonningGameObject.AddComponent<PoolableObject>();

            if (clonningPoolObject.Identifier.InstanceID_ == 0 || !this._objectPool.TryGetValue(clonningPoolObject.Identifier.InstanceID_, out localObjectPool))
            {
				// localObjectPool = new QueueKimo<GameObject>(this._defaultLocalPoolSize);
				localObjectPool = new QueueKimo<GameObject>();

				clonningPoolObject.Identifier = new PoolObject(this.GetNewPoolObjectFreeInstanceID());
                this._objectPool.Add(clonningPoolObject.Identifier.InstanceID_, localObjectPool);
            }
            
            // Clone the poolable object and make sure he has the same InstanceID_
            GameObject clonnedGameObject = Instantiate(clonningGameObject);
            clonnedGameObject.GetComponent<PoolableObject>().Identifier = clonningPoolObject.Identifier;

            this.PutInsidePool(clonnedGameObject, localObjectPool);
        }

        /// <summary>
        /// Usually used on prefabs to pool created objects
        /// </summary>
        /// <param name="clonningGameObject"></param>
        /// <param name="quantity"></param>
        public void PoolObjectClones(GameObject clonningGameObject, int quantity)
        {
            QueueKimo<GameObject> localObjectPool;

            // If this object isn't poolable object then make him
            PoolableObject clonningPoolObject = clonningGameObject.GetComponent<PoolableObject>();
            if (clonningPoolObject == null)
                clonningPoolObject = clonningGameObject.AddComponent<PoolableObject>();

            if (clonningPoolObject.Identifier.InstanceID_ == 0 || !this._objectPool.TryGetValue(clonningPoolObject.Identifier.InstanceID_, out localObjectPool))
            {
                // localObjectPool = new QueueKimo<GameObject>(this._defaultLocalPoolSize);
				localObjectPool = new QueueKimo<GameObject>();

				clonningPoolObject.Identifier = new PoolObject(this.GetNewPoolObjectFreeInstanceID());
                this._objectPool.Add(clonningPoolObject.Identifier.InstanceID_, localObjectPool);
            }

            for (int i = 0; i < quantity; i++)
            {
                // Clone the poolable object and make sure he has the same InstanceID_
                GameObject clonnedGameObject = Instantiate(clonningGameObject);
                clonnedGameObject.GetComponent<PoolableObject>().Identifier = clonningPoolObject.Identifier;

                this.PutInsidePool(clonnedGameObject, localObjectPool);
            }
        }

        public void PoolObject(PoolableObject poolableObject)
        {
            QueueKimo<GameObject> localObjectPool;

            if (poolableObject.Identifier.InstanceID_ == 0 || !this._objectPool.TryGetValue(poolableObject.Identifier.InstanceID_, out localObjectPool))
            {
				// localObjectPool = new QueueKimo<GameObject>(this._defaultLocalPoolSize);
				localObjectPool = new QueueKimo<GameObject>();

				poolableObject.Identifier = new PoolObject(this.GetNewPoolObjectFreeInstanceID());
                this._objectPool.Add(poolableObject.Identifier.InstanceID_, localObjectPool);
            }

			this.PutInsidePool(poolableObject.gameObject, localObjectPool);

#if UNITY_EDITOR
			PrefabType prefabType = PrefabUtility.GetPrefabType(poolableObject.gameObject);
			if (prefabType != PrefabType.None)// && prefabType != PrefabType.DisconnectedPrefabInstance)
				Debug.LogError("Prefab was put inside object pool. Check if you are not pooling prefab. Prefab type: " + prefabType);
#endif
		}

        public void PoolObject(GameObject poolingGameObject)
        {
            PoolableObject poolableObject;
            if (!poolingGameObject.transform.GetComponent(out poolableObject))
                poolableObject = poolingGameObject.AddComponent<PoolableObject>();

            this.PoolObject(poolableObject);
        }

		//? Under Question
		public void PoolObject(PoolableObject poolableObject, float time)
		{
			this.StartCoroutine(this.PoolObjectProcess(poolableObject, time));
		}

		//? Under Question
		public IEnumerator PoolObjectProcess(PoolableObject poolableObject, float time)
		{
			yield return new WaitForSeconds(time);

			this.PoolObject(poolableObject);
		}

        private void DepoolObject<T>(PoolableObject poolableObject, out T depoolObject, bool looping, bool borrow, float time)
			where T : Component
        {
            QueueKimo<GameObject> localObjectPool;

            if (poolableObject.Identifier.InstanceID_ == 0 || !this._objectPool.TryGetValue(poolableObject.Identifier.InstanceID_, out localObjectPool))
            {
				// localObjectPool = new QueueKimo<GameObject>(this._defaultLocalPoolSize);
				localObjectPool = new QueueKimo<GameObject>();

				poolableObject.Identifier = new PoolObject(this.GetNewPoolObjectFreeInstanceID());
                this._objectPool.Add(poolableObject.Identifier.InstanceID_, localObjectPool);

                depoolObject = Instantiate(poolableObject).GetComponent<T>();
                depoolObject.GetComponent<PoolableObject>().Identifier = poolableObject.Identifier;
            }
            else
            {
                if (localObjectPool.Count > 0 && !localObjectPool.Peek().activeSelf)
                    depoolObject = localObjectPool.Dequeue().GetComponent<T>();
                else
                {
                    depoolObject = Instantiate(poolableObject).GetComponent<T>();
                    depoolObject.GetComponent<PoolableObject>().Identifier = poolableObject.Identifier;
                }
            }

            if (looping)
                localObjectPool.Enqueue(depoolObject.gameObject);
            if (borrow)
                this.StartCoroutine(this.Borrow(depoolObject.gameObject, time));

            depoolObject.gameObject.SetActive(true);
        }

        public T DepoolObject<T>(PoolableObject poolableObject, bool looping = true, bool borrow = false, float time = 2f) where T: Component
        {
            T depoolObject;
            this.DepoolObject(poolableObject, out depoolObject, looping, borrow, time);

            return depoolObject;
        }

        public T DepoolObject<T>(PoolableObject poolableObject, Vector3 newPosition, bool looping = true, bool borrow = false, float time = 2f) where T: Component
        {
            T depoolObject;
            this.DepoolObject(poolableObject, out depoolObject, looping, borrow, time);

            depoolObject.transform.position = newPosition;
            return depoolObject;
        }

		public T DepoolObject<T>(PoolableObject poolableObject, Vector3 newPosition, Quaternion newRotation, bool looping = true, bool borrow = false, float time = 2f) where T : Component
		{
			T depoolObject;
			this.DepoolObject(poolableObject, out depoolObject, looping, borrow, time);

			depoolObject.transform.position = newPosition;
			depoolObject.transform.rotation = newRotation;
			return depoolObject;
		}

		public T DepoolObject<T>(PoolableObject poolableObject, Transform newParent, bool looping = true, bool borrow = false, float time = 2f, bool worldPositionStays = false) where T: Component
        {
            T depoolObject;
            this.DepoolObject(poolableObject, out depoolObject, looping, borrow, time);
            
            depoolObject.transform.SetParent(newParent, worldPositionStays);
            return depoolObject;
        }

		public T DepoolObject<T>(PoolableObject poolableObject, Transform newParent, Vector3 newPosition, bool looping = true, bool borrow = false, float time = 2f, bool worldPositionStays = false) where T : Component
		{
			T depoolObject;
			this.DepoolObject(poolableObject, out depoolObject, looping, borrow, time);

			depoolObject.transform.SetParent(newParent, worldPositionStays);
			depoolObject.transform.position = newPosition;
			return depoolObject;
		}

		private IEnumerator Borrow(GameObject depoolObject, float time)
        {
            yield return new WaitForSeconds(time);
            depoolObject.SetActive(false);
        }

        /// <summary>
        /// Remove specific object from pool
        /// </summary>
        /// <param name="poolableObject">Object to remove</param>
        public void DisposeOfPoolableObjectReference(PoolableObject poolableObject)
        {
            QueueKimo<GameObject> localObjectPool;
            if (this._objectPool.TryGetValue(poolableObject.Identifier.InstanceID_, out localObjectPool))
            {
                localObjectPool.Remove(poolableObject.gameObject);
            }
        }

        [SerializeField] private PoolObjectInitializationContainer[] _poolObjectsInitializationContainer;
        private void InitializePool()
        {
            this._objectPool = new Dictionary<long, QueueKimo<GameObject>>(this._poolObjectsInitializationContainer.Length + 2);

            // For an object to pool
            for (int i = 0; i < this._poolObjectsInitializationContainer.Length; i++)
            {
                this.PoolObjectClones(this._poolObjectsInitializationContainer[i].InitialPoolableGameObject, this._poolObjectsInitializationContainer[i].PoolQuantity);
            }
        }

        protected override void Awake()
        {
            base.Awake();

            this.InitializePool();
        }

#if UNITY_EDITOR
		private void Reset()
		{
			this.name = "Object Pool";
		}
#endif
	}

    [System.Serializable]
    public class PoolObjectInitializationContainer
    {
        [SerializeField] private GameObject _initialPoolableGameObject;
        public GameObject InitialPoolableGameObject { get { return this._initialPoolableGameObject; } }

        [SerializeField] private int _poolQuantity = 5;
        public int PoolQuantity { get { return this._poolQuantity; } }
    }
	
    public struct PoolObject
    {
        public long InstanceID_ { get; private set; }

        public PoolObject(long instanceID)
        {
            this.InstanceID_ = instanceID;
        }
    }
}

namespace Kimo.Assistance.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(ObjectPool))]
    [CanEditMultipleObjects]
    public class ObjectPoolEditor : Editor
    {
        // private SerializedProperty _defaultLocalPoolSizeProp;
        private SerializedProperty _poolObjectsInitializationContainerProp;

        private void OnEnable()
        {
            // this._defaultLocalPoolSizeProp = serializedObject.FindProperty("_defaultLocalPoolSize");
            this._poolObjectsInitializationContainerProp = serializedObject.FindProperty("_poolObjectsInitializationContainer");
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            ObjectPool sAudioSourceBehavior = target as ObjectPool;
#pragma warning restore 0219
        }
    }
#endif
}
