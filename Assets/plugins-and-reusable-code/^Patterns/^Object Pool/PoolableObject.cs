﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
    public class PoolableObject : MonoBehaviour
    {
        public PoolObject Identifier { get; set; }

        private void OnDestroy()
        {
            ObjectPool.Instance_.DisposeOfPoolableObjectReference(this);
        }

#if UNITY_EDITOR
        protected virtual void OnDrawGizmos()
        {
            
        }
#endif
    }
}

namespace Kimo.Assistance.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(PoolableObject))]
    [CanEditMultipleObjects]
    public class PoolableObjectEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            PoolableObject sPoolableObject = target as PoolableObject;
#pragma warning restore 0219
        }
    }
#endif
}