﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;
using Kimo.Assistance.UTILITY;

namespace Kimo.Audio
{
	[System.Serializable]
    public class AudioPlayerPlaybackEngine
    {
		[SerializeField] private AudioSourceBehavior.AudioSourceBehaviourPresetType _usedPresetType;
		[SerializeField] private AudioArchivePlaybackEngine _audioArchivePlaybackEngine;

		[SerializeField] private AudioArchiveMultiplePlaybackEngine _audioArchiveMultiplePlaybackEngine;
    }
}

