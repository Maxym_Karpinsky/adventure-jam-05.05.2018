﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

using Kimo.Core.PCG;

namespace Kimo.Audio
{
    [System.Serializable]
    public class AudioArchivePlaybackEngine
    {
        [SerializeField] protected AudioSourceBehavior playbackAudioSourceBehaviour;
		public AudioSourceBehavior _PlaybackAudioSourceBehaviour { get { return this.playbackAudioSourceBehaviour; } }

        [SerializeField] [HideInDerivedInspector] protected AudioArchive activePlaybackAudioArchive;
		public AudioArchive _ActivePlaybackAudioArchive { get { return this.activePlaybackAudioArchive; } }

        public void Play()
        {
            AudioPlayer.PlayClip(this.playbackAudioSourceBehaviour.Source_, this.activePlaybackAudioArchive.GetCurrentAudioClip());
        }

		public void PlayNext()
        {
            AudioPlayer.PlayClip(this.playbackAudioSourceBehaviour.Source_, this.activePlaybackAudioArchive.GetNextAudioClip());
        }

        public void PlayPrevious()
        {
            AudioPlayer.PlayClip(this.playbackAudioSourceBehaviour.Source_, this.activePlaybackAudioArchive.GetPreviousAudioClip());
        }

		public void PlayRandom()
		{
			AudioPlayer.PlayClip(this.playbackAudioSourceBehaviour.Source_, this.activePlaybackAudioArchive.GetRandomAudioClip());
		}

		public void Stop()
		{
			this.playbackAudioSourceBehaviour.Source_.Stop();
		}

		public void Pause()
		{
			this.playbackAudioSourceBehaviour.Source_.Pause();
		}

		public void Resume()
		{
			this.playbackAudioSourceBehaviour.Source_.UnPause();
		}

        public AudioArchivePlaybackEngine(AudioSourceBehavior audioSourceBehavior, AudioArchive audioArchive)
        {
            this.playbackAudioSourceBehaviour = audioSourceBehavior;
            this.activePlaybackAudioArchive = audioArchive;
        }

        public enum PlaybackMode { Normal, Random }

		public void PlayArchive(PlaybackMode playbackMode)
		{
			this.playbackAudioSourceBehaviour.StopAllCoroutines();
			switch (playbackMode)
			{
				case PlaybackMode.Normal:

					this.playbackAudioSourceBehaviour.StartCoroutine(AudioArchivePlaybackEngine.PlayArchiveNormalProcess(this.playbackAudioSourceBehaviour.Source_, this.activePlaybackAudioArchive));

					break;
				case PlaybackMode.Random:

					this.playbackAudioSourceBehaviour.StartCoroutine(AudioArchivePlaybackEngine.PlayArchiveRandomProcess(this.playbackAudioSourceBehaviour.Source_, this.activePlaybackAudioArchive));

					break;
			}
		}

		public void PlayArchive(PlaybackMode playbackMode, MinMaxFloat minMaxTimeWindow)
		{
			this.playbackAudioSourceBehaviour.StopAllCoroutines();
			switch (playbackMode)
			{
				case PlaybackMode.Normal:

					this.playbackAudioSourceBehaviour.StartCoroutine(
						AudioArchivePlaybackEngine.PlayArchiveNormalProcess(
							this.playbackAudioSourceBehaviour.Source_, 
							this.activePlaybackAudioArchive, 
							minMaxTimeWindow
						)
					);

					break;
				case PlaybackMode.Random:

					this.playbackAudioSourceBehaviour.StartCoroutine(
						AudioArchivePlaybackEngine.PlayArchiveRandomProcess(
							this.playbackAudioSourceBehaviour.Source_, 
							this.activePlaybackAudioArchive, 
							minMaxTimeWindow
						)
					);

					break;
			}
		}

		public static void PlayArchive(AudioSourceBehavior audioSourceBehavior, AudioArchive audioArchive, PlaybackMode playbackMode)
        {
            audioSourceBehavior.StopAllCoroutines();
            switch (playbackMode)
            {
				case PlaybackMode.Normal:

					audioSourceBehavior.StartCoroutine(AudioArchivePlaybackEngine.PlayArchiveNormalProcess(audioSourceBehavior.Source_, audioArchive));

					break;
				case PlaybackMode.Random:

					audioSourceBehavior.StartCoroutine(AudioArchivePlaybackEngine.PlayArchiveRandomProcess(audioSourceBehavior.Source_, audioArchive));

                    break;
            }
        }

		private static IEnumerator PlayArchiveRandomProcess(AudioSource audioSource, AudioArchive audioArchive)
        {
            for (int i = 0; i < audioArchive.AudioClipsCount; i++)
            {
                AudioClip audioClip = audioArchive.GetRandomAudioClip();

                AudioPlayer.PlayClip(audioSource, audioClip);

                yield return new WaitForSecondsRealtime(audioClip.length);
            }
        }

		private static IEnumerator PlayArchiveRandomProcess(AudioSource audioSource, AudioArchive audioArchive, MinMaxFloat minMaxTimeWindow)
		{
			for (int i = 0; i < audioArchive.AudioClipsCount; i++)
			{
				AudioClip audioClip = audioArchive.GetRandomAudioClip();

				AudioPlayer.PlayClip(audioSource, audioClip);

				yield return new WaitForSecondsRealtime(audioClip.length + minMaxTimeWindow.GetRandomValue());
			}
		}

        private static IEnumerator PlayArchiveNormalProcess(AudioSource audioSource, AudioArchive audioArchive)
        {
            AudioClip audioClip = audioArchive.GetCurrentAudioClip();

            AudioPlayer.PlayClip(audioSource, audioClip);

            yield return new WaitForSecondsRealtime(audioClip.length);

            for (int i = 1; i < audioArchive.AudioClipsCount; i++)
            {
                audioClip = audioArchive.GetNextAudioClip();

                AudioPlayer.PlayClip(audioSource, audioClip);

                yield return new WaitForSecondsRealtime(audioClip.length);
            }
        }

		private static IEnumerator PlayArchiveNormalProcess(AudioSource audioSource, AudioArchive audioArchive, MinMaxFloat minMaxTimeWindow)
		{
			AudioClip audioClip = audioArchive.GetCurrentAudioClip();

			AudioPlayer.PlayClip(audioSource, audioClip);

			yield return new WaitForSecondsRealtime(audioClip.length + minMaxTimeWindow.GetRandomValue());

			for (int i = 1; i < audioArchive.AudioClipsCount; i++)
			{
				audioClip = audioArchive.GetNextAudioClip();

				AudioPlayer.PlayClip(audioSource, audioClip);

				yield return new WaitForSecondsRealtime(audioClip.length + minMaxTimeWindow.GetRandomValue());
			}
		}
	}
}