﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Audio;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Audio
{
	[System.Serializable]
    [RequireComponent(typeof(AudioSource))]
    public class AudioSourceBehavior : PoolableObject
    {
		public enum AudioSourceBehaviourPresetType
		{
			Default,
			Loud,
			Quiet,
			Audio3D,
			Audio2D
		}

		[SerializeField] private AudioSourceBehaviourPresetType _behaviourPresetType;
		public AudioSourceBehaviourPresetType _BehaviorPresetType { get { return this._behaviourPresetType; } }

        public AudioSource Source_ { get; protected set; }

		protected virtual void Awake()
        {
            this.Source_ = this.GetComponent<AudioSource>();
        }

        protected virtual void Start()
        {

        }

#if UNITY_EDITOR
		protected internal void Initialize(AudioSourceBehaviourPresetType audioSourceBehaviourPresetType, AudioMixerGroup audioMixerGroup)
		{
			this._behaviourPresetType = audioSourceBehaviourPresetType;
			this.GetComponent<AudioSource>().outputAudioMixerGroup = audioMixerGroup;
		}

		private void OnDestroy()
		{
			Debug.LogWarning("If the object was a part of Audio Player - its reference was lost");
		}

		protected override void OnDrawGizmos()
        {
            base.OnDrawGizmos();
        }
#endif
    }
}

namespace Kimo.Audio.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(AudioSourceBehavior))]
    [CanEditMultipleObjects]
    public class AudioSourceBehaviorEditor : Editor
    {
		//private void CustomOnSceneGUI(SceneView sceneView)
		//{
		//	AudioSourceBehavior sAudioSourceBehavior = target as AudioSourceBehavior;

		//	Event e = Event.current;
		//	if (e != null)
		//	{
		//		switch (e.type)
		//		{
		//			case EventType.KeyDown:

		//				Debug.Log(Event.current);

		//				if (Event.current.Equals(Event.KeyboardEvent("delete")) &&
		//					Selection.activeGameObject == sAudioSourceBehavior.gameObject)
		//				{
		//					if (EditorUtility.DisplayDialog(
		//						"Object Deletion",
		//						"Are you sure you want to delete this object?\nIf the object was part of Audio Player - its reference will be lost",
		//						"Continue",
		//						"Cancel"))
		//					{
		//						DestroyImmediate(sAudioSourceBehavior.gameObject);
		//					}
		//				}

		//				break;
		//		}
		//	}

		//}

        private void OnEnable()
        {
			//SceneView.onSceneGUIDelegate -= this.CustomOnSceneGUI;
			//SceneView.onSceneGUIDelegate += this.CustomOnSceneGUI;
		}

		public override void OnInspectorGUI()
        {
			DrawDefaultInspector();

#pragma warning disable 0219
            AudioSourceBehavior sAudioSourceBehavior = target as AudioSourceBehavior;
#pragma warning restore 0219
		}
    }
#endif
}