﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Audio;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;
using Kimo.Assistance.UTILITY;

namespace Kimo.Audio
{
	[System.Serializable]
    public class AudioPlayerData
    {
		public const string AUDIO_SOURCE_BEHAVIOUR_PRESETS_CONTAINER_TAG = "Audio Source Behaviour Presets Container";

		[SerializeField] private AudioPlayer.AudioPlayerType _audioPlayerType;
		public AudioPlayer.AudioPlayerType _AudioPlayerType { get { return this._audioPlayerType; } }

		private string _volumeKey;

		[SerializeField] private AudioMixerGroup _audioMixerGroup;
		public AudioMixerGroup _AudioMixerGroup { get { return this._audioMixerGroup; } }

		private float _volume;
		public float Volume
		{
			get { return this._volume; }
			set
			{
				this._volume = Mathf.Lerp(-80f, 0f, value);
				this._audioMixerGroup.audioMixer.SetFloat(this._volumeKey, this._volume);
			}
		}

		[SerializeField] private AudioSourceBehavior[] _audioSourceBehaviorPresets = new AudioSourceBehavior[Enum.GetNames(typeof(AudioSourceBehavior.AudioSourceBehaviourPresetType)).Length];
		private Dictionary<int, AudioSourceBehavior> _audioSourceBehaviorPresetType_presetRelation;

		public AudioSourceBehavior GetAudioSourceBehaviourPreset(AudioSourceBehavior.AudioSourceBehaviourPresetType audioSourceBehaviourPresetType)
		{
			return this._audioSourceBehaviorPresetType_presetRelation[(int)audioSourceBehaviourPresetType];
		}

		[SerializeField] private AudioPlayerPlaybackEngine _audioPlayerPlaybackEngine;
		public AudioPlayerPlaybackEngine _AudioPlayerPlaybackEngine { get { return this._audioPlayerPlaybackEngine; } }

		public void InitializeAudioSourceBehaviourPresets()
		{
			this._audioSourceBehaviorPresetType_presetRelation = new Dictionary<int, AudioSourceBehavior>(this._audioSourceBehaviorPresets.Length);

			for (int i = 0; i < this._audioSourceBehaviorPresets.Length; i++)
			{
				if (!this._audioSourceBehaviorPresetType_presetRelation.ContainsKey((int)this._audioSourceBehaviorPresets[i]._BehaviorPresetType))
				{
					ObjectPool.Instance_.PoolObjectClone(this._audioSourceBehaviorPresets[i].gameObject);

					this._audioSourceBehaviorPresetType_presetRelation.Add((int)this._audioSourceBehaviorPresets[i]._BehaviorPresetType, this._audioSourceBehaviorPresets[i]);
				}
#if UNITY_EDITOR
				else
					Debug.LogWarning("Multiple presets of the same type aren't supported (Code can't guess which one of them to use that is why they should be unique). Consider using unique preset types per Source Behavior Presets.");
#endif
			}

			this._volumeKey = this._audioPlayerType.ToString();
		}

#if UNITY_EDITOR
		public bool IsInitialized_ { get; private set; }

		protected internal void Initialize(AudioPlayer.AudioPlayerType audioPlayerType)
		{
			this._audioPlayerType = audioPlayerType;

			this.IsInitialized_ = true;
		}

		public void ResetAudioSourceBehaviourPresets(string containerName, AudioPlayer audioPlayer)
		{
			this._audioMixerGroup = audioPlayer._AudioMixer.FindMatchingGroups(containerName)[0];
			
			string[] audioSourceBehaviourPresetTypeNames = Enum.GetNames(typeof(AudioSourceBehavior.AudioSourceBehaviourPresetType));

			// Delete old data and objects
			for (int i = 0; i < this._audioSourceBehaviorPresets.Length; i++)
				if (this._audioSourceBehaviorPresets[i] != null)
					UnityEngine.Object.DestroyImmediate(this._audioSourceBehaviorPresets[i].gameObject);

			// Create new objects / Reset
			GameObject presetsContainer = new GameObject(containerName);
			presetsContainer.transform.SetParent(audioPlayer.transform);
			presetsContainer.tag = AUDIO_SOURCE_BEHAVIOUR_PRESETS_CONTAINER_TAG;

			this._audioSourceBehaviorPresets = new AudioSourceBehavior[Enum.GetNames(typeof(AudioSourceBehavior.AudioSourceBehaviourPresetType)).Length];

			for (int i = 0; i < audioSourceBehaviourPresetTypeNames.Length; i++)
			{
				GameObject audioSourceBehaviorGameObject = new GameObject(string.Format("Audio Source Behaviour Preset - {0} - {1}", this._audioPlayerType.ToString(), audioSourceBehaviourPresetTypeNames[i]));
				audioSourceBehaviorGameObject.transform.SetParent(presetsContainer.transform);

				this._audioSourceBehaviorPresets[i] = audioSourceBehaviorGameObject.AddComponent<AudioSourceBehavior>();
				this._audioSourceBehaviorPresets[i].Initialize(audioSourceBehaviourPresetTypeNames[i].ToEnum<AudioSourceBehavior.AudioSourceBehaviourPresetType>(false), this._audioMixerGroup);
			}
		}
#endif
	}
}

namespace Kimo.Audio.UTILITY
{
#if UNITY_EDITOR
	[CustomPropertyDrawer(typeof(AudioPlayerData))]
	public class AudioPlayerDataDrawer : MultiSupportPropertyAttributeDrawer
	{
		private bool _foldOut;
		private bool _presetsFoldOut;

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return 0;
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			SerializedProperty audioPlayerTypeProp = property.FindPropertyRelative("_audioPlayerType");
			SerializedProperty audioMixerGroupProp = property.FindPropertyRelative("_audioMixerGroup");
			SerializedProperty audioSourceBehaviourPresetsProp = property.FindPropertyRelative("_audioSourceBehaviorPresets");
			SerializedProperty audioPlayerPlaybackEngineProp = property.FindPropertyRelative("_audioPlayerPlaybackEngine");

			EditorGUI.BeginProperty(position, label, property);

			EditorGUILayout.BeginVertical(
				GUIStyleUtility.GetBoxStyle(
					Texture2DUtility.CreateTexture(
						10, 
						50, 
						new Color(0.9f, 0.9f, 0.9f, 0.2f), 
						Color.black, 
						new Texture2DUtility.Texture2DBorder(1, 1, 1, 1)
					), 
					new RectOffset(0, 5, 5, 5)
				)
			);

			this._foldOut = EditorPrefs.GetBool(audioPlayerTypeProp.propertyPath, false);
			this._foldOut = EditorGUILayout.Foldout(this._foldOut, new GUIContent(audioPlayerTypeProp.GetEnumDisplayName()), true);
			EditorPrefs.SetBool(audioPlayerTypeProp.propertyPath, this._foldOut);

			if (this._foldOut)
			{
				EditorGUI.indentLevel++;
				GUI.enabled = false;

				EditorGUILayout.PropertyField(audioPlayerTypeProp);
				EditorGUILayout.PropertyField(audioMixerGroupProp);

				GUI.enabled = true;

				this._presetsFoldOut = EditorPrefs.GetBool(audioSourceBehaviourPresetsProp.propertyPath, false);
				this._presetsFoldOut = EditorGUILayout.Foldout(this._presetsFoldOut, new GUIContent("Audio Source Behaviour Presets"), true);
				EditorPrefs.SetBool(audioSourceBehaviourPresetsProp.propertyPath, this._presetsFoldOut);

				if (this._presetsFoldOut)
				{
					// SerializedProperty audioSourceBehaviourPresetsProp = property.FindPropertyRelative("_audioSourceBehaviorPresets");

					string[] audioSourceBehaviourPresetTypeNames = Enum.GetNames(typeof(AudioSourceBehavior.AudioSourceBehaviourPresetType));

					EditorGUI.indentLevel++;

					for (int i = 0; i < audioSourceBehaviourPresetsProp.arraySize; i++)
					{
						SerializedProperty presetProp = audioSourceBehaviourPresetsProp.GetArrayElementAtIndex(i);

						EditorGUILayout.PropertyField(presetProp, new GUIContent(audioSourceBehaviourPresetTypeNames[i]), true);
					}

					EditorGUI.indentLevel--;
				}

				EditorGUILayout.PropertyField(audioPlayerPlaybackEngineProp, true);

				EditorGUI.indentLevel--;
			}

			EditorGUILayout.EndVertical();

			EditorGUI.EndProperty();
		}
	}
#endif
}