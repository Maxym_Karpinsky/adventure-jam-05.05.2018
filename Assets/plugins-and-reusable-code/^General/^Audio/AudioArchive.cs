﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Audio
{
	[CreateAssetMenu(fileName = "s Audio Archive", menuName = "Archives/Audio/Audio Archive")]
	public class AudioArchive : ScriptableObject
    {
        [SerializeField] private AudioClip[] _audioClips;
        public int AudioClipsCount { get { return this._audioClips.Length; } }

        public AudioClip GetRandomAudioClip()
        {
            return this._audioClips[Random.Range(0, this._audioClips.Length)];
        }

        private int _clipIndex = 0;
        public void ResetClipIndex()
        {
            this._clipIndex = 0;
        }

        public AudioClip GetCurrentAudioClip()
        {
            return this._audioClips[this._clipIndex];
        }

        public AudioClip GetNextAudioClip()
        {
            this._clipIndex = (this._clipIndex + 1) % this._audioClips.Length;

            return this._audioClips[this._clipIndex];
        }

        public AudioClip GetPreviousAudioClip()
        {
            this._clipIndex = (this._clipIndex - 1 + this._audioClips.Length) % this._audioClips.Length;

            return this._audioClips[this._clipIndex];
        }
    }
}