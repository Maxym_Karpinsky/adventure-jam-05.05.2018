﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Audio
{
    public class AudioProcessing : MonoBehaviour 
    {
		public static float[] GetAudioSpectrum(AudioSource audioSource, int numberOfSamples, FFTWindow fftWindow)
		{
			float[] spectrum = new float[numberOfSamples];

			// populate array with fequency spectrum data
			audioSource.GetSpectrumData(spectrum, 0, fftWindow);

			return spectrum;
		}

		public Transform[] audioSpectrumObjects;
		[Range(1, 100)] public float heightMultiplier;
		[Range(64, 8192)] public int numberOfSamples = 1024; //step by 2
		public FFTWindow fftWindow;
		public float lerpTime = 1;
		public Slider sensitivitySlider;

		/*
		 * The intensity of the frequencies found between 0 and 44100 will be
		 * grouped into 1024 elements. So each element will contain a range of about 43.06 Hz.
		 * The average human voice spans from about 60 hz to 9k Hz
		 * we need a way to assign a range to each object that gets animated. that would be the best way to control and modify animatoins.
		*/

		void Start()
		{
			sensitivitySlider.onValueChanged.AddListener(delegate {
				SensitivityValueChangedHandler(sensitivitySlider);
			});
		}

		void Update()
		{

			// initialize our float array
			float[] spectrum = new float[numberOfSamples];

			// populate array with fequency spectrum data
			GetComponent<AudioSource>().GetSpectrumData(spectrum, 0, this.fftWindow);


			// loop over audioSpectrumObjects and modify according to fequency spectrum data
			// this loop matches the Array element to an object on a One-to-One basis.
			for (int i = 0; i < audioSpectrumObjects.Length; i++)
			{

				// apply height multiplier to intensity
				float intensity = spectrum[i] * heightMultiplier;

				// calculate object's scale
				float lerpY = Mathf.Lerp(audioSpectrumObjects[i].localScale.y, intensity, lerpTime);
				Vector3 newScale = new Vector3(audioSpectrumObjects[i].localScale.x, lerpY, audioSpectrumObjects[i].localScale.z);

				// appply new scale to object
				audioSpectrumObjects[i].localScale = newScale;

			}
		}

		public void SensitivityValueChangedHandler(Slider sensitivitySlider)
		{
			heightMultiplier = sensitivitySlider.value;
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{

		//}
#endif
	}
}

namespace Kimo.Audio.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(AudioProcessing))]
    [CanEditMultipleObjects]
    public class AudioProcessingEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            AudioProcessing sAudioProcessing = target as AudioProcessing;
#pragma warning restore 0219
        }
    }
#endif
}