﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Audio
{
    public class AudioArchivePlaybackEngineBehaviour : MonoBehaviour 
    {
		[SerializeField] private AudioArchivePlaybackEngine _audioArchivePlaybackEngine;
		[SerializeField] private bool _playRandomLoop;

		[SerializeField][MinMaxFloatRange(0, 500)] private MinMaxFloat _timeWindow;

		private void Start()
		{
			if (this._playRandomLoop)
			{
				this._audioArchivePlaybackEngine._PlaybackAudioSourceBehaviour.Source_.loop = this._playRandomLoop;
				this._audioArchivePlaybackEngine.PlayRandom();
			}
			else
				this._audioArchivePlaybackEngine.PlayArchive(AudioArchivePlaybackEngine.PlaybackMode.Normal, this._timeWindow);
		}

#if UNITY_EDITOR
		protected virtual void OnDrawGizmos()
        {
            
        }
#endif
    }
}

namespace Kimo.Audio.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(AudioArchivePlaybackEngineBehaviour))]
    [CanEditMultipleObjects]
    public class AudioArchivePlaybackEngineBehaviourEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            AudioArchivePlaybackEngineBehaviour sAudioArchivePlaybackEngineBehaviour = target as AudioArchivePlaybackEngineBehaviour;
#pragma warning restore 0219
        }
    }
#endif
}