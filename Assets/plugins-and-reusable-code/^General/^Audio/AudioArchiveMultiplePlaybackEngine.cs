﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Audio
{
    [System.Serializable]
    public class AudioArchiveMultiplePlaybackEngine : AudioArchivePlaybackEngine
    {
        [SerializeField] private AudioArchive[] _playbackAudioArchives;

		private int _activeArchiveIndex = 0;
		public void ResetEngine()
		{
			this._activeArchiveIndex = 0;
		}

		public AudioArchive GetCurrentArchive()
		{
			return this._playbackAudioArchives[this._activeArchiveIndex];
		}

		public void PlayNextArchive()
		{
			this._activeArchiveIndex = (this._activeArchiveIndex + 1) % this._playbackAudioArchives.Length;

			this.activePlaybackAudioArchive = this._playbackAudioArchives[this._activeArchiveIndex];

			this.Play();
		}

		public void PlayPreviousArchive()
		{
			this._activeArchiveIndex = (this._activeArchiveIndex - 1 + this._playbackAudioArchives.Length) % this._playbackAudioArchives.Length;

			this.activePlaybackAudioArchive = this._playbackAudioArchives[this._activeArchiveIndex];

			this.Play();
		}

		public AudioArchiveMultiplePlaybackEngine(AudioSourceBehavior audioSourceBehavior, AudioArchive[] audioArchives) : base(audioSourceBehavior, audioArchives[0])
		{
			this._playbackAudioArchives = audioArchives;
		}
	}
}