﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Audio;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;
using Kimo.Assistance.UTILITY;

namespace Kimo.Audio
{
    public sealed class AudioPlayer : MonoBehaviourSingleton<AudioPlayer>
    {
		public enum AudioType
		{
			Diegetic, // The ones that exist in game world
			Nondiegetic // The ones that only (player/person playing the game) can hear
		}

		public enum AudioPlayerType
		{
			Music,
			Ambience,
			Specialty,
			Foley,
			Narrative
		}

		[SerializeField] private AudioMixer _audioMixer;
		public AudioMixer _AudioMixer { get { return this._audioMixer; } }

		[SerializeField] private AudioPlayerData[] _audioPlayerData = new AudioPlayerData[Enum.GetNames(typeof(AudioPlayerType)).Length];
		public AudioPlayerData[] _AudioPlayerData { get { return this._audioPlayerData; } }

		private static Dictionary<int, AudioPlayerData> s_audioPlayerType_audioPlayerDataRelation;

		public AudioPlayerData GetAudioPlayerData(AudioPlayerType audioPlayerType)
		{
			return s_audioPlayerType_audioPlayerDataRelation[(int)audioPlayerType];
		}

		// AudioPlayerPlaybackEngine

		// User Interface 

		[SerializeField] private AudioArchiveMultiplePlaybackEngine AudioArchivePlaybackEngineBehaviour;

		public const string MASTER_VOLUME_KEY = "Master Volume";

		private float _volume;
		public float Volume
		{
			get { return this._volume; }
			set
			{
				this._volume = Mathf.Lerp(-80f, 0f, value);
				this._audioMixer.SetFloat(AudioPlayer.MASTER_VOLUME_KEY, this._volume);
			}
		}

		public void InitializeAudioPlayerData()
		{
			s_audioPlayerType_audioPlayerDataRelation = new Dictionary<int, AudioPlayerData>(this._audioPlayerData.Length);

			string[] audioPlayerTypesNames = Enum.GetNames(typeof(AudioPlayerType));

			if (this._audioPlayerData.Length != audioPlayerTypesNames.Length)
				Debug.LogError("The audio player data and type names aren't of the same size");

			for (int i = 0; i < this._audioPlayerData.Length; i++)
			{
				this._audioPlayerData[i].InitializeAudioSourceBehaviourPresets();

				s_audioPlayerType_audioPlayerDataRelation.Add((int)audioPlayerTypesNames[i].ToEnum<AudioPlayerType>(false), this._audioPlayerData[i]);
			}

			// this.transform.DestroyChildrenWithTag(AudioPlayerData.AUDIO_SOURCE_BEHAVIOUR_PRESETS_CONTAINER_TAG);
		}

		public static AudioSource GetFreeAudioSource(AudioPlayerType audioPlayerDataType, AudioSourceBehavior.AudioSourceBehaviourPresetType audioSourceBehaviourPresetType = AudioSourceBehavior.AudioSourceBehaviourPresetType.Default)
		{
			AudioPlayerData audioPlayerData = s_audioPlayerType_audioPlayerDataRelation[(int)audioPlayerDataType];

			return ObjectPool.Instance_.DepoolObject<AudioSourceBehavior>(
					audioPlayerData.GetAudioSourceBehaviourPreset(audioSourceBehaviourPresetType)
				).Source_;
		}

		public static void PlayClipAtWorldPoint(AudioSourceBehavior audioSourceBehavior, AudioClip audioClip, Vector3 worldPoint)
        {
            audioSourceBehavior.transform.position = worldPoint;

            AudioPlayer.PlayClip(audioSourceBehavior.Source_, audioClip);
        }

		public static void PlayClipAtWorldPoint(AudioClip audioClip, Vector3 worldPoint, AudioPlayerType audioPlayerDataType, AudioSourceBehavior.AudioSourceBehaviourPresetType audioSourceBehaviourPresetType = AudioSourceBehavior.AudioSourceBehaviourPresetType.Default)
		{
			AudioPlayerData audioPlayerData = s_audioPlayerType_audioPlayerDataRelation[(int)audioPlayerDataType];

			// [Old presets system code][1]

			AudioPlayer.PlayClip(
				ObjectPool.Instance_.DepoolObject<AudioSourceBehavior>(
					audioPlayerData.GetAudioSourceBehaviourPreset(audioSourceBehaviourPresetType), 
					worldPoint, 
					borrow: true, 
					time: audioClip.length
				).Source_, 
				audioClip
			);

			// [1]
			//			AudioSourceBehavior audioSourceBehavior;
			//			if (audioPlayerData._audioSourceBehaviorPresetType_presetRelation.TryGetValue((int)audioSourceBehaviourPresetType, out audioSourceBehavior))
			//			{
			//				audioSourceBehavior = 
			//			}
			//			else
			//			{
			//				audioSourceBehavior = ObjectPool.Instance_.DepoolObject<AudioSourceBehavior>(audioPlayerData._audioSourceBehaviorPresetType_presetRelation[(int)AudioPlayerData.AudioSourceBehaviourPresetType.Default], worldPoint, borrow: true, time: audioClip.length);
			//#if UNITY_EDITOR
			//				Debug.LogError("There is no such preset type: " + audioSourceBehaviourPresetType + "in presets container! Check if there is such kind of preset with correct type set. The default preset was used.\nThis warning shows up only in editor.");
			//#endif
			//			}
		}

		public static void PlayClip(AudioClip audioClip, AudioPlayerType audioPlayerDataType, AudioSourceBehavior.AudioSourceBehaviourPresetType audioSourceBehaviourPresetType = AudioSourceBehavior.AudioSourceBehaviourPresetType.Default)
		{
			AudioPlayerData audioPlayerData = s_audioPlayerType_audioPlayerDataRelation[(int)audioPlayerDataType];

			AudioPlayer.PlayClip(
				ObjectPool.Instance_.DepoolObject<AudioSourceBehavior>(
					audioPlayerData.GetAudioSourceBehaviourPreset(audioSourceBehaviourPresetType),
					borrow: true,
					time: audioClip.length
				).Source_,
				audioClip
			);
		}

		public static void PlayClip(AudioSource audioSource, AudioClip audioClip)
        {
            audioSource.clip = audioClip;
            audioSource.Play();
        }

        public static void PlayClip(AudioSource audioSource, AudioClip audioClip, float volume)
        {
            audioSource.volume = volume;
            audioSource.clip = audioClip;
            audioSource.Play();
        }

        public static void PlayClip(AudioSource audioSource, AudioClip audioClip, float volume, bool loop)
        {
            audioSource.loop = loop;
            audioSource.volume = volume;
            audioSource.clip = audioClip;
            audioSource.Play();
        }

        public static void PlayClip(AudioSource audioSource, AudioClip audioClip, float volume, float pitch)
        {
            audioSource.pitch = pitch;
            audioSource.volume = volume;
            audioSource.clip = audioClip;
            audioSource.Play();
        }

        public static void PlayClip(AudioSource audioSource, AudioClip audioClip, float volume, bool loop, float pitch)
        {
            audioSource.loop = loop;
            audioSource.volume = volume;
            audioSource.clip = audioClip;
            audioSource.pitch = pitch;
            audioSource.Play();
        }

        public static void PlayClip(AudioSource audioSource, AudioClip audioClip, bool loop, float pitchMin, float pitchMax)
        {
            audioSource.pitch = UnityEngine.Random.Range(pitchMin, pitchMax);
            audioSource.loop = loop;
            audioSource.clip = audioClip;
            audioSource.Play();
        }

        public static void PlayClip(AudioSource audioSource, AudioClip audioClip, float volume, float pitchMin, float pitchMax)
        {
            audioSource.pitch = UnityEngine.Random.Range(pitchMin, pitchMax);
            audioSource.volume = volume;
            audioSource.clip = audioClip;
            audioSource.Play();
        }

        public static void PlayClip(AudioSource audioSource, AudioClip audioClip, float volume, bool loop, float pitchMin, float pitchMax)
        {
            audioSource.pitch = UnityEngine.Random.Range(pitchMin, pitchMax);
            audioSource.loop = loop;
            audioSource.volume = volume;
            audioSource.clip = audioClip;
            audioSource.Play();
        }

        protected override void Awake()
        {
            base.Awake();

			this.InitializeAudioPlayerData();
		}

#if UNITY_EDITOR
		public void ResetAudioPlayerData()
		{
			this._audioMixer = AssetDatabase.LoadAssetAtPath<AudioMixer>(PathUtility.GetScriptFileDirectoryPath(this) + "/Master Audio Mixer.mixer");
			if (this._audioMixer == null)
				Debug.LogWarning("Couldn't find Master Audio Mixer. Drag and drop one yourself.");

			TagUtility.AddTag(AudioPlayerData.AUDIO_SOURCE_BEHAVIOUR_PRESETS_CONTAINER_TAG);

			// Delete old data and objects
			this.transform.DestroyChildrenImmediateWithTag(AudioPlayerData.AUDIO_SOURCE_BEHAVIOUR_PRESETS_CONTAINER_TAG);

			// Create new objects / Reset
			string[] audioPlayerTypesNames = Enum.GetNames(typeof(AudioPlayerType));

			if (this._audioPlayerData.Length != audioPlayerTypesNames.Length)
				Debug.LogError("The audio player data and type names aren't of the same size");

			for (int i = 0; i < this._audioPlayerData.Length; i++)
			{
				this._audioPlayerData[i].Initialize(audioPlayerTypesNames[i].ToEnum<AudioPlayerType>(false));
				this._audioPlayerData[i].ResetAudioSourceBehaviourPresets(audioPlayerTypesNames[i], this);
			}
		}

		public void Reset()
		{
			this.name = "Audio Player";

			if (this._audioPlayerData.Length > 0 && this._audioPlayerData[0] == null)
			{
				for (int i = 0; i < this._audioPlayerData.Length; i++)
				{
					this._audioPlayerData[i] = new AudioPlayerData();
				}

				this.ResetAudioPlayerData();
			}
			else
			{

				if (EditorUtility.DisplayDialog(
					"Reset confirmation",
					"Are you sure you want to reset Audio Player settings?\nAll the related data will be lost.\nBackup gameojbects you want to save before proceeding.\nUnfortunately references to Game Objects are lost anyway :( - Use Reset Audio Player Instead to avoid this issue.",
					"Reset",
					"Cancel")
				)
				{ this.ResetAudioPlayerData(); }
			}
		}

		[ContextMenu("Reset Audio Player Data")]
		public void ResetAudioPlayer()
		{
			this.name = "Audio Player";

			if (EditorUtility.DisplayDialog(
				"Reset confirmation", 
				"Are you sure you want to reset Audio Player settings?\nAll the related data will be lost.\nBackup gameojbects you want to save before proceeding.", 
				"Reset", 
				"Cancel")
			) { this.ResetAudioPlayerData(); }
		}
#endif
	}
}

namespace Kimo.Audio.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(AudioPlayer))]
    [CanEditMultipleObjects]
    public class AudioControllerEditor : Editor
    {
		private SerializedProperty _audioMixerProp;
        private SerializedProperty _audioPlayerDataProp;

		private bool _audioPlayerDataFoldOut;

        private void OnEnable()
        {
			this._audioMixerProp = this.serializedObject.FindProperty("_audioMixer");
			this._audioPlayerDataProp = this.serializedObject.FindProperty("_audioPlayerData");

			this._audioPlayerDataFoldOut = EditorPrefs.GetBool(this._audioPlayerDataProp.propertyPath, true);
        }

		private void DrawAudioPlayer()
		{
			// Black outline
			EditorGUILayout.BeginVertical(
				GUIStyleUtility.GetBoxStyle(
					Texture2DUtility.CreateTexture(
						10,
						50,
						Color.white,
						Color.black,
						new Texture2DUtility.Texture2DBorder(1, 1, 1, 1)
					),
					new RectOffset(0, 0, 0, 0)
				)
			);

			// Yellow outline
			EditorGUILayout.BeginVertical(
				GUIStyleUtility.GetBoxStyle(
					Texture2DUtility.CreateTexture(
						10,
						50,
						Color.white,
						Color.yellow,
						new Texture2DUtility.Texture2DBorder(1, 1, 1, 1)
					),
					new RectOffset(0, 0, 0, 0)
				)
			);

			// Master Audio Mixer
			EditorGUILayout.PropertyField(this._audioMixerProp, true);

			// End Yellow
			EditorGUILayout.EndVertical();
			// End Black
			EditorGUILayout.EndVertical();

			EditorGUILayout.BeginVertical(
				GUIStyleUtility.GetBoxStyle(
					Texture2DUtility.CreateTexture(
						10,
						50,
						Color.white,
						Color.black,
						new Texture2DUtility.Texture2DBorder(1, 1, 1, 1)
					),
					new RectOffset(0, 0, 0, 0)
				)
			);

			this._audioPlayerDataFoldOut = EditorGUILayout.Foldout(this._audioPlayerDataFoldOut, new GUIContent("Audio Player Data"), true);
			EditorPrefs.SetBool(this._audioPlayerDataProp.propertyPath, this._audioPlayerDataFoldOut);

			if (this._audioPlayerDataFoldOut)
			{
				EditorGUI.indentLevel++;

				for (int i = 0; i < this._audioPlayerDataProp.arraySize; i++)
				{
					EditorGUILayout.PropertyField(this._audioPlayerDataProp.GetArrayElementAtIndex(i), true);
				}

				EditorGUI.indentLevel--;
			}

			EditorGUILayout.PropertyField(this.serializedObject.FindProperty("AudioArchivePlaybackEngineBehaviour"), true);

			EditorGUILayout.EndVertical();
		}

		public override void OnInspectorGUI()
        {
			AudioPlayer sAudioController = target as AudioPlayer;

			this.serializedObject.Update();

			EditorGUILayout.BeginVertical(
				GUIStyleUtility.GetBoxStyle(
					Texture2DUtility.CreateTexture(
						10,
						50,
						Color.white,
						new Color(0f, 0.7f, 0.7f, 1),
						new Texture2DUtility.Texture2DBorder(1, 1, 2, 1),
						0.65f
					),
					new RectOffset(0, 5, 15, 0)
				)
			);

			EditorGUI.indentLevel++;

			this.DrawAudioPlayer();

			EditorGUI.indentLevel--;
			EditorGUILayout.EndVertical();

			this.serializedObject.ApplyModifiedProperties();
        }
	}
#endif
}