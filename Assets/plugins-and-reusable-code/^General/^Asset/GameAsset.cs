﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core
{
    public class GameAsset : MonoBehaviour 
    {
        public enum AvailabilityState { Free, Neutral, Obstacle }

        [SerializeField] private AvailabilityState _state = AvailabilityState.Obstacle;
        public AvailabilityState _State { get { return this._state; } }

		[Range(0f, 1f)]
		[SerializeField] private float _probability;
		public float _Probability { get { return this._probability; } }

#if UNITY_EDITOR
        protected virtual void OnDrawGizmos()
        {
            
        }
#endif
    }
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(GameAsset))]
    [CanEditMultipleObjects]
    public class GameAssetEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            GameAsset sGameAsset = target as GameAsset;
#pragma warning restore 0219
        }
    }
#endif
}