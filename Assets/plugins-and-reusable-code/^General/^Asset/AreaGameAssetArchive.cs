﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
	[CreateAssetMenu(fileName = "s Area Game Asset Archive", menuName = "Archives/Game Asset/Area Game Asset Archive")]
	public class AreaGameAssetArchive : GameAssetArchive<AreaGameAsset>
	{
    }
}