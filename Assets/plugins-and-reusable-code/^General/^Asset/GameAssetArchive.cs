﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core
{
    public abstract class GameAssetArchive<TGameAsset> : ScriptableObject
		where TGameAsset : GameAsset
    {
		[SerializeField] private List<TGameAsset> _assets;
        public List<TGameAsset> Assets
		{
			get { return this._assets; }
			set
			{
				this._assets = value;
				this.Initialize();
			}
		}

		public void Initialize()
		{
			// this._assets.Sort();
		}

		public GameAssetArchive()
		{
			this.Initialize();
		}

        public TGameAsset GetRandomAsset()
        {
			float randomProbability = Random.value;

			int i = 0;
			while (i < this._assets.Count - 1 && this._assets[i + 1]._Probability >= randomProbability)
				i++;

            return this._assets[i];
        }

        private int _nextAssetIndex = 0;
        public void ResetAssetIndex()
        {
            this._nextAssetIndex = 0;
        }

        public TGameAsset GetNextAsset()
        {
            TGameAsset asset = this.Assets[this._nextAssetIndex];

            this._nextAssetIndex = (this._nextAssetIndex + 1) % this.Assets.Count;
            return asset;
        }
    }

	[CreateAssetMenu(fileName = "s Game Asset Archive", menuName = "Archives/Game Asset/Game Asset Archive")]
	public class GameAssetArchive : GameAssetArchive<GameAsset>
	{
	}
}