﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance.UTILITY;

namespace Kimo.Core
{
    public class AreaGameAsset : GameAsset 
    {
		[SerializeField] private Vector3 _relativeBoundsSize = new Vector3(0.5f, 0.5f, 0.5f);
		public Vector3 _RelativeBoundsSize { get { return this._relativeBoundsSize; } }

		[SerializeField] private LayerMask _generationRestrictedLayerMask;
		public LayerMask _GenerationRestrictedLayerMask { get { return this._generationRestrictedLayerMask; } }

#if UNITY_EDITOR
		protected override void OnDrawGizmos()
		{
			GizmosUtility.DrawCombinedCube(this.transform.position, this._relativeBoundsSize, Color.red, 0.2f);
		}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(AreaGameAsset))]
    [CanEditMultipleObjects]
    public class AreaGameAssetEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            AreaGameAsset sAreaGameAsset = target as AreaGameAsset;
#pragma warning restore 0219
        }
    }
#endif
}