﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

using TMPro;

using Kimo.Assistance;

namespace Kimo.Core
{
	[System.Serializable]
	public class InventoryItemDisplayData
	{
		[SerializeField] private Image _itemIconImageField;
		public Image _ItemIconImageField { get { return this._itemIconImageField; } }

		[SerializeField] private TextMeshProUGUI _itemQuantityTextField;
		public TextMeshProUGUI _ItemQuantityTextField { get { return this._itemQuantityTextField; } }
	}

	[RequireComponent(typeof(PoolableObject))]
	public class InventoryItemDisplayController : MonoBehaviour 
    {
		[SerializeField] private InventoryItemDisplayData _inventoryItemDisplayData;

		private InventoryItem _displayedItem;

		public virtual void Display(InventoryItem item)
		{
			this._displayedItem = item;

			this.ActivateGameObject();

			this.InitializeLayout();
			this.UpdateLayout();
		}

		public virtual void InitializeLayout()
		{
			if (this._displayedItem == null)
				return;

			if (this._displayedItem.InventoryItemSharedData_.Stackable_)
				this._inventoryItemDisplayData._ItemQuantityTextField.ActivateGameObject();
			else
				this._inventoryItemDisplayData._ItemQuantityTextField.DeactivateGameObject();

			this._inventoryItemDisplayData._ItemIconImageField.ActivateGameObject();
			this._inventoryItemDisplayData._ItemIconImageField.sprite = this._displayedItem.InventoryItemSharedData_.Sprite_;
		}

		public virtual void UpdateLayout()
		{
			if (this._displayedItem == null)
				return;

			if (this._displayedItem.InventoryItemSharedData_.Stackable_)
				this._inventoryItemDisplayData._ItemQuantityTextField.text = this._displayedItem._Quantity.ToString();
		}

		public virtual void Clear(InventoryDisplayController.DisplayClearMode displayClearMode)
		{
			switch (displayClearMode)
			{
				case InventoryDisplayController.DisplayClearMode.Partial:

					this._displayedItem = null;

					this._inventoryItemDisplayData._ItemQuantityTextField.text = "";
					this._inventoryItemDisplayData._ItemQuantityTextField.DeactivateGameObject();

					break;
				case InventoryDisplayController.DisplayClearMode.Full:

					this.DeactivateGameObject();

					this._displayedItem = null;

					this._inventoryItemDisplayData._ItemQuantityTextField.text = "";
					this._inventoryItemDisplayData._ItemQuantityTextField.DeactivateGameObject();

					this._inventoryItemDisplayData._ItemIconImageField.DeactivateGameObject();

					break;
				case InventoryDisplayController.DisplayClearMode.Pooling:

					this.Clear(InventoryDisplayController.DisplayClearMode.Partial);
					ObjectPool.Instance_.PoolObject(this.GetComponent<PoolableObject>()); //TODO: change it to normal method in object pool or cache the poolable object

					break;
				case InventoryDisplayController.DisplayClearMode.Destruction:

					Destroy(this.gameObject);

					break;
			}
		}

#if UNITY_EDITOR
        protected virtual void OnDrawGizmos()
        {
            
        }
#endif
    }
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(InventoryItemDisplayController))]
    [CanEditMultipleObjects]
    public class ItemDisplayControllerEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            InventoryItemDisplayController sItemDisplayController = target as InventoryItemDisplayController;
#pragma warning restore 0219
        }
    }
#endif
}