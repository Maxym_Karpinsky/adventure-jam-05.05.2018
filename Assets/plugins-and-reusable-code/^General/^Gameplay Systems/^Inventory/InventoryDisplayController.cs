﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
	[System.Serializable]
	public class InventoryDisplayData
	{
		[SerializeField] private InventoryItemDisplayController _itemDisplayControllerPrefab;
		public InventoryItemDisplayController _ItemDisplayControllerPrefab { get { return this._itemDisplayControllerPrefab; } }

		[SerializeField] private Transform _itemDisplaysContainer;
		public Transform _ItemDisplaysContainer { get { return this._itemDisplaysContainer; } }
	}

	public class InventoryDisplayController : MonoBehaviourSingleton<InventoryDisplayController>
    {
		public enum DisplayClearMode { Partial, Full, Pooling, Destruction }

		[SerializeField] private InventoryDisplayData _inventoryDisplayData;

		private Inventory _displayedInventory;

		public void Display(Inventory inventory)
		{
			this._displayedInventory = inventory;

			this.InitializeLayout();
			this.UpdateLayout();
		}

		private InventoryItemDisplayController[] _inventoryItemDisplayControllers;

		public void InitializeLayout()
		{
			if (this._displayedInventory == null)
				return;

			this.Clear(DisplayClearMode.Pooling);

			this._inventoryItemDisplayControllers = new InventoryItemDisplayController[this._displayedInventory.Capacity_];

			for (int i = 0; i < this._displayedInventory.Capacity_; i++)
			{
				InventoryItemDisplayController inventoryItemDisplayController = Instantiate(this._inventoryDisplayData._ItemDisplayControllerPrefab, this._inventoryDisplayData._ItemDisplaysContainer);
				this._displayedInventory.AddItemDisplay(inventoryItemDisplayController, i);

				this._inventoryItemDisplayControllers[i] = inventoryItemDisplayController;
			}
		}

		public void UpdateLayout()
		{
			if (this._displayedInventory == null)
				return;

			for (int i = 0; i < this._displayedInventory.Capacity_; i++)
			{
				this._inventoryItemDisplayControllers[i].UpdateLayout();
			}
		}

		public void Clear(DisplayClearMode displayClearMode)
		{
			if (this._inventoryItemDisplayControllers == null)
				return;

			for (int i = 0; i < this._inventoryItemDisplayControllers.Length; i++)
			{
				this._inventoryItemDisplayControllers[i].Clear(displayClearMode);
			}

			switch (displayClearMode)
			{
				case DisplayClearMode.Partial:
					break;
				case DisplayClearMode.Full:
					break;
				case DisplayClearMode.Pooling:

					this._inventoryItemDisplayControllers = null;

					break;
				case DisplayClearMode.Destruction:

					this._inventoryItemDisplayControllers = null;

					break;
			}
		}

#if UNITY_EDITOR
        protected virtual void OnDrawGizmos()
        {
            
        }
#endif
    }
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(InventoryDisplayController))]
    [CanEditMultipleObjects]
    public class InventoryDisplayControllerEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            InventoryDisplayController sInventoryDisplayController = target as InventoryDisplayController;
#pragma warning restore 0219
        }
    }
#endif
}