﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core
{
	[System.Serializable]
	[CreateAssetMenu(fileName = "Inventory Item Shared Data", menuName = "Inventory/Item Shared Data", order = 1)]
	public class InventoryItemSharedData : ScriptableObject
	{
		public int Id_;
		public bool Stackable_;
		public Sprite Sprite_;
		public string Description_;

		//public int Id_ { get; private set; }
		//public bool Stackable_ { get; private set; }
		//public Sprite Sprite_ { get; private set; }
		//public string Description_ { get; private set; }
	}

	[System.Serializable]
	public class InventoryItem
    {
		public InventoryItemSharedData InventoryItemSharedData_ { get; private set; }

		public event UnityAction OnQuantityChange;

		private int _quantity;
		public int _Quantity
		{
			get { return this._quantity; }
			set
			{
				this._quantity = value;
				if (this.OnQuantityChange != null)
					this.OnQuantityChange.Invoke();
			}
		}

		public InventoryItem(int id, int quantity)
		{
			this.InventoryItemSharedData_ = InventoryItemDatabaseController.Instance_.FetchSharedData(id);
			this._Quantity = quantity;
		}
	}
}