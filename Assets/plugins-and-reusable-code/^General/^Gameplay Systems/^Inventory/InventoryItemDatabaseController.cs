﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    public class InventoryItemDatabaseController : MonoBehaviourSingleton<InventoryItemDatabaseController>
    {
		private Dictionary<int, InventoryItemSharedData> _itemId_inventoryItemSharedDataRelation;

		[SerializeField] private InventoryItemSharedData[] _inventoryItemSharedData;

		public InventoryItemSharedData FetchSharedData(int itemId)
		{
			return this._itemId_inventoryItemSharedDataRelation[itemId];
		}

		private void InitializeDatabase()
		{
			this._itemId_inventoryItemSharedDataRelation = new Dictionary<int, InventoryItemSharedData>(this._inventoryItemSharedData.Length);

			for (int i = 0; i < this._inventoryItemSharedData.Length; i++)
			{
				this._itemId_inventoryItemSharedDataRelation.Add(this._inventoryItemSharedData[i].Id_, this._inventoryItemSharedData[i]);
			}
		}

		protected override void Awake()
		{
			base.Awake();

			this.InitializeDatabase();
		}

#if UNITY_EDITOR
		protected virtual void OnDrawGizmos()
        {
            
        }
#endif
    }
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(InventoryItemDatabaseController))]
    [CanEditMultipleObjects]
    public class InventoryItemDatabaseControllerEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            InventoryItemDatabaseController sInventoryItemDatabaseController = target as InventoryItemDatabaseController;
#pragma warning restore 0219
        }
    }
#endif
}