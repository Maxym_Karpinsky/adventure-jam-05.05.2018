﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core
{
	public abstract class ObjectiveTask : Task
	{
		public override bool TryToSatisfy()
		{
			if (this.IsSatisfied())
			{
				if ((this._ParentTask as Objective)._ObjectiveState == Objective.ObjectiveState.Active)
					this.TryToSatisfyParentTask();
				return true;
			}

			return false;
		}

		protected ObjectiveTask(int id, string name, string description, Task parentTask) : base(id, name, description, parentTask)
		{
		}
	}
}