﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core
{
    public interface ICondition
    {
		bool IsSatisfied();
		bool TryToSatisfy();
	}
}