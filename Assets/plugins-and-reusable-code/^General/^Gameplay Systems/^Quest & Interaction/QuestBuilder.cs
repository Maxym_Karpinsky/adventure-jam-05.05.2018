﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core
{
	[System.Serializable]
    public class QuestBuilder 
    {
		[SerializeField] private QuestData[] _questData;
		private Dictionary<int, Quest> _questId_questRelation;

		[SerializeField] private Quest _activeQuest;
		public Quest _ActiveQuest { get { return this._activeQuest; } }

		public Quest FetchQuest(int questId)
		{
			return this._questId_questRelation[questId];
		}

		public bool ActivateQuest(int questId)
		{
			if (this._activeQuest.IsSatisfied())
			{
				this._activeQuest = this.FetchQuest(questId);

				return true;
			}

			return false;
		}

		public void Initialize()
		{
			this._questId_questRelation = new Dictionary<int, Quest>(this._questData.Length);

			this._questId_questRelation.Add(this._activeQuest._Id, this._activeQuest);
			this._activeQuest.Initialize();

			/*
			for (int i = 0; i < this._questData.Length; i++)
			{
				Quest quest = new Quest(
					this._questData[i]._Id, 
					this._questData[i]._Name, 
					this._questData[i]._Description
				);

				this._questId_questRelation.Add(
					this._questData[i]._Id,
					quest
				);

				Objective[] questObjectives = new Objective[this._questData[i]._ObjectiveData.Length];

				for (int j = 0; j < questObjectives.Length; j++)
				{
					questObjectives[j] = new Objective(
						this._questData[i]._ObjectiveData[j]._Id,
						this._questData[i]._ObjectiveData[j]._Name,
						this._questData[i]._ObjectiveData[j]._Description,
						quest
					);

					ObjectiveTask[] objectiveTasks = new ObjectiveTask[this._questData[i]._ObjectiveData[j]._TaskData.Length];

					for (int k = 0; k < this._questData[i]._ObjectiveData[j]._TaskData.Length; k++)
					{
						switch (this._questData[i]._ObjectiveData[j]._TaskData[k]._TaskType)
						{
							case TaskData.TaskType.Talk:

								objectiveTasks[k] = new TalkObjectiveTask(
									this._questData[i]._ObjectiveData[j]._TaskData[k]._Id,
									this._questData[i]._ObjectiveData[j]._TaskData[k]._Name,
									this._questData[i]._ObjectiveData[j]._TaskData[k]._Description,
									questObjectives[j]
								);

								break;
							case TaskData.TaskType.Item:
								break;
							case TaskData.TaskType.Kill:
								break;
						}
					}

					questObjectives[j].Initialize(objectiveTasks);
				}

				quest.Initialize(questObjectives);
			}
			*/
		}

		public QuestBuilder(string dataPath)
		{
			// TODO: Implement it with json

			this.Initialize();
		}
    }
}