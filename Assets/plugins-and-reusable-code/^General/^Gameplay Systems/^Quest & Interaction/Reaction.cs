﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core
{
	[System.Serializable]
    public class Reaction
    {
		[SerializeField] private Objective _objective;
		public Objective _Objective { get { return this._objective; } }

		[SerializeField] private ReactionComponent[] _reactionComponents;
		private Dictionary<int, UnityEvent> _objectiveState_reactionComponentRelation;

		public void Initialize()
		{
			this._objectiveState_reactionComponentRelation = new Dictionary<int, UnityEvent>(this._reactionComponents.Length);
			for (int i = 0; i < this._reactionComponents.Length; i++)
			{
				this._objectiveState_reactionComponentRelation.Add((int)this._reactionComponents[i]._ExecutionState, this._reactionComponents[i]._ReactionEvent);
			}
		}

		public bool React()
		{
			UnityEvent reactionEvent;
			if (this._objectiveState_reactionComponentRelation.TryGetValue((int)this._objective._ObjectiveState, out reactionEvent))
			{
				reactionEvent.Invoke();
				return true;
			}

			return false;
		}
    }

	[System.Serializable]
	public struct ReactionComponent
	{
		[SerializeField] private Objective.ObjectiveState _executionState;
		public Objective.ObjectiveState _ExecutionState { get { return this._executionState; } }

		[SerializeField] private UnityEvent _reactionEvent;
		public UnityEvent _ReactionEvent { get { return this._reactionEvent; } }
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
#endif
}