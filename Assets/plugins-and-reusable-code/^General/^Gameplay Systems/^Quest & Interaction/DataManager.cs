﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    public class DataManager : MonoBehaviourSingleton<DataManager> 
    {
		[Header("Resources")]
		[SerializeField] private string _questsDataPath;
		[SerializeField] private QuestBuilder _questBuilder;
		public QuestBuilder _QuestBuilder { get { return this._questBuilder; } }

		protected override void Awake()
		{
			base.Awake();

			this._questBuilder.Initialize();
			// this._questBuilder = new QuestBuilder(this._questsDataPath);
		}

#if UNITY_EDITOR
		protected virtual void OnDrawGizmos()
        {
            
        }
#endif
    }
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(DataManager))]
    [CanEditMultipleObjects]
    public class DataManagerEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            DataManager sDataManager = target as DataManager;
#pragma warning restore 0219
        }
    }
#endif
}