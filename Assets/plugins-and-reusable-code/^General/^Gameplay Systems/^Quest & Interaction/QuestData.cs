﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core
{
	[System.Serializable]
	[CreateAssetMenu(fileName = "Quest Data", menuName = "Quest/Data/Quest Data", order = 1)]
	public class QuestData : ScriptableObject 
    {
		[SerializeField] private int _id;
		public int _Id { get { return this._id; } }

		[SerializeField] private string _name;
		public string _Name { get { return this._name; } }

		[SerializeField] private string _description;
		public string _Description { get { return this._description; } }

		[SerializeField] private ObjectiveData[] _objectiveData;
		public ObjectiveData[] _ObjectiveData { get { return this._objectiveData; } }
	}

	[System.Serializable]
	[CreateAssetMenu(fileName = "Objective Data", menuName = "Quest/Data/Objective Data", order = 1)]
	public class ObjectiveData : ScriptableObject
	{
		[SerializeField] private TaskData[] _taskData;
		public TaskData[] _TaskData { get { return this._taskData; } }

		[SerializeField] private int _id;
		public int _Id { get { return this._id; } }

		[SerializeField] private string _name;
		public string _Name { get { return this._name; } }

		[SerializeField] private string _description;
		public string _Description { get { return this._description; } }
	}

	[System.Serializable]
	[CreateAssetMenu(fileName = "Task Data", menuName = "Quest/Data/Task Data", order = 1)]
	public class TaskData : ScriptableObject
	{
		public enum TaskType { Talk, Item, Kill }

		[SerializeField] private int _id;
		public int _Id { get { return this._id; } }

		[SerializeField] private string _name;
		public string _Name { get { return this._name; } }

		[SerializeField] private string _description;
		public string _Description { get { return this._description; } }

		[SerializeField] private TaskType _taskType;
		public TaskType _TaskType { get { return this._taskType; } }
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(QuestData))]
    [CanEditMultipleObjects]
    public class QuestDataEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            QuestData sQuestData = target as QuestData;
#pragma warning restore 0219
        }
    }
#endif
}