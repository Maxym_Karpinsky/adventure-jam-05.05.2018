﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core
{
    public abstract class Executor<TTask> : MonoBehaviour 
		where TTask : ObjectiveTask
    {
		[SerializeField] protected TTask task;

		public abstract void Execute();
    }
}