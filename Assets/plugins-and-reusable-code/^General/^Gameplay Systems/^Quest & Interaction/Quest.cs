﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core
{
	[System.Serializable]
	[CreateAssetMenu(fileName = "Quest", menuName = "Quest/Quest", order = 1)]
	public sealed class Quest : Task 
    {
		[SerializeField] private Objective[] _objectives;
		private Dictionary<int, Objective> _objectiveId_objectiveRelation;

		public Objective FetchObjective(int objectiveId)
		{
			for (int i = 0; i < this._objectives.Length; i++)
			{
				if (this._objectives[i]._Id == objectiveId)
				{
					return this._objectives[i];
				}
			}

			return null;

			// return this._objectiveId_objectiveRelation[objectiveId];
		}

		[SerializeField] private bool _isSatisfied = false;
		public override bool IsSatisfied()
		{
			return this._isSatisfied;
		}

		public override bool TryToSatisfy()
		{
			if (this._isSatisfied)
				return true;

			for (int i = 0; i < this._objectives.Length; i++)
			{
				if (!this._objectives[i].IsSatisfied())
				{
					return false;
				}
			}

			this._isSatisfied = true;

			return true;
		}

		public Quest(int id, string name, string description) : base(id, name, description, new DummyTask())
		{
		}

		public void Initialize(Objective[] objectives)
		{
			this._objectives = objectives;
			this._objectiveId_objectiveRelation = new Dictionary<int, Objective>(this._objectives.Length);

			for (int i = 0; i < this._objectives.Length; i++)
			{
				this._objectiveId_objectiveRelation.Add(this._objectives[i]._Id, this._objectives[i]);
			}
		}

		// TODO: DELETE
		public void Initialize()
		{
			this._objectiveId_objectiveRelation = new Dictionary<int, Objective>(this._objectives.Length);

			for (int i = 0; i < this._objectives.Length; i++)
			{
				this._objectiveId_objectiveRelation.Add(this._objectives[i]._Id, this._objectives[i]);
			}
		}
	}
}