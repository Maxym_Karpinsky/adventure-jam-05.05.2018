﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core
{
    public class TalkTaskExecutor : Executor<TalkObjectiveTask>
    {
		public override void Execute()
		{
			this.task.Satisfy();
		}

#if UNITY_EDITOR
		protected virtual void OnDrawGizmos()
        {
            
        }
#endif
    }
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(TalkTaskExecutor))]
    [CanEditMultipleObjects]
    public class TalkTaskExecutorEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            TalkTaskExecutor sTalkTaskExecutor = target as TalkTaskExecutor;
#pragma warning restore 0219
        }
    }
#endif
}