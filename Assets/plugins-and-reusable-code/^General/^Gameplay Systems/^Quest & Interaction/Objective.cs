﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core
{
	[System.Serializable]
	[CreateAssetMenu(fileName = "Objective", menuName = "Quest/Objective", order = 1)]
	public sealed class Objective : Task
    {
		public enum ObjectiveState
		{
			Inactive,
			Active,
			Satisfied
		}

		[SerializeField] private ObjectiveState _objectiveState = ObjectiveState.Inactive;
		public ObjectiveState _ObjectiveState { get { return this._objectiveState; } }

		public void Activate()
		{
			if (this._objectiveState != ObjectiveState.Satisfied)
				this._objectiveState = ObjectiveState.Active;
			else
				Debug.LogWarning("Trying to activate already satisfied objective");
		}

		public override bool IsSatisfied()
		{
			return this._objectiveState == ObjectiveState.Satisfied;
		}

		[SerializeField] private ObjectiveTask[] _tasks;

		public override bool TryToSatisfy()
		{
			if (this._objectiveState == ObjectiveState.Active)
			{
				for (int i = 0; i < this._tasks.Length; i++)
				{
					if (!this._tasks[i].IsSatisfied())
						return false;
				}
			}

			this._objectiveState = ObjectiveState.Satisfied;

			this.TryToSatisfyParentTask();

			return true;
		}

		public Objective(int id, string name, string description, Task parentTask) : base(id, name, description, parentTask)
		{
		}

		public void Initialize(ObjectiveTask[] tasks)
		{
			this._tasks = tasks;
		}
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
#endif
}