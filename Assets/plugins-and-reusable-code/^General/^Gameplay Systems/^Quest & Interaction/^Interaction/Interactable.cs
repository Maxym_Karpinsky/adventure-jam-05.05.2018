﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core
{
    public class Interactable : MonoBehaviour 
    {
		[SerializeField] private Relation[] _quest_reactionRelationsData;
		private Dictionary<int, Reaction> _questId_reactionRelation;

		[SerializeField] private UnityEvent _defaultReactionEvent;
		public UnityEvent _DefaultReactionEvent { get { return this._defaultReactionEvent; } }

		public void Interact(BaseEventData baseEventData)
		{
			this.Interact();
		}

		public void Interact()
		{
			if (!this._questId_reactionRelation[DataManager.Instance_._QuestBuilder._ActiveQuest._Id].React())
				this._defaultReactionEvent.Invoke();
		}

		public void InteractAll(BaseEventData baseEventData)
		{
			this.InteractAll();
		}

		public void InteractAll()
		{
			for (int i = 0; i < this._quest_reactionRelationsData.Length; i++)
			{
				this._quest_reactionRelationsData[i]._Reaction.React();
			}
		}

		protected virtual void Awake()
		{
			this._questId_reactionRelation = new Dictionary<int, Reaction>(this._quest_reactionRelationsData.Length);
			for (int i = 0; i < this._quest_reactionRelationsData.Length; i++)
			{
				this._quest_reactionRelationsData[i]._Reaction.Initialize();
				this._questId_reactionRelation.Add(this._quest_reactionRelationsData[i]._Quest._Id, this._quest_reactionRelationsData[i]._Reaction);
			}
		}

#if UNITY_EDITOR
        protected virtual void OnDrawGizmos()
        {
            
        }
#endif
    }

	[System.Serializable]
	public struct Relation
	{
		[SerializeField] private Quest _quest;
		public Quest _Quest { get { return this._quest; } }

		[SerializeField] private Reaction _reaction;
		public Reaction _Reaction { get { return this._reaction; } }
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(Interactable))]
    [CanEditMultipleObjects]
    public class InteractableEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            Interactable sInteractable = target as Interactable;
#pragma warning restore 0219
        }
    }
#endif
}