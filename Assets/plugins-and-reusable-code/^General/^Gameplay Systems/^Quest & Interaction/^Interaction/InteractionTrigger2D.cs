﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
	[RequireComponent(typeof(Collider))]
	public class InteractionTrigger2D : MonoBehaviour
	{
		[SerializeField] private LayeredEventTriggerData[] _onTriggerEnterData;
		[SerializeField] private LayeredEventTriggerData[] _onTriggerStayData;
		[SerializeField] private LayeredEventTriggerData[] _onTriggerExitData;

#if UNITY_EDITOR
		protected virtual void Reset()
		{
			foreach (Collider2D collider in this.GetComponents<Collider2D>())
			{
				if (collider.isTrigger)
				{
					return;
				}
			}

			BoxCollider2D defaultCollider = this.gameObject.AddComponent<BoxCollider2D>();
			defaultCollider.isTrigger = true;
		}
#endif

		private void OnTriggerEnter2D(Collider2D other)
		{
			for (int i = 0; i < this._onTriggerEnterData.Length; i++)
			{
				if (((1 << other.gameObject.layer) & this._onTriggerEnterData[i]._LayerMask) != 0)
					this._onTriggerEnterData[i]._UnityEvent.Invoke();
			}
		}

		private void OnTriggerStay2D(Collider2D other)
		{
			for (int i = 0; i < this._onTriggerStayData.Length; i++)
			{
				if (((1 << other.gameObject.layer) & this._onTriggerEnterData[i]._LayerMask) != 0)
					this._onTriggerStayData[i]._UnityEvent.Invoke();
			}
		}

		private void OnTriggerExit2D(Collider2D other)
		{
			for (int i = 0; i < this._onTriggerExitData.Length; i++)
			{
				if (((1 << other.gameObject.layer) & this._onTriggerEnterData[i]._LayerMask) != 0)
					this._onTriggerExitData[i]._UnityEvent.Invoke();
			}
		}

#if UNITY_EDITOR
		protected virtual void OnDrawGizmos()
		{

		}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(InteractionTrigger2D))]
    [CanEditMultipleObjects]
    public class InteractionTrigger2DEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            InteractionTrigger2D sInteractionTrigger2D = target as InteractionTrigger2D;
#pragma warning restore 0219

			foreach (Collider2D collider in sInteractionTrigger2D.GetComponents<Collider2D>())
			{
				if (collider.isTrigger)
				{
					return;
				}
			}

			EditorGUILayout.HelpBox("There is no trigger collider attached. Events might not trigger", MessageType.Warning); //TODO: Search for colliders recursively
		}
    }
#endif
}