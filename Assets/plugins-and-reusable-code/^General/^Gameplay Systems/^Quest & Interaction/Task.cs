﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core
{
	[System.Serializable]
    public abstract class Task : ScriptableObject, ICondition
    {
		[SerializeField] private int _id;
		public int _Id { get { return this._id; } }

		[SerializeField] private string _name;
		public string _Name { get { return this._name; } }

		[SerializeField] private string _description;
		public string _Description { get { return this._description; } }

		[SerializeField] private Task _parentTask;
		public Task _ParentTask { get { return this._parentTask; } }

		public void TryToSatisfyParentTask()
		{
			this._parentTask.TryToSatisfy();
		}

		protected Task(int id, string name, string description, Task parentTask)
		{
			this._id = id;
			this._name = name;
			this._description = description;
			this._parentTask = parentTask;
		}

		public abstract bool TryToSatisfy();
		public abstract bool IsSatisfied();
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
#endif
}