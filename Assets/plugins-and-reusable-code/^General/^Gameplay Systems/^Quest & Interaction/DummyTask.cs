﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core
{
	[System.Serializable]
	[CreateAssetMenu(fileName = "Dummy Task", menuName = "Quest/Dummy Task", order = 1)]
	public class DummyTask : Task
	{
		public DummyTask(int id = 1, string name = "Dummy", string description = "Dummy", Task parentTask = null) : base(id, name, description, parentTask)
		{
		}

		public override bool IsSatisfied()
		{
			return false;
		}

		public override bool TryToSatisfy()
		{
			return false;
		}
	}
}