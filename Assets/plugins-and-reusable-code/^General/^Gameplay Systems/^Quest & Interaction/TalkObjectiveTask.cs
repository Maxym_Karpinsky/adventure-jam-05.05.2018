﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core
{
	[System.Serializable]
	[CreateAssetMenu(fileName = "Talk Task", menuName = "Quest/Talk Task", order = 1)]
	public class TalkObjectiveTask : ObjectiveTask
	{
		[SerializeField] private bool _satisfied = false;

		public override bool IsSatisfied()
		{
			return this._satisfied;
		}

		public void Satisfy()
		{
			this._satisfied = true;

			this.TryToSatisfy();
		}

		public TalkObjectiveTask(int id, string name, string description, Task parentTask) : base(id, name, description, parentTask)
		{
		}
	}
}