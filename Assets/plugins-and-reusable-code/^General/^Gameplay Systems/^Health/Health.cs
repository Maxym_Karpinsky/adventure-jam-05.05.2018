﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
	[RequireComponent(typeof(LivingEntity))]
    public class Health : MonoBehaviour 
    {
		[SerializeField] private HealthChangeUnityEvent _OnValueChanged;

		[SerializeField] private MinMaxFloat _valueBounds = new MinMaxFloat(0f, 100f);

		private float _value;

		private LivingEntity _livingEntity;

		public void Add(float value)
		{
			if (!this._livingEntity._Alive)
				return;

			this._value += value;
			if (this._value < this._valueBounds.Max)
				this._value = this._valueBounds.Max;

			this._OnValueChanged.Invoke(this._value, this._valueBounds.Max);
		}

		public void Reduce(float value)
		{
			if (!this._livingEntity._Alive)
				return;

			this._value -= value;
			if (this._value <= this._valueBounds.Min)
			{
				this._value = this._valueBounds.Min;

				this._livingEntity.Destroy();
			}

			this._OnValueChanged.Invoke(this._value, this._valueBounds.Max);
		}

		public void ResetHealth()
		{
			if (!this._livingEntity._Alive)
				return;

			this._value = this._valueBounds.Max;

			this._OnValueChanged.Invoke(this._value, this._valueBounds.Max);
		}

		private void Awake()
		{
			this._livingEntity = this.GetComponent<LivingEntity>();
		}

		private void Start()
		{
			this.ResetHealth();
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{

		//}
#endif

		[System.Serializable]
		public class HealthChangeUnityEvent : UnityEvent<float, float> { }
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(Health))]
    [CanEditMultipleObjects]
    public class HealthEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            Health sHealth = target as Health;
#pragma warning restore 0219
        }
    }
#endif
}