﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    public class Entity : MonoBehaviour 
    {
		public virtual void Destroy()
		{
			Destroy(this.gameObject);
		}

#if UNITY_EDITOR
        //protected override void OnDrawGizmos()
        //{
            
        //}
#endif
    }
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(Entity))]
    [CanEditMultipleObjects]
    public class EntityEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            Entity sEntity = target as Entity;
#pragma warning restore 0219
        }
    }
#endif
}