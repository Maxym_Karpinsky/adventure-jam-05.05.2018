﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    public class LivingEntity : Entity
    {
		protected bool alive = true;
		public bool _Alive { get { return this.alive; } }

		public override void Destroy()
		{
			this.alive = false;

			base.Destroy();
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{

		//}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(LivingEntity))]
    [CanEditMultipleObjects]
    public class LivingEntityEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            LivingEntity sLivingEntity = target as LivingEntity;
#pragma warning restore 0219
        }
    }
#endif
}