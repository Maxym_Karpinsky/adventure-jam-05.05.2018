﻿/* Created by Max.K.Kimo */

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using System;
using System.Reflection;

namespace Kimo.Assistance
{
#if UNITY_EDITOR
	public static class SerializedPropertyExtensions
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="serializedProperty"></param>
		/// <returns>Enum name value</returns>
		public static string GetEnumName(this SerializedProperty serializedProperty)
		{
			return serializedProperty.enumNames[serializedProperty.enumValueIndex];
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="serializedProperty"></param>
		/// <returns>Enum capitalized and formatted name string value</returns>
		public static string GetEnumDisplayName(this SerializedProperty serializedProperty)
		{
			return serializedProperty.enumDisplayNames[serializedProperty.enumValueIndex];
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TEnum"></typeparam>
		/// <param name="serializedProperty"></param>
		/// <returns>Enum value</returns>
		public static TEnum GetEnumValue<TEnum>(this SerializedProperty serializedProperty)
			where TEnum : struct, IComparable, IFormattable, IConvertible
		{
			return serializedProperty.enumNames[serializedProperty.enumValueIndex].ToEnum<TEnum>(false);
		}

		public static void AddToObjectArray(this SerializedProperty arrayProperty, UnityEngine.Object item)
		{
			if (!arrayProperty.isArray)
				throw new ArgumentException("SerializedProperty " + arrayProperty.name + " is not an array.");

			arrayProperty.serializedObject.Update();

			// Add a null array item to the end of the array then populate it with the object parameter
			arrayProperty.InsertArrayElementAtIndex(arrayProperty.arraySize);
			arrayProperty.GetArrayElementAtIndex(arrayProperty.arraySize - 1).objectReferenceValue = item;

			arrayProperty.serializedObject.ApplyModifiedProperties();
		}

		public static void RemoveFromObjectArrayAt(this SerializedProperty arrayProperty, int index)
		{
			if (index < 0)
				throw new IndexOutOfRangeException("Index out of bounds");

			if (!arrayProperty.isArray)
				throw new ArgumentException("SerializedProperty " + arrayProperty.name + " is not an array");

			if (index > arrayProperty.arraySize - 1)
				throw new IndexOutOfRangeException("Index out of bounds");

			arrayProperty.serializedObject.Update();

			// If item at the index is non-null - ``delete it`` - make it null
			if (arrayProperty.GetArrayElementAtIndex(index).objectReferenceValue)
				arrayProperty.DeleteArrayElementAtIndex(index);

			// Delete a null array item at the index
			arrayProperty.DeleteArrayElementAtIndex(index);

			arrayProperty.serializedObject.ApplyModifiedProperties();
		}

		public static bool RemoveFromObjectArray(this SerializedProperty arrayProperty, UnityEngine.Object item)
		{
			if (!arrayProperty.isArray)
				throw new ArgumentException("SerializedProperty " + arrayProperty.name + " is not an array");

			if (item == null)
				throw new NullReferenceException("`item` cannot be null");

			arrayProperty.serializedObject.Update();

			for (int i = 0; i < arrayProperty.arraySize; i++)
			{
				SerializedProperty elementProperty = arrayProperty.GetArrayElementAtIndex(i);

				if (elementProperty.objectReferenceValue == item)
				{
					arrayProperty.RemoveFromObjectArrayAt(i);
					return true;
				}
			}

			return false;
		}

		public static object GetValue(this SerializedProperty property)
		{
			Type containingObjectType = property.serializedObject.targetObject.GetType();

			FieldInfo fieldInfo = containingObjectType.GetFieldInfoViaPath(property.propertyPath);
			return fieldInfo.GetValue(property.serializedObject.targetObject);
		}

		public static void SetValue(this SerializedProperty property, object value)
		{
			Type containingObjectType = property.serializedObject.targetObject.GetType();

			FieldInfo fieldInfo = containingObjectType.GetFieldInfoViaPath(property.propertyPath);
			fieldInfo.SetValue(property.serializedObject.targetObject, value);
		}

		public static FieldInfo GetContainingObjectFieldInfo(this SerializedProperty property)
		{
			Type containingObjectType = property.serializedObject.targetObject.GetType();

			FieldInfo fieldInfo = containingObjectType.GetFieldInfoViaPath(property.propertyPath);
			return fieldInfo;
		}

		public static FieldInfo GetParentObjectFieldInfo(this SerializedProperty property)
		{
			Type containingObjectType = property.serializedObject.targetObject.GetType();
			
			FieldInfo fieldInfo = containingObjectType.GetParentObjectFieldInfoViaPath(property.propertyPath);
			return fieldInfo;
		}

		public static Type GetContainingObjectFieldType(this SerializedProperty property)
		{
			Type containingObjectType = property.serializedObject.targetObject.GetType();

			FieldInfo fieldInfo = containingObjectType.GetFieldInfoViaPath(property.propertyPath);
			return fieldInfo.FieldType;
		}

		public static Type GetParentObjectFieldType(this SerializedProperty property)
		{
			Type containingObjectType = property.serializedObject.targetObject.GetType();

			FieldInfo fieldInfo = containingObjectType.GetParentObjectFieldInfoViaPath(property.propertyPath);
			return fieldInfo.FieldType;
		}

		public static Type GetParentObjectFieldType(this SerializedProperty property, string[] fieldNames)
		{
			Type containingObjectType = property.serializedObject.targetObject.GetType();

			FieldInfo fieldInfo = containingObjectType.GetParentObjectFieldInfoViaPath(fieldNames);
			return fieldInfo.FieldType;
		}
	}
#endif
}