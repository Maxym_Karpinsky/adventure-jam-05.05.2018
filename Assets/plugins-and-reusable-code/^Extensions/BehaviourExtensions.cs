﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
    public static class BehaviourExtensions
    {
		public static void Enable(this Behaviour behaviour)
		{
			behaviour.enabled = true;
		}

		public static void Disable(this Behaviour behaviour)
		{
			behaviour.enabled = false;
		}
    }
}