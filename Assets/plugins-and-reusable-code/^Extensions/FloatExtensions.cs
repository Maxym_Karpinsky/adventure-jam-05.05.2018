﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance.UTILITY;

namespace Kimo.Assistance
{
    public static class FloatExtensions
    {
		public static float Interpolate010From01(this float value) //TODO: think about naming and about removing that clamp
		{
			value = value.Clamp01();

			return 4f * (value - (value * value));
		}

		public static float Interpolate010From02(this float value) //TODO: think about naming and about removing that clamp
		{
			value = value.Clamp01();

			return 1f - (1f - value) * (1f - value);
		}

		public static float Ease(this float value, float magnitude)
		{
			value = value.Clamp01();
			magnitude += 1;

			float powered = Mathf.Pow(value, magnitude);

			return powered / (powered + Mathf.Pow(1f - value, magnitude));
		}

		public static float EaseOut(this float value)
		{
			return Mathf.Sin(value * MathUtility.PI_HALF);
		}

		public static float EaseIn(this float value)
		{
			return 1f - Mathf.Cos(value * MathUtility.PI_HALF);
		}

		public static float SmoothStep(this float value)
		{
			value = value.Clamp01();

			return value * value * (3f - 2f * value);
		}

		public static float SmootherStep(this float value)
		{
			value = value.Clamp01();

			return value * value * value * (value * (6f * value - 15f) + 10f);
		}

		public static float Quadratic(this float value)
		{
			return value * value;
		}

		public static float Qubic(this float value)
		{
			return value * value * value;
		}

		public static float Clamp01(this float value)
		{
			return Mathf.Clamp01(value);
		}

		/// <summary>
		/// Returns value transformed from range(0, valueMaximum) to range(-valueMaximum, valueMaximum)
		/// </summary>
		/// <param name="value"></param>
		/// <param name="valueMaximum"></param>
		/// <returns></returns>
		public static float ToNegativePositiveInterval(this float value, float valueMaximum)
		{
#if UNITY_EDITOR
			//if (value < 0)
			//	Debug.LogError("Value cannot be negative. Wrong use. Value should be between 0 and valueMaximum");

			// value = value.Clamp01();
#endif

			return value * 2f - valueMaximum;
		}

		public static string ToCurrencyString(this float value)
		{
			int suffixIndex = 0;

			while (value >= CurrencyUtility.VALUE_NAME_THRESHOLD_NUMBER_FLOAT)
			{
				value /= CurrencyUtility.VALUE_NAME_THRESHOLD_NUMBER_FLOAT;
				++suffixIndex;

				if (suffixIndex >= CurrencyUtility.Suffixes.Length)
					return string.Format("{0}{1}", value, CurrencyUtility.Suffixes[CurrencyUtility.Suffixes.Length - 1]);
			}

			return string.Format("{0}{1}", value, CurrencyUtility.Suffixes[suffixIndex]);
		}
	}
}