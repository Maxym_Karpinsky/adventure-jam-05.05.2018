﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
    public static class DoubleExtensions
    {
		public static string ToCurrencyString(this double value)
		{
			int suffixIndex = 0;

			while (value >= CurrencyUtility.VALUE_NAME_THRESHOLD_NUMBER_DOUBLE)
			{
				value /= CurrencyUtility.VALUE_NAME_THRESHOLD_NUMBER_DOUBLE;
				++suffixIndex;

				if (suffixIndex >= CurrencyUtility.Suffixes.Length)
					return string.Format("{0}{1}", value, CurrencyUtility.Suffixes[CurrencyUtility.Suffixes.Length - 1]);
			}

			return string.Format("{0}{1}", value, CurrencyUtility.Suffixes[suffixIndex]);
		}
	}
}