﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
    public static class ComponentExtensions
    {
		public static T GetComponentSafe<T>(this Component component)
			where T : Component
		{
			return component.GetComponent<T>() ?? component.gameObject.AddComponent<T>();
		}

		public static bool GetComponent<T>(this Component component, out T resultComponent)
			where T : Component
		{
			resultComponent = component.GetComponent<T>();

			return resultComponent != null;
		}

		public static bool HasComponent<T>(this Component component)
			where T : Component
		{
			return component.GetComponent<T>() != null;
		}

		public static void ActivateGameObject(this Component component)
		{
			component.gameObject.SetActive(true);
		}

		public static void ActivateGameObjects(this Component[] components)
		{
			for (int i = 0; i < components.Length; i++)
			{
				components[i].ActivateGameObject();
			}
		}

		public static void ActivateGameObjectsOfType<T>(this Component[] components)
			where T : Component
		{
			for (int i = 0; i < components.Length; i++)
			{
				Component component = components[i].GetComponent<T>();
				if (component != null)
					component.ActivateGameObject();
			}
		}

		public static void DeactivateGameObject(this Component component)
		{
			component.gameObject.SetActive(false);
		}

		public static void DeactivateGameObjects(this Component[] components)
		{
			for (int i = 0; i < components.Length; i++)
			{
				components[i].DeactivateGameObject();
			}
		}

		public static void DeactivateGameObjectsOfType<T>(this Component[] components)
			where T : Component
		{
			for (int i = 0; i < components.Length; i++)
			{
				Component component = components[i].GetComponent<T>();
				if (component != null)
					component.DeactivateGameObject();
			}
		}
	}
}