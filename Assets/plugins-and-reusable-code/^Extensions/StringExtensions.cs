﻿/* Created by Max.K.Kimo */

using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
	public static class StringExtensions
	{
		// TODO: You have colors code in another project - look it up

		public static string SetColor(this string value, Color color)
		{
			return string.Format("<color={0}>{1}</color>", ColorUtility.ToHtmlStringRGBA(color), value);
		}

		public static string SetColor(this string value, string color)
		{
			return string.Format("<color={0}>{1}</color>", color, value);
		}

		public static TEnum ToEnum<TEnum>(this string value, bool ignoreCase = true)
			where TEnum : struct, IComparable, IFormattable, IConvertible
		{
			return (TEnum)Enum.Parse(typeof(TEnum), value, ignoreCase);
		}

		public static TEnum ToEnum<TEnum>(this string value, TEnum defaultValue, bool ignoreCase = true)
			where TEnum : struct, IComparable, IFormattable, IConvertible
		{
			if (string.IsNullOrEmpty(value))
				return defaultValue;

			Array enumValues = Enum.GetValues(typeof(TEnum));

			if (ignoreCase)
			{
				value = value.ToLower();

				for (int i = 0; i < enumValues.Length; i++)
					if (enumValues.GetValue(i).ToString().ToLower().Equals(value))
						return (TEnum)enumValues.GetValue(i);
			}
			else
				for (int i = 0; i < enumValues.Length; i++)
					if (enumValues.GetValue(i).ToString().Equals(value))
						return (TEnum)enumValues.GetValue(i);

			return defaultValue;
		}
	}
}