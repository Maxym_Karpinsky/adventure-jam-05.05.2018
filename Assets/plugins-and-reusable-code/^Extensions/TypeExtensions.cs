﻿/* Created by Max.K.Kimo */

using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
	public static class TypeExtensions
	{


		public static FieldInfo GetFieldInfoViaPath(this Type type, string path)
		{
			FieldInfo fieldInfo = type.GetField(path);

			string[] fieldNames = path.Split('.');
			for (int i = 0; i < fieldNames.Length; i++)
			{
				fieldInfo = type.GetField(fieldNames[i], BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
				if (fieldInfo != null)
					type = fieldInfo.FieldType;
				else
					break;
			}

			return fieldInfo;
		}

		public static readonly string[] PATH_FIELD_NAMES_EXCEPTIONS_SUBSRTINGS = { "Array", "data[" };

		private static bool IsException(string fieldName)
		{
			for (int j = 0; j < TypeExtensions.PATH_FIELD_NAMES_EXCEPTIONS_SUBSRTINGS.Length; j++)
				if (fieldName.Substring(0, TypeExtensions.PATH_FIELD_NAMES_EXCEPTIONS_SUBSRTINGS[j].Length) == TypeExtensions.PATH_FIELD_NAMES_EXCEPTIONS_SUBSRTINGS[j])
					return true;

			return false;
		}

		/// <summary>
		/// Search to top of the hierarchy via fieldNames
		/// </summary>
		/// <param name="type"></param>
		/// <param name="fieldNames"></param>
		/// <returns>Parent object FieldInfo || null if has no parent</returns>
		public static FieldInfo GetParentObjectFieldInfoViaPath(this Type type, string[] fieldNames)
		{
			FieldInfo fieldInfo = null;

			for (int i = 0; i < fieldNames.Length - 1; i++)
			{
				//if (TypeExtensions.IsException(fieldNames[i]))
				//	continue;
				if (type.IsArray)
				{
					i += 2;
					if (i >= fieldNames.Length - 1)
						return null;

					type = type.GetElementType();
				}

				fieldInfo = type.GetField(fieldNames[i], BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
				if (fieldInfo != null)
					type = fieldInfo.FieldType;
				else
					break;
			}

			return fieldInfo;
		}

		public static FieldInfo GetParentObjectFieldInfoViaPath(this Type type, string path)
		{
			string[] fieldNames = path.Split('.');

			return type.GetParentObjectFieldInfoViaPath(fieldNames);
		}
	}
}