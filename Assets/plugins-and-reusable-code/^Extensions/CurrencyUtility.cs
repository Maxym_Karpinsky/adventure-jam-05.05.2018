﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
    public static class CurrencyUtility
    {
		public static string[] Suffixes = { "", "K", "M", "B", "T", "Qa", "Qi", "Sx", "Sp", "O", "N", "D", "U", "Aa", "AA", "AB", "BA", "BB", "CA", "CB", "DA", "DB", "EA", "EB", "FA", "FB" };

		public const int VALUE_NAME_THRESHOLD_NUMBER_INTEGER = 1000;
		public const float VALUE_NAME_THRESHOLD_NUMBER_FLOAT = 1000f;
		public const double VALUE_NAME_THRESHOLD_NUMBER_DOUBLE = 1000d;
	}
}