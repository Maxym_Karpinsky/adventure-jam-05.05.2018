﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
    public static class TransformExtensions 
    {
		public static void ResetPosition(this Transform transform)
		{
			transform.position = Vector3.zero;
		}

		public static void ResetLocalPosition(this Transform transform)
		{
			transform.localPosition = Vector3.zero;
		}

		public static void ResetRotation(this Transform transform)
		{
			transform.rotation = Quaternion.identity;
		}

		public static void ResetLocalRotation(this Transform transform)
		{
			transform.localRotation = Quaternion.identity;
		}

		public static void ResetLocalScale(this Transform transform)
		{
			transform.localScale = Vector3.zero;
		}

		public static void ActivateChildren(this Transform transform)
		{
			for (int i = 0; i < transform.childCount; i++)
			{
				transform.GetChild(i).gameObject.SetActive(true);
			}
		}

		public static void ActivateChildrenOfType<T>(this Transform transform)
			where T : Component
		{
			for (int i = 0; i < transform.childCount; i++)
			{
				Component component = transform.GetChild(i).GetComponent<T>();
				if (component != null)
					component.gameObject.SetActive(true);
			}
		}

		public static void DeactivateChildren(this Transform transform)
		{
			for (int i = 0; i < transform.childCount; i++)
			{
				transform.GetChild(i).gameObject.SetActive(false);
			}
		}

		public static void DeactivateChildrenOfType<T>(this Transform transform)
			where T : Component
		{
			for (int i = 0; i < transform.childCount; i++)
			{
				Component component = transform.GetChild(i).GetComponent<T>();
				if (component != null)
					component.gameObject.SetActive(false);
			}
		}

		public static void DestroyChildren(this Transform transform)
		{
			for (int i = transform.childCount - 1; i >= 0; --i)
			{
				GameObject.Destroy(transform.GetChild(i).gameObject);
			}
		}

		public static void DestroyChildrenOfType<T>(this Transform transform)
			where T : Component
		{
			for (int i = transform.childCount - 1; i >= 0; --i)
			{
				Component component = transform.GetChild(i).GetComponent<T>();
				if (component != null)
					GameObject.Destroy(component.gameObject);
			}
		}

		public static void DestroyChildrenWithTag(this Transform transform, string tag)
		{
			for (int i = transform.childCount - 1; i >= 0; --i)
			{
				GameObject childGameObject = transform.GetChild(i).gameObject;
				if (childGameObject.CompareTag(tag))
					GameObject.Destroy(childGameObject);
			}
		}

		public static void DestroyChildrenImmediate(this Transform transform)
		{
			for (int i = transform.childCount - 1; i >= 0; --i)
			{
				GameObject.DestroyImmediate(transform.GetChild(i).gameObject);
			}
		}

		public static void DestroyChildrenImmediateOfType<T>(this Transform transform)
			where T : Component
		{
			for (int i = transform.childCount - 1; i >= 0; --i)
			{
				Component component = transform.GetChild(i).GetComponent<T>();
				if (component != null)
					GameObject.DestroyImmediate(component.gameObject);
			}
		}

		public static void DestroyChildrenImmediateWithTag(this Transform transform, string tag)
		{
			for (int i = transform.childCount - 1; i >= 0; --i)
			{
				GameObject childGameObject = transform.GetChild(i).gameObject;
				if (childGameObject.CompareTag(tag))
					GameObject.DestroyImmediate(childGameObject);
			}
		}
	}
}