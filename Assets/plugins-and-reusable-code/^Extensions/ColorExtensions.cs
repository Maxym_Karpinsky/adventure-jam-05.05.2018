﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
	public static class ColorExtensions
	{
		public static Color NormalBlend(this Color color, Color overlayColor)
		{
			return color * (1 - overlayColor.a) * color.a + overlayColor * overlayColor.a;
		}
	}
}