﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core
{
    public static class ColliderExtensions
    {
		public static void Enable(this Collider collider)
		{
			collider.enabled = true;
		}

		public static void Disable(this Collider collider)
		{
			collider.enabled = false;
		}
	}
}