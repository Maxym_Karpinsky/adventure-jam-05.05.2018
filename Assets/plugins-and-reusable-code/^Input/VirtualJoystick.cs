﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

#if UNITY_EDITOR
using UnityEditor;
#endif


namespace Kimo.Core
{
    public class VirtualJoystick : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
    {
        private Image _backgorundImage;
        [SerializeField] private Image _joystickBubble;
        private Vector3 _inputVector;
        public Vector3 InputVector { get { return this._inputVector; } }

        private static VirtualJoystick _instance;
        public static VirtualJoystick Instance { get { return _instance; } }
        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
            else if (_instance != this)
            {
                Destroy(this.gameObject);
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            Vector2 position;

            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(this._backgorundImage.rectTransform,
                eventData.position,
                eventData.pressEventCamera,
                out position))
            {
                position.x /= this._backgorundImage.rectTransform.sizeDelta.x;
                position.y /= this._backgorundImage.rectTransform.sizeDelta.y;

                this._inputVector = new Vector3(position.x * 2 + 1, position.y * 2 - 1, 0);
                this._inputVector = this._inputVector.sqrMagnitude >= 1f ? this._inputVector.normalized : this._inputVector;

                this._joystickBubble.rectTransform.anchoredPosition = new Vector3(this._inputVector.x * this._backgorundImage.rectTransform.sizeDelta.x / 2, this._inputVector.y * this._backgorundImage.rectTransform.sizeDelta.y / 2);
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            this.OnDrag(eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            this._inputVector = Vector3.zero;
            this._joystickBubble.rectTransform.anchoredPosition = Vector3.zero;
        }

        private void Start()
        {
            this._backgorundImage = this.GetComponent<Image>();
        }
    }
}