/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
    public static class InputUtility
    {
        /// <summary>
        /// Get result of check whether mouse button is still being pressed.
        /// Ignores times when mouse button is being pressed over UI elements.
        /// </summary>
        /// <param name="index">Index of mouse button</param>
        /// <returns>Check result of mouse button pressing. Ignoring UI elements.</returns>
        public static bool GetMouseButton(int index)
        {
            return Input.GetMouseButton(index) && !EventSystem.current.IsPointerOverGameObject();
        }

        /// <summary>
        /// Get result of check whether mouse button was pressed.
        /// Ignores times when mouse button was pressed over UI elements.
        /// </summary>
        /// <param name="index">Index of mouse button</param>
        /// <returns>Check result of mouse button pressing. Ignoring UI elements.</returns>
        public static bool GetMouseButtonDown(int index)
        {
            return Input.GetMouseButtonDown(index) && !EventSystem.current.IsPointerOverGameObject();
        }

        /// <summary>
        /// Get result of check whether mouse button was released.
        /// Ignores times when mouse button was released over UI elements.
        /// NOTE: Don't use this method unless you are sure that you want this kind of behavior.
        /// NOTE: Second time this method would work if previous time mouse button was released over UI element - will be called next time you release this button after pressing it.
        /// </summary>
        /// <param name="index">Index of mouse button</param>
        /// <returns>Check result of mouse button release. Ignoring UI elements.</returns>
        public static bool GetMouseButtonUp(int index)
        {
            return Input.GetMouseButtonUp(index) && !EventSystem.current.IsPointerOverGameObject();
        }
    }
}