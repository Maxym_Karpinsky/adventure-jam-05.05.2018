﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
    public class ImmutableArrayAttribute : MultiSupportPropertyAttribute 
    {


#if UNITY_EDITOR
		public override void DrawInInspector(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUILayout.BeginVertical(GUI.skin.box);

			EditorGUILayout.PropertyField(property, true);

			EditorGUILayout.EndVertical();
		}
#endif
	}
}

namespace Kimo.Assistance.UTILITY
{
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(ImmutableArrayAttribute))]
    [CanEditMultipleObjects]
    public class ImmutableArrayAttributeDrawer : MultiSupportPropertyAttributeDrawer
    {

	}
#endif
}