﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
	public abstract class MultiSupportPropertyAttribute : PropertyAttribute
    {
#if UNITY_EDITOR
		public virtual float GetPropertyExtensionHeight() { return 0; }
		public virtual void BeginDrawInInspector(Rect position, SerializedProperty property, GUIContent label) { }
        public virtual void DrawInInspector(Rect position, SerializedProperty property, GUIContent label) { }
		public virtual void EndDrawInInspector(Rect position, SerializedProperty property, GUIContent label) { }
#endif
	}
}

namespace Kimo.Assistance.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(MultiSupportPropertyAttribute))]
    [CanEditMultipleObjects]
    public class MultiSupportPropertyAttributeDrawer : PropertyDrawer
    {
		private MultiSupportPropertyAttribute _attribute;

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			if (this._attribute == null)
				this._attribute = this.attribute as MultiSupportPropertyAttribute;

			return base.GetPropertyHeight(property, label) + this._attribute.GetPropertyExtensionHeight();
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
			//foreach (var attribute in fieldInfo.GetCustomAttributes(false))
			//{
			//	(attribute as MultiSupportPropertyAttribute).DrawInInspector(position, property, label);
			//}

			this._attribute.BeginDrawInInspector(position, property, label);
			this._attribute.DrawInInspector(position, property, label);
			this._attribute.EndDrawInInspector(position, property, label);
        }
    }
#endif
}