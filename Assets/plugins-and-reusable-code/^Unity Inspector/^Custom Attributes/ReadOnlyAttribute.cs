﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
    public class ReadOnlyAttribute : MultiSupportPropertyAttribute
    {


#if UNITY_EDITOR
		public override void BeginDrawInInspector(Rect position, SerializedProperty property, GUIContent label)
		{
			GUI.enabled = false;
		}

		public override void DrawInInspector(Rect position, SerializedProperty property, GUIContent label)
		{
			
		}

		public override void EndDrawInInspector(Rect position, SerializedProperty property, GUIContent label)
		{
			GUI.enabled = true;
		}
#endif
	}
}

namespace Kimo.Assistance.UTILITY
{
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
    [CanEditMultipleObjects]
    public class ReadOnlyAttributeDrawer : MultiSupportPropertyAttributeDrawer
    {
	}
#endif
}