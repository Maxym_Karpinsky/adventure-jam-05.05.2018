﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
    public class HideInInspectorIfAttribute : PropertyAttribute
    {
        public string ConditionFieldName;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conditionFieldName">Name of the condition field</param>
        public HideInInspectorIfAttribute(string conditionFieldName)
        {
            this.ConditionFieldName = conditionFieldName;
        }
    }
}

namespace Kimo.Assistance.UTILITY
{
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(HideInInspectorIfAttribute))]
    [CanEditMultipleObjects]
    public class HideInInspectorIfAttributeDrawer : HideInInspectorDrawerBase
    {
        protected override bool IsHidden(SerializedProperty property)
        {
            HideInInspectorIfAttribute attr = this.attribute as HideInInspectorIfAttribute;

            // Get path to that property and get path without its name(path to the property(object, field) where current property was defined) to get condition field path from the same class later
            string propertyDeclatationObjectPath = property.propertyPath.Substring(0, property.propertyPath.Length - property.name.Length);

            // Find that property by the path
            SerializedProperty attributeConditionField = property.serializedObject.FindProperty(propertyDeclatationObjectPath + attr.ConditionFieldName);

            // If the field was found return it's boolean value
            if (attributeConditionField != null && attributeConditionField.propertyType == SerializedPropertyType.Boolean)
                return !attributeConditionField.boolValue;

            Debug.LogWarning(attr.ConditionFieldName + " is invalid condition(not supported type) or not a condition at all!");
            return false;
        }
    }
#endif
}