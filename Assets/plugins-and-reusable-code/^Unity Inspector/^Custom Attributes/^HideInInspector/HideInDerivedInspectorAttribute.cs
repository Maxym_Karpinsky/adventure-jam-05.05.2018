﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using System;

namespace Kimo.Assistance
{
    public class HideInDerivedInspectorAttribute : MultiSupportPropertyAttribute
    {
#if UNITY_EDITOR
		public readonly Type _DerivedType;

        /// <summary>
        /// Hide from specific decended type
        /// </summary>
        /// <param name="derivedType"></param>
        public HideInDerivedInspectorAttribute(Type derivedType)
        {
            _DerivedType = derivedType;
        }

        /// <summary>
        /// Hide from all descended types
        /// </summary>
        public HideInDerivedInspectorAttribute()
        {
            _DerivedType = null;
        }

        public override void DrawInInspector(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.PropertyField(position, property, label, true);
        }
#endif
	}
}

namespace Kimo.Assistance.UTILITY
{
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(HideInDerivedInspectorAttribute))]
    [CanEditMultipleObjects]
    public class HideInDerivedInspectorDrawer : HideInInspectorDrawerBase
    {
        private bool? _cachedIsHidden;

        protected override bool IsHidden(SerializedProperty property)
        {
            if (!this._cachedIsHidden.HasValue)
            {
                HideInDerivedInspectorAttribute attr = this.attribute as HideInDerivedInspectorAttribute;

                string[] fieldNames = property.propertyPath.Split('.');

                // If it's not a nested field
                if (fieldNames.Length <= 1)
                {
                    if (attr._DerivedType != null)
                        this._cachedIsHidden = property.serializedObject.targetObject.GetType() == attr._DerivedType;
                    else
                        this._cachedIsHidden = property.serializedObject.targetObject.GetType() != fieldInfo.DeclaringType;
                }
                else
                {
                    if (attr._DerivedType != null)
                        this._cachedIsHidden = property.GetParentObjectFieldType(fieldNames) == attr._DerivedType;
                    else
                        this._cachedIsHidden = property.GetParentObjectFieldType(fieldNames) != fieldInfo.DeclaringType;
                }
            }

            return this._cachedIsHidden.Value;
        }
    }
#endif
}