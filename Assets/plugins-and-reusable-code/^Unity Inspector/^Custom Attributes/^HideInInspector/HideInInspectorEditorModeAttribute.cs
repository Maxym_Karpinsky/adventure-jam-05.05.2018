﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
    public class HideInInspectorEditorModeAttribute : PropertyAttribute 
    {

    }
}

namespace Kimo.Assistance.UTILITY
{
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(HideInInspectorEditorModeAttribute))]
    [CanEditMultipleObjects]
    public class HideInInspectorEditorModeAttributeDrawer : HideInInspectorDrawerBase
    {
        protected override bool IsHidden(SerializedProperty property)
        {
            return !Application.isPlaying;
        }
    }
#endif
}