﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
    public class HideInInspectorPlayModeAttribute : PropertyAttribute
    {
    }
}

namespace Kimo.Assistance.UTILITY
{
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(HideInInspectorPlayModeAttribute))]
    [CanEditMultipleObjects]
    public class HideInInspectorPlayModeAttributeDrawer : HideInInspectorDrawerBase
    {
        protected override bool IsHidden(SerializedProperty property)
        {
            return Application.isPlaying;
        }
    }
#endif
}