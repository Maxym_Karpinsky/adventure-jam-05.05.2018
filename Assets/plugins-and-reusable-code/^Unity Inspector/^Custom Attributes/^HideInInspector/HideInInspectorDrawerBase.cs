﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance.UTILITY
{
#if UNITY_EDITOR
    public abstract class HideInInspectorDrawerBase : MultiSupportPropertyAttributeDrawer
    {
        protected abstract bool IsHidden(SerializedProperty property);

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return this.IsHidden(property) ? -EditorGUIUtility.standardVerticalSpacing : base.GetPropertyHeight(property, label);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!this.IsHidden(property))
                base.OnGUI(position, property, label);
        }
    }
#endif
}