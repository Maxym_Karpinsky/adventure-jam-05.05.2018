﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
    [System.Serializable]
    public struct MinMaxFloat
    {
        public float Min, Max;

        public MinMaxFloat(float min = 0f, float max = 100f)
        {
            this.Min = min;
            this.Max = max;
        }

        public float GetRandomValue()
        {
            return Random.Range(this.Min, this.Max);
        }
    }

    
    /// <summary>
    /// Attribute to add range functionality to MinMaxFloat
    /// </summary>
    public class MinMaxFloatRangeAttribute : MultiSupportPropertyAttribute
    {
		private const float MINIMAL_COMPACT_WIDTH = 325f;

		public float MinLimit, MaxLimit;

        public MinMaxFloatRangeAttribute(float minLimit = 0f, float maxLimit = 100f)
        {
            this.MinLimit = minLimit;
            this.MaxLimit = maxLimit;
        }

        private bool _compactFormat = true;

#if UNITY_EDITOR
		public override float GetPropertyExtensionHeight()
		{
			if (this._compactFormat)
				return InspectorUtility.DEFAULT_FIELD_HEIGHT;
			return 0;
		}

		public override void DrawInInspector(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            // property.type == "MinMaxFloat" @ works fine as well
            // This way of checking is safer
            if (property.type == typeof(MinMaxFloat).Name)
            {
                SerializedProperty minProperty = property.FindPropertyRelative("Min");
                SerializedProperty maxProperty = property.FindPropertyRelative("Max");

                float min = minProperty.floatValue;
                float max = maxProperty.floatValue;


                Rect minMaxSliderPositionRect;

                Rect minPositionRect;
                Rect maxPositionRect;

                this._compactFormat = Screen.width < MinMaxFloatRangeAttribute.MINIMAL_COMPACT_WIDTH;

                if (this._compactFormat)
                {
                    minMaxSliderPositionRect = new Rect(position.x, position.y, position.width, position.height);

                    minPositionRect = new Rect(minMaxSliderPositionRect.x + minMaxSliderPositionRect.width - InspectorUtility.DEFAULT_FIELD_WIDTH * 2 - InspectorUtility.ADDITIONAL_SPACE_HORIZONTAL_WIDTH, position.y + InspectorUtility.DEFAULT_FIELD_HEIGHT, InspectorUtility.DEFAULT_FIELD_WIDTH, InspectorUtility.DEFAULT_FIELD_HEIGHT);
                    maxPositionRect = new Rect(minMaxSliderPositionRect.x + minMaxSliderPositionRect.width - InspectorUtility.DEFAULT_FIELD_WIDTH, position.y + InspectorUtility.DEFAULT_FIELD_HEIGHT, InspectorUtility.DEFAULT_FIELD_WIDTH, InspectorUtility.DEFAULT_FIELD_HEIGHT);
                }
                else
                {
                    minMaxSliderPositionRect = new Rect(position.x, position.y, position.width - (InspectorUtility.DEFAULT_FIELD_WIDTH + InspectorUtility.ADDITIONAL_SPACE_HORIZONTAL_WIDTH) * 2, InspectorUtility.DEFAULT_FIELD_HEIGHT);

                    minPositionRect = new Rect(minMaxSliderPositionRect.x + minMaxSliderPositionRect.width + InspectorUtility.ADDITIONAL_SPACE_HORIZONTAL_WIDTH, position.y, InspectorUtility.DEFAULT_FIELD_WIDTH, InspectorUtility.DEFAULT_FIELD_HEIGHT);
                    maxPositionRect = new Rect(minMaxSliderPositionRect.x + minMaxSliderPositionRect.width + InspectorUtility.ADDITIONAL_SPACE_HORIZONTAL_WIDTH + InspectorUtility.DEFAULT_FIELD_WIDTH + InspectorUtility.ADDITIONAL_SPACE_HORIZONTAL_WIDTH, position.y, InspectorUtility.DEFAULT_FIELD_WIDTH, InspectorUtility.DEFAULT_FIELD_HEIGHT);
                }

                EditorGUI.BeginChangeCheck();

                EditorGUI.MinMaxSlider(minMaxSliderPositionRect, label, ref min, ref max, this.MinLimit, this.MaxLimit);

                int previousIndentLevel = EditorGUI.indentLevel;
                EditorGUI.indentLevel = 0;

                min = EditorGUI.FloatField(minPositionRect, min);
				if (min > max)
				{
					if (min > this.MaxLimit)
						min = this.MaxLimit;

					max = min;
				}

				max = EditorGUI.FloatField(maxPositionRect, max);
				if (max < min)
				{
					if (max < this.MinLimit)
						max = this.MinLimit;

					min = max;
				}

				if (min < this.MinLimit)
					min = this.MinLimit;
				if (max > this.MaxLimit)
					max = this.MaxLimit;

				if (EditorGUI.EndChangeCheck())
                {
                    minProperty.floatValue = min;
                    maxProperty.floatValue = max;
                }
				
                EditorGUI.indentLevel = previousIndentLevel;
            }
            else
            {
                EditorGUILayout.HelpBox("The type of the " + property.name + " should be MinMaxFloat in order for attribute to work correctly.", MessageType.Error);
            }

            EditorGUI.EndProperty();
        }
#endif
	}
}

namespace Kimo.Assistance.UTILITY
{
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(MinMaxFloat))]
    [CanEditMultipleObjects]
    public class MinMaxFloatDrawer : PropertyDrawer
    {
        private const float FLOAT_FIELD_WIDTH = 50f;
        private const float FLOAT_FIELD_HEIGHT = 16f;
        private const float SPACE_HORIZONTAL_WIDTH = 5f;
        private const float SPACE_VERTICAL_HEIGHT = 2f;
        private const float MINIMAL_COMPACT_WIDTH = 325f;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            SerializedProperty minProperty = property.FindPropertyRelative("Min");
            SerializedProperty maxProperty = property.FindPropertyRelative("Max");

            float min = minProperty.floatValue;
            float max = maxProperty.floatValue;

            position.y += (FLOAT_FIELD_HEIGHT + SPACE_VERTICAL_HEIGHT) / 2;
            Rect prefixLabelPosition = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            EditorGUIUtility.labelWidth = SPACE_HORIZONTAL_WIDTH * 6;

            Rect minPositionRect = new Rect(prefixLabelPosition.x, position.y - FLOAT_FIELD_HEIGHT / 2, prefixLabelPosition.width, FLOAT_FIELD_HEIGHT);
            Rect maxPositionRect = new Rect(prefixLabelPosition.x, position.y + FLOAT_FIELD_HEIGHT / 2 + SPACE_VERTICAL_HEIGHT, prefixLabelPosition.width, FLOAT_FIELD_HEIGHT);

            GUILayout.Space(FLOAT_FIELD_HEIGHT + SPACE_VERTICAL_HEIGHT);

            int previousIdentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            EditorGUI.BeginChangeCheck();

            min = EditorGUI.FloatField(minPositionRect, "Min", min);
            if (min > max)
                max = min;

            max = EditorGUI.FloatField(maxPositionRect, "Max", max);
            if (max < min)
                min = max;

            if (EditorGUI.EndChangeCheck())
            {
                minProperty.floatValue = min;
                maxProperty.floatValue = max;
            }

            EditorGUI.indentLevel = previousIdentLevel;

            EditorGUI.EndProperty();
        }
    }

    [CustomPropertyDrawer(typeof(MinMaxFloatRangeAttribute))]
    [CanEditMultipleObjects]
    public class MinMaxFloatRangeDrawer : MultiSupportPropertyAttributeDrawer
    {
        
    }
#endif
}