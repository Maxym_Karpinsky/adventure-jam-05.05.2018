﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Assistance
{
#if UNITY_EDITOR
	public static class InspectorUtility 
    {
		public const float DEFAULT_FIELD_WIDTH = 50f;
		public const float DEFAULT_FIELD_HEIGHT = 16f;
		public const float ADDITIONAL_SPACE_HORIZONTAL_WIDTH = 5f;
		public const float ADDITIONAL_SPACE_VERTICAL_HEIGHT = 2f;
	}
#endif
}