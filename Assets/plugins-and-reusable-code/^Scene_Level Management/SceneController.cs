﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    public class SceneController : MonoBehaviourSingleton<SceneController> 
    {
		#region LoadSceneAsync

		public delegate void AsyncInformationFetchAction(float progress);

		public IEnumerator LoadSceneAsyncProcess(int sceneBuildIndex, AsyncInformationFetchAction asyncInformationFetch)
		{
			AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneBuildIndex);

			while (!asyncOperation.isDone)
			{
				asyncInformationFetch.Invoke(asyncOperation.progress);

				yield return null;
			}
		}

		public IEnumerator LoadSceneAsyncProcess(int sceneBuildIndex, AsyncInformationFetchAction asyncInformationFetch, LoadSceneMode loadSceneMode)
		{
			AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneBuildIndex, loadSceneMode);

			while (!asyncOperation.isDone)
			{
				asyncInformationFetch.Invoke(asyncOperation.progress);

				yield return null;
			}
		}

		public IEnumerator LoadSceneAsyncProcess(string sceneName, AsyncInformationFetchAction asyncInformationFetch)
		{
			AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName);

			while (!asyncOperation.isDone)
			{
				asyncInformationFetch.Invoke(asyncOperation.progress);

				yield return null;
			}
		}

		public IEnumerator LoadSceneAsyncProcess(string sceneName, AsyncInformationFetchAction asyncInformationFetch, LoadSceneMode loadSceneMode)
		{
			AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName, loadSceneMode);

			while (!asyncOperation.isDone)
			{
				asyncInformationFetch.Invoke(asyncOperation.progress);

				yield return null;
			}
		}

		public void LoadSceneAsync(int sceneBuildIndex, AsyncInformationFetchAction asyncInformationFetch)
		{
			this.StartCoroutine(this.LoadSceneAsyncProcess(sceneBuildIndex, asyncInformationFetch));
		}

		public void LoadSceneAsync(int sceneBuildIndex, AsyncInformationFetchAction asyncInformationFetch, LoadSceneMode loadSceneMode)
		{
			this.StartCoroutine(this.LoadSceneAsyncProcess(sceneBuildIndex, asyncInformationFetch, loadSceneMode));
		}

		public void LoadSceneAsync(string sceneName, AsyncInformationFetchAction asyncInformationFetch)
		{
			this.StartCoroutine(this.LoadSceneAsyncProcess(sceneName, asyncInformationFetch));
		}

		public void LoadSceneAsync(string sceneName, AsyncInformationFetchAction asyncInformationFetch, LoadSceneMode loadSceneMode)
		{
			this.StartCoroutine(this.LoadSceneAsyncProcess(sceneName, asyncInformationFetch, loadSceneMode));
		}

		#endregion

		public static void LoadNextScene()
		{
			int activeSceneBuildIndex = SceneManager.GetActiveScene().buildIndex;

			if (activeSceneBuildIndex + 1 < SceneManager.sceneCountInBuildSettings)
				SceneManager.LoadScene(activeSceneBuildIndex + 1);
		}

		public static void LoadPreviousScene()
		{
			int activeSceneBuildIndex = SceneManager.GetActiveScene().buildIndex;

			if (activeSceneBuildIndex > 0)
				SceneManager.LoadScene(activeSceneBuildIndex - 1);
		}

		[SerializeField] private Text _someProgressTextField;
		[SerializeField] private Image _someProgressImageField;

		public void Something(float progress)
		{
			this._someProgressTextField.text = Mathf.Lerp(0f, 100f, progress).ToString();
			this._someProgressImageField.fillAmount = progress;
		}

		private void Start()
		{
			this.LoadSceneAsync(1, this.Something);
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{

		//}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(SceneController))]
    [CanEditMultipleObjects]
    public class SceneControllerEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            SceneController sSceneController = this.target as SceneController;
#pragma warning restore 0219
        }
    }
#endif
}