﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core
{
	[System.Serializable]
	public class NavMeshAgentData
	{

	}

	[RequireComponent(typeof(NavMeshAgent))]
    public class NavMeshAgentController : MonoBehaviour
    {
		[SerializeField] protected NavMeshAgentData navMeshAgentData;

		public NavMeshAgent NavMeshAgent_ { get; private set; }

		public void SetDestination(Vector3 target)
		{
			this.NavMeshAgent_.SetDestination(target);
		}

		private void Awake()
		{
			this.NavMeshAgent_ = this.GetComponent<NavMeshAgent>();
		}

#if UNITY_EDITOR
		protected virtual void OnDrawGizmos()
		{

		}
#endif
    }
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(NavMeshAgentController))]
    [CanEditMultipleObjects]
    public class NavMeshAgentControllerEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            NavMeshAgentController sNavMeshAgentController = target as NavMeshAgentController;
#pragma warning restore 0219
        }
    }
#endif
}