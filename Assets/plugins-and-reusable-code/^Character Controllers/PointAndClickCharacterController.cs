﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;
using Kimo.Audio;
using Kimo.Assistance;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Kimo.Core
{
	[System.Serializable]
	public class PointAndClickCharacterData
	{
		public Inventory Inventory { get; set; }

		public const string WALK_ANIMATION_KEY = "Walk";
		public const string RUN_ANIMATION_KEY = "Run";
		public const string IDLE_ANIMATION_KEY = "Idle";
		public const string JUMP_ANIMATION_KEY = "Jump";
		public const string PICK_UP_ANIMATION_KEY = "PickUp";
		public const string TALK_ANIMATION_KEY = "Talk";

		private int _walkAnimationHash = Animator.StringToHash(WALK_ANIMATION_KEY);
		public int _WalkAnimationHash { get { return this._walkAnimationHash; } }

		private int _runAnimationHash = Animator.StringToHash(RUN_ANIMATION_KEY);
		public int _RunAnimationHash { get { return this._runAnimationHash; } }

		private int _idleAnimationHash = Animator.StringToHash(IDLE_ANIMATION_KEY);
		public int _IdleAnimationHash { get { return this._idleAnimationHash; } }

		private int _jumpAnimationHash = Animator.StringToHash(JUMP_ANIMATION_KEY);
		public int _JumpAnimationHash { get { return this._jumpAnimationHash; } }

		private int _pickUpAnimationHash = Animator.StringToHash(PICK_UP_ANIMATION_KEY);
		public int _PickUpAnimationHash { get { return this._pickUpAnimationHash; } }

		private int _talkAnimationHash = Animator.StringToHash(TALK_ANIMATION_KEY);
		public int _TalkAnimationHash { get { return this._talkAnimationHash; } }

		[SerializeField] private AudioClip _movementAudioClip;
		public AudioClip _MovementAudioClip { get { return this._movementAudioClip; } }
	}

	[RequireComponent(typeof(NavMeshAgentController), typeof(Animator))]
	public class PointAndClickCharacterController : MonoBehaviour
	{
		[SerializeField] private PointAndClickCharacterData _pointAndClickCharacterData;
		public PointAndClickCharacterData _PointAndClickCharacterData { get { return this._pointAndClickCharacterData; } }

		private NavMeshAgentController _navMeshAgentController;
		private Animator _animator;

		private Cooldown _movementInitiationAudioCooldown;

		public void OnPointerClick(BaseEventData baseEventData)
		{
			PointerEventData pointerEventData = baseEventData as PointerEventData;

			this._navMeshAgentController.SetDestination(pointerEventData.pointerCurrentRaycast.worldPosition);

			if (this._movementInitiationAudioCooldown.IsFinished() && Random.value > 0.8f)
			{
				AudioPlayer.PlayClip(this._pointAndClickCharacterData._MovementAudioClip, AudioPlayer.AudioPlayerType.Narrative, AudioSourceBehavior.AudioSourceBehaviourPresetType.Default);

				this._movementInitiationAudioCooldown.Reset();
			}

			// this._animator.SetTrigger(this._pointAndClickCharacterData._WalkAnimationHash);
		}

		private void Awake()
		{
			this._navMeshAgentController = this.GetComponent<NavMeshAgentController>();
			this._animator = this.GetComponent<Animator>();

			this._movementInitiationAudioCooldown = new Cooldown(5f);

			this._pointAndClickCharacterData.Inventory = new Inventory(8, 8);
		}

		private void Start()
		{
			// InventoryDisplayController.Instance_.Display(this._pointAndClickCharacterData.Inventory);
			this._animator.SetTrigger(this._pointAndClickCharacterData._IdleAnimationHash);
		}

#if UNITY_EDITOR
		protected virtual void OnDrawGizmos()
		{

		}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
	[CustomEditor(typeof(PointAndClickCharacterController))]
	[CanEditMultipleObjects]
	public class PointAndClickCharacterControllerEditor : Editor
	{
		private void OnEnable()
		{

		}

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();

#pragma warning disable 0219
			PointAndClickCharacterController sPointAndClickCharacterController = target as PointAndClickCharacterController;
#pragma warning restore 0219
		}
	}
#endif
}