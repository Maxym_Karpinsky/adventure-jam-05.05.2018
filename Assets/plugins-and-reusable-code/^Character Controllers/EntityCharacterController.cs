﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using Kimo.Audio;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
	[System.Serializable]
	public class EntityCharacterData
	{
		[Header("Movement")]
		[SerializeField] private float _movementSpeedWalkingState = 5f;
		public float _MovementSpeedWalkingState { get { return this._movementSpeedWalkingState; } }

		[SerializeField] private float _movementSpeedItemPickWalkingState = 3f;
		public float _MovementSpeedItemPickWalkingState { get { return this._movementSpeedItemPickWalkingState; } }

		[SerializeField] private AudioArchive _movementAudioArchive;
		public AudioArchive _MovementAudioArchive { get { return this._movementAudioArchive; } }

		[SerializeField] private float _movementSpeedJumpingState = 3f;
		public float _MovementSpeedJumpingState { get { return this._movementSpeedJumpingState; } }

		[SerializeField] private AudioArchive _jumpingAudioArchive;
		public AudioArchive _JumpingAudioArchive { get { return this._jumpingAudioArchive; } }

		[Header("Grounding")]
		[SerializeField] private Transform _groundCheckPoint;
		public Transform _GroundCheckPoint { get { return this._groundCheckPoint; } }

		[SerializeField] private float _groundCheckRadius = 0.4f;
		public float _GroundCheckRadius { get { return this._groundCheckRadius; } }

		public bool IsGrounded { get; set; }

		[SerializeField] private float _defaultGroundCheckFrequency = 1f;
		public float _DefaultGroundCheckFrequency { get { return this._defaultGroundCheckFrequency; } }

		public float GroundCheckFrequency { get; set; }

		public float GroundCheckCooldown { get; set; }

		[SerializeField] private LayerMask _groundLayerMask;
		public LayerMask _GroundLayerMask { get { return this._groundLayerMask; } }

		[Header("Jump")]
		[SerializeField] private float _jumpForce = 250f;
		public float _JumpForce { get { return this._jumpForce; } }

		public EntityCharacterState EntityState { get; set; }
	}

	[RequireComponent(typeof(Rigidbody))]
	public abstract class EntityCharacterController : MonoBehaviour 
    {
		[SerializeField] protected EntityCharacterData entityCharacterData;
		public EntityCharacterData _EntityCharacterData { get { return this.entityCharacterData; } }

		private Rigidbody _rigidbody;
		public Rigidbody _RigidBody { get { return this._rigidbody; } }

		[SerializeField] private Animator _animator;
		public Animator _Animator { get { return this._animator; } }

		public void TriggerAnimation(int idHash)
		{
			if (this._animator != null)
				this._animator.SetTrigger(idHash);
		}

		protected virtual void Awake()
		{
			this._rigidbody = this.GetComponent<Rigidbody>();

			if (this._animator == null)
				this._animator = this.GetComponent<Animator>();

			this.entityCharacterData.GroundCheckFrequency = this.entityCharacterData._DefaultGroundCheckFrequency;
		}

		protected virtual void Start()
		{
			this.entityCharacterData.EntityState = new EntityCharacterStateIdle(this);
		}

		protected virtual void Update()
		{
			this.entityCharacterData.EntityState.Update();

			this.entityCharacterData.GroundCheckCooldown -= Time.deltaTime;

			if (this.entityCharacterData.GroundCheckCooldown <= 0)
			{
				this.entityCharacterData.IsGrounded = Physics.CheckSphere(
					this.entityCharacterData._GroundCheckPoint.position,
					this.entityCharacterData._GroundCheckRadius,
					this.entityCharacterData._GroundLayerMask
				) && this._rigidbody.velocity.y <= 0.01f;

				this.entityCharacterData.GroundCheckCooldown = this.entityCharacterData.GroundCheckFrequency;
			}

			// Vector3 directionalRotation = CameraController.Instance_._ControlledCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, CameraController.Instance_._ControlledCamera.transform.position.y - this.transform.position.y)) - this.transform.position;
			// directionalRotation.y = 0;

			// this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(directionalRotation), Time.deltaTime * 3.5f);
		}

		public void Move(Vector3 velocity)
		{
			velocity = Quaternion.Euler(0, CameraController.Instance_._ControlledCamera.transform.eulerAngles.y, 0) * velocity;
			// velocity.y = this._rigidbody.velocity.y;

			this.transform.Translate(velocity * Time.deltaTime, Space.World);
			this._rigidbody.MovePosition(this.transform.position);

			// Null the physics movement - I guess that can be done in Rigidbody as well
			this._rigidbody.velocity = new Vector3(0, this._rigidbody.velocity.y, 0);

			velocity.y = 0;
			if (velocity.sqrMagnitude > 0.001f)
				this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(velocity), Time.deltaTime * 8f);
		}

#if UNITY_EDITOR
		protected virtual void OnDrawGizmos()
        {
			if (this.entityCharacterData._GroundCheckPoint != null)
				Gizmos.DrawWireSphere(this.entityCharacterData._GroundCheckPoint.position, this.entityCharacterData._GroundCheckRadius);
		}
#endif
    }
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(EntityCharacterController))]
    [CanEditMultipleObjects]
    public class EntityControllerEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            EntityCharacterController sEntityController = target as EntityCharacterController;
#pragma warning restore 0219
        }
    }
#endif
}