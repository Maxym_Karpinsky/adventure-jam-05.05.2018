﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
	public abstract class EntityCharacterState
	{
		protected EntityCharacterController entityCharacterController;

		public EntityCharacterState(EntityCharacterController entityCharacterController)
		{
			// Debug.Log(this.ToString());

			this.entityCharacterController = entityCharacterController;
		}

		public abstract bool HandleInput();
		public abstract void Update();
	}
}