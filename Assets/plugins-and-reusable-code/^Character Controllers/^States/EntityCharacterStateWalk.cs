﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using Kimo.Audio;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    public class EntityCharacterStateWalk : EntityCharacterState
	{
		private static int animationHash = Animator.StringToHash("Run");
		private CooldownExecutor _cooldownExecutor;

		public EntityCharacterStateWalk(EntityCharacterController entityCharacterController) : base(entityCharacterController)
		{
			if (this.HandleInput())
				this.entityCharacterController.TriggerAnimation(animationHash);

			this._cooldownExecutor = new CooldownExecutor(new Cooldown(0.4f, 0.4f), this.PlayWalkingSound);
		}

		public override bool HandleInput()
		{
			bool isInputDetected = false;
			Vector3 movement = Vector3.zero;

			if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
			{
				movement += Vector3.forward;

				isInputDetected = true;
			}
			else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
			{
				movement -= Vector3.forward;

				isInputDetected = true;
			}

			if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
			{
				movement -= Vector3.right;

				isInputDetected = true;
			}
			else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
			{
				movement += Vector3.right;

				isInputDetected = true;
			}

			this.entityCharacterController.Move(movement.normalized * this.entityCharacterController._EntityCharacterData._MovementSpeedWalkingState);

			return isInputDetected;
		}


		public override void Update()
		{
			if (Input.GetKey(KeyCode.Space) && this.entityCharacterController._EntityCharacterData.IsGrounded)
			{
				this.entityCharacterController._EntityCharacterData.EntityState = new EntityCharacterStateJump(this.entityCharacterController);
			}
			else if (!this.HandleInput())
			{
				this.entityCharacterController._EntityCharacterData.EntityState = new EntityCharacterStateIdle(this.entityCharacterController);
			}
			else
			{
				this._cooldownExecutor.Execute();
			}
		}

		public void PlayWalkingSound()
		{
			//AudioPlayer.PlaySoundEffectsClipAtWorldPoint(
			//	this.entityCharacterController._EntityCharacterData._MovementAudioArchive.GetRandomAudioClip(), 
			//	AudioPlayer.PresetType.Default, 
			//	this.entityCharacterController.transform.position
			//);
		}
	}
}