﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
	public class EntityCharacterStateIdle : EntityCharacterState
	{
		private static int animationHash = Animator.StringToHash("Idle");

		public EntityCharacterStateIdle(EntityCharacterController entityCharacterController) : base(entityCharacterController)
		{
			this.entityCharacterController.TriggerAnimation(animationHash);
		}

		public override bool HandleInput()
		{
			return true;
		}

		public override void Update()
		{
			this.entityCharacterController.Move(Vector3.zero);

			if (Input.GetKey(KeyCode.Space) && this.entityCharacterController._EntityCharacterData.IsGrounded)
			{
				this.entityCharacterController._EntityCharacterData.EntityState = new EntityCharacterStateJump(this.entityCharacterController);
			}
			else if (
				Input.GetKey(KeyCode.W) ||
				Input.GetKey(KeyCode.S) ||
				Input.GetKey(KeyCode.A) ||
				Input.GetKey(KeyCode.D) ||
				Input.GetKey(KeyCode.UpArrow) ||
				Input.GetKey(KeyCode.DownArrow) ||
				Input.GetKey(KeyCode.LeftArrow) ||
				Input.GetKey(KeyCode.RightArrow))
			{
				this.entityCharacterController._EntityCharacterData.EntityState = new EntityCharacterStateWalk(this.entityCharacterController);
			}
		}
	}
}