﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    public class EntityCharacterStateItemPickIdle : MonoBehaviour 
    {


#if UNITY_EDITOR
        protected virtual void OnDrawGizmos()
        {
            
        }
#endif
    }
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(EntityCharacterStateItemPickIdle))]
    [CanEditMultipleObjects]
    public class EntityCharacterStateItemPickIdleEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            EntityCharacterStateItemPickIdle sEntityCharacterStateItemPickIdle = target as EntityCharacterStateItemPickIdle;
#pragma warning restore 0219
        }
    }
#endif
}