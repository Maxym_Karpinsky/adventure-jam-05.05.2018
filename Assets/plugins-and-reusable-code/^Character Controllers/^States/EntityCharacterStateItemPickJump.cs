﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using Kimo.Audio;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    public class EntityCharacterStateItemPickJump : EntityCharacterState 
    {
		private static int animationHash = Animator.StringToHash("Jump");

		public EntityCharacterStateItemPickJump(EntityCharacterController entityCharacterController) : base(entityCharacterController)
		{
			this.entityCharacterController._EntityCharacterData.GroundCheckFrequency = 0.2f;
			this.entityCharacterController._EntityCharacterData.IsGrounded = false;

			this.entityCharacterController._RigidBody.velocity = Vector3.zero;
			this.entityCharacterController._RigidBody.AddForce(Vector3.up * this.entityCharacterController._EntityCharacterData._JumpForce);

			this.entityCharacterController.TriggerAnimation(animationHash);

			//AudioPlayer.PlaySoundEffectsClipAtWorldPoint(
			//	this.entityCharacterController._EntityCharacterData._JumpingAudioArchive.GetRandomAudioClip(),
			//	AudioPlayer.PresetType.Default,
			//	this.entityCharacterController.transform.position
			//);

			this.HandleInput();
		}

		public override bool HandleInput()
		{
			bool isInputDetected = false;
			Vector3 movement = Vector3.zero;

			if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
			{
				movement += Vector3.forward;

				isInputDetected = true;
			}
			else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
			{
				movement -= Vector3.forward;

				isInputDetected = true;
			}

			if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
			{
				movement -= Vector3.right;

				isInputDetected = true;
			}
			else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
			{
				movement += Vector3.right;

				isInputDetected = true;
			}

			this.entityCharacterController.Move(movement.normalized * this.entityCharacterController._EntityCharacterData._MovementSpeedJumpingState);

			return isInputDetected;
		}

		public override void Update()
		{
			if (this.entityCharacterController._EntityCharacterData.IsGrounded)
			{
				this.entityCharacterController._EntityCharacterData.GroundCheckFrequency = this.entityCharacterController._EntityCharacterData._DefaultGroundCheckFrequency;

				if (this.HandleInput())
					this.entityCharacterController._EntityCharacterData.EntityState = new EntityCharacterStateItemPickWalk(this.entityCharacterController);
			}
			else
			{
				this.HandleInput();
			}
		}
	}
}