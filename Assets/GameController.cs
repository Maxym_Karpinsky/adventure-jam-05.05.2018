﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;
using Kimo.Assistance.UTILITY;

using TMPro;

namespace Kimo.Core
{
    public class GameController : MonoBehaviourSingleton<GameController>
    {
		[SerializeField] ComboTracker _comboTracker;
		public ComboTracker _ComboTracker { get { return this._comboTracker; } }

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{
		//}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(GameController))]
    [CanEditMultipleObjects]
    public class GameControllerEditor : Editor
    {
#pragma warning disable 0219
        private GameController _sGameController;
#pragma warning restore 0219

        private void OnEnable()
        {
            this._sGameController = target as GameController;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
        }
    }
#endif
}