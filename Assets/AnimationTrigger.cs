﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;
using Kimo.Assistance.UTILITY;

using TMPro;

namespace Kimo.Core
{
	[RequireComponent(typeof(Animator))]
    public class AnimationTrigger : MonoBehaviour
    {
		[SerializeField] private string[] _animationStringKeys;
		private int[] _animationHashKeys;

		public int _AnimationsQuantity { get { return this._animationHashKeys.Length; } }

		private Animator _animator;

		public void TriggerAnimation(int animationIndex)
		{
			this._animator.SetTrigger(this._animationHashKeys[animationIndex]);
		}

		private void Awake()
		{
			this._animator = this.GetComponent<Animator>();

			this._animationHashKeys = new int[this._animationStringKeys.Length];
			for (int i = 0; i < this._animationStringKeys.Length; i++)
			{
				this._animationHashKeys[i] = Animator.StringToHash(this._animationStringKeys[i]);
			}
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{
		//}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(AnimationTrigger))]
    [CanEditMultipleObjects]
    public class AnimationTriggerEditor : Editor
    {
#pragma warning disable 0219
        private AnimationTrigger _sAnimationTrigger;
#pragma warning restore 0219

        private void OnEnable()
        {
            this._sAnimationTrigger = target as AnimationTrigger;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
        }
    }
#endif
}