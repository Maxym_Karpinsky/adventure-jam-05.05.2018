﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;
using Kimo.Assistance.UTILITY;

using TMPro;

namespace Kimo.Core
{
    public class ComboDisplayController : UserInterfaceDisplayController<ComboDisplayController, ComboDisplay>
    {
		[SerializeField] private ComboTracker _comboTracker;
		private Animator _animator;

		public override void UpdateLayout()
		{
			this.controlledUserInterfaceDisplay._ComboTextField.text = "x" + this._comboTracker._ComboValue.ToCurrencyString();

			this._animator.SetTrigger("Combo");
		}

		protected override void Awake()
		{
			base.Awake();

			this._animator = this.GetComponent<Animator>();
		}

		private void Start()
		{
			this.Close();
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{
		//}
#endif
	}
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(ComboDisplayController))]
    [CanEditMultipleObjects]
    public class ComboDisplayControllerEditor : Editor
    {
#pragma warning disable 0219
        private ComboDisplayController _sComboDisplayController;
#pragma warning restore 0219

        private void OnEnable()
        {
            this._sComboDisplayController = target as ComboDisplayController;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
        }
    }
#endif
}