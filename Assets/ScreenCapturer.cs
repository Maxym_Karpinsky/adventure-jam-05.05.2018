﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using System;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;
using Kimo.Assistance.UTILITY;

namespace Kimo.Core
{
#if UNITY_EDITOR
	public class ScreenCapturer : MonoBehaviourSingleton<ScreenCapturer>
	{
		private bool _processingImage;

		[Range(1f, 15f)]
		[SerializeField] private float _recordingCooldownTime = 5f;
		private Cooldown _recordingCooldown;

		[SerializeField] private Camera _camera;
		[SerializeField] private Vector2 _resolution = new Vector2(512, 288);

		[SerializeField] private KeyCode _recordingTriggerKeyCode = KeyCode.F9;

		public void Record()
		{
			this._recordingCooldown.Reset();

			this.StartCoroutine(this.RecordProcess());
		}

		private List<Texture2D> _recordedTextures;

		private IEnumerator RecordProcess()
		{
			this._recordedTextures = new List<Texture2D>(128);

			while (!this._recordingCooldown.IsFinished())
			{
				this._processingImage = true;

				ScreenCaptureUtility.CameraCaptureOverlayedScreenshotAsTexture(this, this._camera, (int)this._resolution.x, (int)this._resolution.y, new ScreenCaptureUtility.TextureOverlayData[] { }, this.ProcessImage);

				yield return new WaitUntil(() => !this._processingImage);
			}

			this.SaveTexturesAsPNG();
		}
		
		private void ProcessImage(Texture2D texture2D)
		{
			texture2D.name = (this._recordingCooldown.GetElapsedTime() / this._recordingCooldown.TagetElapsedTime_).ToString("P3");
			this._recordedTextures.Add(texture2D);

			this._processingImage = false;
		}

		private void SaveTexturesAsPNG()
		{
			string dataPath = Application.dataPath + string.Format("/Screenshots/Date - {0}. Time - {1}", DateTime.Now.ToLongDateString(), string.Format(DateTime.Now.ToString("h{0} mm{1} ss{2} tt"), "h", "m", "s"));

			DirectoryInfo directoryInfo = Directory.CreateDirectory(dataPath);
			
			for (int i = 0; i < this._recordedTextures.Count; i++)
				File.WriteAllBytes(Path.Combine(directoryInfo.FullName, string.Format("Frame - {0} Resolution - {1}x{2}.png", this._recordedTextures[i].name, this._recordedTextures[i].width, this._recordedTextures[i].height)), this._recordedTextures[i].EncodeToPNG());

			AssetDatabase.Refresh();
		}

		private void SaveTexturesAsGIF()
		{
			string dataPath = Application.dataPath + string.Format("/Screenshots/Date - {0}. Time - {1}", DateTime.Now.ToLongDateString(), string.Format(DateTime.Now.ToString("h{0} mm{1} ss{2} tt"), "h", "m", "s"));

			DirectoryInfo directoryInfo = Directory.CreateDirectory(dataPath);

			for (int i = 0; i < this._recordedTextures.Count; i++)
			{
				string imagePath = Path.Combine(directoryInfo.FullName, string.Format("Frame - {0} Resolution - {1}x{2}.png", this._recordedTextures[i].name, this._recordedTextures[i].width, this._recordedTextures[i].height));

				byte[] textureAsPNGBytes = this._recordedTextures[i].EncodeToPNG();

				File.WriteAllBytes(imagePath, textureAsPNGBytes);

				
			}

			// System.Windows.Media.Imaging - suka bliat
			// https://docs.microsoft.com/en-us/dotnet/framework/wpf/graphics-multimedia/how-to-encode-and-decode-a-gif-image
			// https://www.codeproject.com/Articles/11505/NGif-Animated-GIF-Encoder-for-NET

			throw new NotImplementedException();

			AssetDatabase.Refresh();
		}

#if UNITY_EDITOR
		private void Update()
		{
			if (Input.GetKeyDown(this._recordingTriggerKeyCode))
			{
				this.Record();
			}
		}
#endif

		protected override void Awake()
		{
			base.Awake();

			this._recordingCooldown = new Cooldown(this._recordingCooldownTime);
		}
	}
#endif
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
	[CustomEditor(typeof(ScreenCapturer))]
	[CanEditMultipleObjects]
	public class ScreenCapturerEditor : Editor
	{
		private void OnEnable()
		{

		}

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();

#pragma warning disable 0219
			ScreenCapturer sScreenCapturer = target as ScreenCapturer;
#pragma warning restore 0219

			if (GUILayout.Button("Record"))
			{
				sScreenCapturer.Record();
			}
		}
	}
#endif
}