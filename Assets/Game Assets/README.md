Hacking + Propaganda and people without emotions.


Gameplay: hacking (beat them up/slash/top-down shooter) by the person in government that works secretly to bring down the system and doesn't take pills to reduce his emotions. Similar to watchdogs 2, but with swords, emotionless people, and top-down.


End goal: I think to reach the final most important person like in Equilibrium which will bring down the system from the root. Or maybe to upload a virus to manufacturing system to transform the pills to take no effect on people, so that they could have their emotions back.


Visuals: We are targeting sci-fi neon low poly style, inspiration can be taken from titles like Blade Runner, Tron, Guardians Of The Galaxy, Star Wars, Valerian and the City of a Thousand Planets.
But mostly we are going for a cyberpunk style.
3D with post-processing stack (bloom....). Simple shaped particles. Minimalistic UI. Lights, flickering, neon. Text messages in English as well as symbols (maybe Korean, Japanese) along with cyrillic (I can take care of cyrillic).

Music: The gameplay will mostly be slow paced, exploratory like in adventure games. The music will only speed up during important battles or when character has been noticed(we may have stealth mechanics in some parts of the game) or when character is in danger. Mostly `dark style`. Some inspiration can be taken from soundtracks of Watch Dogs 2, Batman: Arkham Knight. Cyberpunk style.
Very important part are kind of interruptions in audio, like dithering, electronic interference. Organ and violin sounds are magnificent if they can be mixed with those.

```
Mechanics:
Hacking (In a prototype just a line indicator that takes some time to finish. Later can be an interface with a puzzle in some cases.).

Movement (WASD).

Interaction (E or Mouse).

Shooting/Attack/Swing (Mouse buttons).
```
