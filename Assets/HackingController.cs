﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Kimo.Assistance;

namespace Kimo.Core
{
    public class HackingController : MonoBehaviourSingleton<HackingController> 
    {
		private bool _processingHacking;

		public void StartHacking(HackingData hackingData, HackingEntity hackingEntity)
		{
			if (hackingData._RequiredSkillLevel < 3 && !this._processingHacking)
			{
				ProgressBarDisplayController.Instance_.SetPosition(hackingEntity.transform);

				this._processingHacking = true;

				this.StartCoroutine(this.HackingProcess(hackingData, hackingEntity));
			}
		}

		private IEnumerator HackingProcess(HackingData hackingData, HackingEntity hackingEntity)
		{
			Cooldown hackingCooldown = new Cooldown(hackingData._HackingComplexity);

			ProgressBarDisplayController.Instance_.Open();

			while (Input.GetKey(KeyCode.E) && !hackingCooldown.IsFinished())
			{
				ProgressBarDisplayController.Instance_.Display(hackingCooldown.GetElapsedTime(), hackingData._HackingComplexity);

				yield return null;
			}

			if (hackingCooldown.IsFinished())
			{
				ProgressBarDisplayController.Instance_.Display(hackingData._HackingComplexity, hackingData._HackingComplexity);
				
				// Give rewards.

				hackingEntity.HasBeenHacked = true;
				hackingEntity._OnSuccess.Invoke();

				yield return new WaitForSeconds(1f);
			}

			ProgressBarDisplayController.Instance_.Close();

			this._processingHacking = false;

			if (!hackingEntity.HasBeenHacked)
			{
				hackingEntity._OnFailure.Invoke();
			}
		}

#if UNITY_EDITOR
        //protected override void OnDrawGizmos()
        //{
            
        //}
#endif
    }
}

namespace Kimo.Core.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(HackingController))]
    [CanEditMultipleObjects]
    public class HackingControllerEditor : Editor
    {
        private void OnEnable()
        {
            
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#pragma warning disable 0219
            HackingController sHackingController = target as HackingController;
#pragma warning restore 0219
        }
    }
#endif
}